// Avoid `console` errors in browsers that lack a console.
(function () {
    var method;
    var noop = function () {};
    var methods = [
        'assert', 'clear', 'count', 'debug', 'dir', 'dirxml', 'error',
        'exception', 'group', 'groupCollapsed', 'groupEnd', 'info', 'log',
        'markTimeline', 'profile', 'profileEnd', 'table', 'time', 'timeEnd',
        'timeStamp', 'trace', 'warn'
    ];
    var length = methods.length;
    var console = (window.console = window.console || {});

    while (length--) {
        method = methods[length];

        // Only stub undefined methods.
        if (!console[method]) {
            console[method] = noop;
        }
    }
}());

// Place any jQuery/helper plugins in here.

/*!
 * jCarousel - Riding carousels with jQuery
 *   http://sorgalla.com/jcarousel/
 *
 * Copyright (c) 2006 Jan Sorgalla (http://sorgalla.com)
 * Dual licensed under the MIT (http://www.opensource.org/licenses/mit-license.php)
 * and GPL (http://www.opensource.org/licenses/gpl-license.php) licenses.
 *
 * Built on top of the jQuery library
 *   http://jquery.com
 *
 * Inspired by the "Carousel Component" by Bill Scott
 *   http://billwscott.com/carousel/
 */

(function (g) {
    var q = {
        vertical: !1,
        rtl: !1,
        start: 1,
        offset: 1,
        size: null,
        scroll: 3,
        visible: null,
        animation: "normal",
        easing: "swing",
        auto: 0,
        wrap: null,
        initCallback: null,
        setupCallback: null,
        reloadCallback: null,
        itemLoadCallback: null,
        itemFirstInCallback: null,
        itemFirstOutCallback: null,
        itemLastInCallback: null,
        itemLastOutCallback: null,
        itemVisibleInCallback: null,
        itemVisibleOutCallback: null,
        animationStepCallback: null,
        buttonNextHTML: "<div></div>",
        buttonPrevHTML: "<div></div>",
        buttonNextEvent: "click",
        buttonPrevEvent: "click",
        buttonNextCallback: null,
        buttonPrevCallback: null,
        itemFallbackDimension: null
    }, m = !1;
    g(window).bind("load.jcarousel", function () {
        m = !0
    });
    g.jcarousel = function (a, c) {
        this.options = g.extend({}, q, c || {});
        this.autoStopped = this.locked = !1;
        this.buttonPrevState = this.buttonNextState = this.buttonPrev = this.buttonNext = this.list = this.clip = this.container = null;
        if (!c || c.rtl === void 0) this.options.rtl = (g(a).attr("dir") || g("html").attr("dir") || "").toLowerCase() == "rtl";
        this.wh = !this.options.vertical ? "width" : "height";
        this.lt = !this.options.vertical ? this.options.rtl ? "right" : "left" : "top";
        for (var b = "", d = a.className.split(" "), f = 0; f < d.length; f++)
            if (d[f].indexOf("jcarousel-skin") != -1) {
                g(a).removeClass(d[f]);
                b = d[f];
                break
            }
        a.nodeName.toUpperCase() == "UL" || a.nodeName.toUpperCase() == "OL" ? (this.list = g(a), this.clip = this.list.parents(".jcarousel-clip"), this.container = this.list.parents(".jcarousel-container")) : (this.container = g(a), this.list = this.container.find("ul,ol").eq(0), this.clip = this.container.find(".jcarousel-clip"));
        if (this.clip.size() === 0) this.clip = this.list.wrap("<div></div>").parent();
        if (this.container.size() === 0) this.container = this.clip.wrap("<div></div>").parent();
        b !== "" && this.container.parent()[0].className.indexOf("jcarousel-skin") == -1 && this.container.wrap('<div class=" ' + b + '"></div>');
        this.buttonPrev = g(".jcarousel-prev", this.container);
        if (this.buttonPrev.size() === 0 && this.options.buttonPrevHTML !== null) this.buttonPrev = g(this.options.buttonPrevHTML).appendTo(this.container);
        this.buttonPrev.addClass(this.className("jcarousel-prev"));
        this.buttonNext = g(".jcarousel-next", this.container);
        if (this.buttonNext.size() === 0 && this.options.buttonNextHTML !== null) this.buttonNext = g(this.options.buttonNextHTML).appendTo(this.container);
        this.buttonNext.addClass(this.className("jcarousel-next"));
        this.clip.addClass(this.className("jcarousel-clip")).css({
            position: "relative"
        });
        this.list.addClass(this.className("jcarousel-list")).css({
            overflow: "hidden",
            position: "relative",
            top: 0,
            margin: 0,
            padding: 0
        }).css(this.options.rtl ? "right" : "left", 0);
        this.container.addClass(this.className("jcarousel-container")).css({
            position: "relative"
        });
        !this.options.vertical && this.options.rtl && this.container.addClass("jcarousel-direction-rtl").attr("dir", "rtl");
        var j = this.options.visible !== null ? Math.ceil(this.clipping() / this.options.visible) : null,
            b = this.list.children("li"),
            e = this;
        if (b.size() > 0) {
            var h = 0,
                i = this.options.offset;
            b.each(function () {
                e.format(this, i++);
                h += e.dimension(this, j)
            });
            this.list.css(this.wh, h + 100 + "px");
            if (!c || c.size === void 0) this.options.size = b.size()
        }
        this.container.css("display", "block");
        this.buttonNext.css("display", "block");
        this.buttonPrev.css("display", "block");
        this.funcNext = function () {
            e.next()
        };
        this.funcPrev = function () {
            e.prev()
        };
        this.funcResize = function () {
            e.resizeTimer && clearTimeout(e.resizeTimer);
            e.resizeTimer = setTimeout(function () {
                e.reload()
            }, 100)
        };
        this.options.initCallback !== null && this.options.initCallback(this, "init");
        !m && g.browser.safari ? (this.buttons(!1, !1), g(window).bind("load.jcarousel", function () {
            e.setup()
        })) : this.setup()
    };
    var f = g.jcarousel;
    f.fn = f.prototype = {
        jcarousel: "0.2.8"
    };
    f.fn.extend = f.extend = g.extend;
    f.fn.extend({
        setup: function () {
            this.prevLast = this.prevFirst = this.last = this.first = null;
            this.animating = !1;
            this.tail = this.resizeTimer = this.timer = null;
            this.inTail = !1;
            if (!this.locked) {
                this.list.css(this.lt, this.pos(this.options.offset) + "px");
                var a = this.pos(this.options.start, !0);
                this.prevFirst = this.prevLast = null;
                this.animate(a, !1);
                g(window).unbind("resize.jcarousel", this.funcResize).bind("resize.jcarousel", this.funcResize);
                this.options.setupCallback !== null && this.options.setupCallback(this)
            }
        },
        reset: function () {
            this.list.empty();
            this.list.css(this.lt, "0px");
            this.list.css(this.wh, "10px");
            this.options.initCallback !== null && this.options.initCallback(this, "reset");
            this.setup()
        },
        reload: function () {
            this.tail !== null && this.inTail && this.list.css(this.lt, f.intval(this.list.css(this.lt)) + this.tail);
            this.tail = null;
            this.inTail = !1;
            this.options.reloadCallback !== null && this.options.reloadCallback(this);
            if (this.options.visible !== null) {
                var a = this,
                    c = Math.ceil(this.clipping() / this.options.visible),
                    b = 0,
                    d = 0;
                this.list.children("li").each(function (f) {
                    b += a.dimension(this, c);
                    f + 1 < a.first && (d = b)
                });
                this.list.css(this.wh, b + "px");
                this.list.css(this.lt, -d + "px")
            }
            this.scroll(this.first, !1)
        },
        lock: function () {
            this.locked = !0;
            this.buttons()
        },
        unlock: function () {
            this.locked = !1;
            this.buttons()
        },
        size: function (a) {
            if (a !== void 0) this.options.size = a, this.locked || this.buttons();
            return this.options.size
        },
        has: function (a, c) {
            if (c === void 0 || !c) c = a;
            if (this.options.size !== null && c > this.options.size) c = this.options.size;
            for (var b = a; b <= c; b++) {
                var d = this.get(b);
                if (!d.length || d.hasClass("jcarousel-item-placeholder")) return !1
            }
            return !0
        },
        get: function (a) {
            return g(">.jcarousel-item-" + a, this.list)
        },
        add: function (a, c) {
            var b = this.get(a),
                d = 0,
                p = g(c);
            if (b.length === 0)
                for (var j, e = f.intval(a), b = this.create(a);;) {
                    if (j = this.get(--e), e <= 0 || j.length) {
                        e <= 0 ? this.list.prepend(b) : j.after(b);
                        break
                    }
                } else d = this.dimension(b);
            p.get(0).nodeName.toUpperCase() == "LI" ? (b.replaceWith(p), b = p) : b.empty().append(c);
            this.format(b.removeClass(this.className("jcarousel-item-placeholder")), a);
            p = this.options.visible !== null ? Math.ceil(this.clipping() / this.options.visible) : null;
            d = this.dimension(b, p) - d;
            a > 0 && a < this.first && this.list.css(this.lt, f.intval(this.list.css(this.lt)) - d + "px");
            this.list.css(this.wh, f.intval(this.list.css(this.wh)) + d + "px");
            return b
        },
        remove: function (a) {
            var c = this.get(a);
            if (c.length && !(a >= this.first && a <= this.last)) {
                var b = this.dimension(c);
                a < this.first && this.list.css(this.lt, f.intval(this.list.css(this.lt)) + b + "px");
                c.remove();
                this.list.css(this.wh, f.intval(this.list.css(this.wh)) - b + "px")
            }
        },
        next: function () {
            this.tail !== null && !this.inTail ? this.scrollTail(!1) : this.scroll((this.options.wrap == "both" || this.options.wrap == "last") && this.options.size !== null && this.last == this.options.size ? 1 : this.first + this.options.scroll)
        },
        prev: function () {
            this.tail !== null && this.inTail ? this.scrollTail(!0) : this.scroll((this.options.wrap == "both" || this.options.wrap == "first") && this.options.size !== null && this.first == 1 ? this.options.size : this.first - this.options.scroll)
        },
        scrollTail: function (a) {
            if (!this.locked && !this.animating && this.tail) {
                this.pauseAuto();
                var c = f.intval(this.list.css(this.lt)),
                    c = !a ? c - this.tail : c + this.tail;
                this.inTail = !a;
                this.prevFirst = this.first;
                this.prevLast = this.last;
                this.animate(c)
            }
        },
        scroll: function (a, c) {
            !this.locked && !this.animating && (this.pauseAuto(), this.animate(this.pos(a), c))
        },
        pos: function (a, c) {
            var b = f.intval(this.list.css(this.lt));
            if (this.locked || this.animating) return b;
            this.options.wrap != "circular" && (a = a < 1 ? 1 : this.options.size && a > this.options.size ? this.options.size : a);
            for (var d = this.first > a, g = this.options.wrap != "circular" && this.first <= 1 ? 1 : this.first, j = d ? this.get(g) : this.get(this.last), e = d ? g : g - 1, h = null, i = 0, k = !1, l = 0; d ? --e >= a : ++e < a;) {
                h = this.get(e);
                k = !h.length;
                if (h.length === 0 && (h = this.create(e).addClass(this.className("jcarousel-item-placeholder")), j[d ? "before" : "after"](h), this.first !== null && this.options.wrap == "circular" && this.options.size !== null && (e <= 0 || e > this.options.size))) j = this.get(this.index(e)), j.length && (h = this.add(e, j.clone(!0)));
                j = h;
                l = this.dimension(h);
                k && (i += l);
                if (this.first !== null && (this.options.wrap == "circular" || e >= 1 && (this.options.size === null || e <= this.options.size))) b = d ? b + l : b - l
            }
            for (var g = this.clipping(), m = [], o = 0, n = 0, j = this.get(a - 1), e = a; ++o;) {
                h = this.get(e);
                k = !h.length;
                if (h.length === 0) {
                    h = this.create(e).addClass(this.className("jcarousel-item-placeholder"));
                    if (j.length === 0) this.list.prepend(h);
                    else j[d ? "before" : "after"](h); if (this.first !== null && this.options.wrap == "circular" && this.options.size !== null && (e <= 0 || e > this.options.size)) j = this.get(this.index(e)), j.length && (h = this.add(e, j.clone(!0)))
                }
                j = h;
                l = this.dimension(h);
                if (l === 0) throw Error("jCarousel: No width/height set for items. This will cause an infinite loop. Aborting...");
                this.options.wrap != "circular" && this.options.size !== null && e > this.options.size ? m.push(h) : k && (i += l);
                n += l;
                if (n >= g) break;
                e++
            }
            for (h = 0; h < m.length; h++) m[h].remove();
            i > 0 && (this.list.css(this.wh, this.dimension(this.list) + i + "px"), d && (b -= i, this.list.css(this.lt, f.intval(this.list.css(this.lt)) - i + "px")));
            i = a + o - 1;
            if (this.options.wrap != "circular" && this.options.size && i > this.options.size) i = this.options.size;
            if (e > i) {
                o = 0;
                e = i;
                for (n = 0; ++o;) {
                    h = this.get(e--);
                    if (!h.length) break;
                    n += this.dimension(h);
                    if (n >= g) break
                }
            }
            e = i - o + 1;
            this.options.wrap != "circular" && e < 1 && (e = 1);
            if (this.inTail && d) b += this.tail, this.inTail = !1;
            this.tail = null;
            if (this.options.wrap != "circular" && i == this.options.size && i - o + 1 >= 1 && (d = f.intval(this.get(i).css(!this.options.vertical ? "marginRight" : "marginBottom")), n - d > g)) this.tail = n - g - d;
            if (c && a === this.options.size && this.tail) b -= this.tail, this.inTail = !0;
            for (; a-- > e;) b += this.dimension(this.get(a));
            this.prevFirst = this.first;
            this.prevLast = this.last;
            this.first = e;
            this.last = i;
            return b
        },
        animate: function (a, c) {
            if (!this.locked && !this.animating) {
                this.animating = !0;
                var b = this,
                    d = function () {
                        b.animating = !1;
                        a === 0 && b.list.css(b.lt, 0);
                        !b.autoStopped && (b.options.wrap == "circular" || b.options.wrap == "both" || b.options.wrap == "last" || b.options.size === null || b.last < b.options.size || b.last == b.options.size && b.tail !== null && !b.inTail) && b.startAuto();
                        b.buttons();
                        b.notify("onAfterAnimation");
                        if (b.options.wrap == "circular" && b.options.size !== null)
                            for (var c = b.prevFirst; c <= b.prevLast; c++) c !== null && !(c >= b.first && c <= b.last) && (c < 1 || c > b.options.size) && b.remove(c)
                    };
                this.notify("onBeforeAnimation");
                if (!this.options.animation || c === !1) this.list.css(this.lt, a + "px"), d();
                else {
                    var f = !this.options.vertical ? this.options.rtl ? {
                        right: a
                    } : {
                        left: a
                    } : {
                        top: a
                    }, d = {
                            duration: this.options.animation,
                            easing: this.options.easing,
                            complete: d
                        };
                    if (g.isFunction(this.options.animationStepCallback)) d.step = this.options.animationStepCallback;
                    this.list.animate(f, d)
                }
            }
        },
        startAuto: function (a) {
            if (a !== void 0) this.options.auto = a;
            if (this.options.auto === 0) return this.stopAuto();
            if (this.timer === null) {
                this.autoStopped = !1;
                var c = this;
                this.timer = window.setTimeout(function () {
                    c.next()
                }, this.options.auto * 1E3)
            }
        },
        stopAuto: function () {
            this.pauseAuto();
            this.autoStopped = !0
        },
        pauseAuto: function () {
            if (this.timer !== null) window.clearTimeout(this.timer), this.timer = null
        },
        buttons: function (a, c) {
            if (a == null && (a = !this.locked && this.options.size !== 0 && (this.options.wrap && this.options.wrap != "first" || this.options.size === null || this.last < this.options.size), !this.locked && (!this.options.wrap || this.options.wrap == "first") && this.options.size !== null && this.last >= this.options.size)) a = this.tail !== null && !this.inTail;
            if (c == null && (c = !this.locked && this.options.size !== 0 && (this.options.wrap && this.options.wrap != "last" || this.first > 1), !this.locked && (!this.options.wrap || this.options.wrap == "last") && this.options.size !== null && this.first == 1)) c = this.tail !== null && this.inTail;
            var b = this;
            this.buttonNext.size() > 0 ? (this.buttonNext.unbind(this.options.buttonNextEvent + ".jcarousel", this.funcNext), a && this.buttonNext.bind(this.options.buttonNextEvent + ".jcarousel", this.funcNext), this.buttonNext[a ? "removeClass" : "addClass"](this.className("jcarousel-next-disabled")).attr("disabled", a ? !1 : !0), this.options.buttonNextCallback !== null && this.buttonNext.data("jcarouselstate") != a && this.buttonNext.each(function () {
                b.options.buttonNextCallback(b, this, a)
            }).data("jcarouselstate", a)) : this.options.buttonNextCallback !== null && this.buttonNextState != a && this.options.buttonNextCallback(b, null, a);
            this.buttonPrev.size() > 0 ? (this.buttonPrev.unbind(this.options.buttonPrevEvent + ".jcarousel", this.funcPrev), c && this.buttonPrev.bind(this.options.buttonPrevEvent + ".jcarousel", this.funcPrev), this.buttonPrev[c ? "removeClass" : "addClass"](this.className("jcarousel-prev-disabled")).attr("disabled", c ? !1 : !0), this.options.buttonPrevCallback !== null && this.buttonPrev.data("jcarouselstate") != c && this.buttonPrev.each(function () {
                b.options.buttonPrevCallback(b, this, c)
            }).data("jcarouselstate", c)) : this.options.buttonPrevCallback !== null && this.buttonPrevState != c && this.options.buttonPrevCallback(b, null, c);
            this.buttonNextState = a;
            this.buttonPrevState = c
        },
        notify: function (a) {
            var c = this.prevFirst === null ? "init" : this.prevFirst < this.first ? "next" : "prev";
            this.callback("itemLoadCallback", a, c);
            this.prevFirst !== this.first && (this.callback("itemFirstInCallback", a, c, this.first), this.callback("itemFirstOutCallback", a, c, this.prevFirst));
            this.prevLast !== this.last && (this.callback("itemLastInCallback", a, c, this.last), this.callback("itemLastOutCallback", a, c, this.prevLast));
            this.callback("itemVisibleInCallback", a, c, this.first, this.last, this.prevFirst, this.prevLast);
            this.callback("itemVisibleOutCallback", a, c, this.prevFirst, this.prevLast, this.first, this.last)
        },
        callback: function (a, c, b, d, f, j, e) {
            if (!(this.options[a] == null || typeof this.options[a] != "object" && c != "onAfterAnimation")) {
                var h = typeof this.options[a] == "object" ? this.options[a][c] : this.options[a];
                if (g.isFunction(h)) {
                    var i = this;
                    if (d === void 0) h(i, b, c);
                    else if (f === void 0) this.get(d).each(function () {
                        h(i, this, d, b, c)
                    });
                    else
                        for (var a = function (a) {
                            i.get(a).each(function () {
                                h(i, this, a, b, c)
                            })
                        }, k = d; k <= f; k++) k !== null && !(k >= j && k <= e) && a(k)
                }
            }
        },
        create: function (a) {
            return this.format("<li></li>", a)
        },
        format: function (a, c) {
            for (var a = g(a), b = a.get(0).className.split(" "), d = 0; d < b.length; d++) b[d].indexOf("jcarousel-") != -1 && a.removeClass(b[d]);
            a.addClass(this.className("jcarousel-item")).addClass(this.className("jcarousel-item-" + c)).css({
                "float": this.options.rtl ? "right" : "left",
                "list-style": "none"
            }).attr("jcarouselindex", c);
            return a
        },
        className: function (a) {
            return a + " " + a + (!this.options.vertical ? "-horizontal" : "-vertical")
        },
        dimension: function (a, c) {
            var b = g(a);
            if (c == null) return !this.options.vertical ? b.outerWidth(!0) || f.intval(this.options.itemFallbackDimension) : b.outerHeight(!0) || f.intval(this.options.itemFallbackDimension);
            else {
                var d = !this.options.vertical ? c - f.intval(b.css("marginLeft")) - f.intval(b.css("marginRight")) : c - f.intval(b.css("marginTop")) - f.intval(b.css("marginBottom"));
                g(b).css(this.wh, d + "px");
                return this.dimension(b)
            }
        },
        clipping: function () {
            return !this.options.vertical ? this.clip[0].offsetWidth - f.intval(this.clip.css("borderLeftWidth")) - f.intval(this.clip.css("borderRightWidth")) : this.clip[0].offsetHeight - f.intval(this.clip.css("borderTopWidth")) - f.intval(this.clip.css("borderBottomWidth"))
        },
        index: function (a, c) {
            if (c == null) c = this.options.size;
            return Math.round(((a - 1) / c - Math.floor((a - 1) / c)) * c) + 1
        }
    });
    f.extend({
        defaults: function (a) {
            return g.extend(q, a || {})
        },
        intval: function (a) {
            a = parseInt(a, 10);
            return isNaN(a) ? 0 : a
        },
        windowLoaded: function () {
            m = !0
        }
    });
    g.fn.jcarousel = function (a) {
        if (typeof a == "string") {
            var c = g(this).data("jcarousel"),
                b = Array.prototype.slice.call(arguments, 1);
            return c[a].apply(c, b)
        } else return this.each(function () {
            var b = g(this).data("jcarousel");
            b ? (a && g.extend(b.options, a), b.reload()) : g(this).data("jcarousel", new f(this, a))
        })
    }
})(jQuery);

/*
 * customRadioCheck: jQuery plguin for checkbox and radio replacement
 * Usage: $('input[type=checkbox], input[type=radio]').customRadioCheck();
 * Author: Cedric Ruiz
 * License: MIT
 */
;
(function () {
    $.fn.customRadioCheck = function () {

        return this.each(function () {

            var $this = $(this);
            var $span = $('<span/>');

            $span.addClass('custom-' + ($this.is(':checkbox') ? 'check' : 'radio'));
            $this.is(':checked') && $span.addClass('checked'); // init
            $span.insertAfter($this);

            $this.parent('label').addClass('custom-label')
                .attr('onclick', ''); // Fix clicking label in iOS
            // hide by shifting left
            $this.css({
                position: 'absolute',
                left: '-9999px'
            });

            // Events
            $this.on({
                change: function () {

                    if ($this.is(':radio')) {
                        console.log('Changing a Radiobutton!');
                        $this.parent().siblings('label')
                            .find('.custom-radio').removeClass('checked');

                    }
                    console.log('Changing!');
                    $span.toggleClass('checked', $this.is(':checked'));
                },
                focus: function () {
                    $span.addClass('focus');
                    console.log('Fokused!');
                },
                blur: function () {
                    $span.removeClass('focus');
                    console.log('Unfokused!');
                }
            });
        });
    };
}());

/**
 * Coin Slider - Unique jQuery Image Slider
 * @version: 1.0 - (2010/04/04)
 * @requires jQuery v1.2.2 or later
 * @author Ivan Lazarevic
 * Examples and documentation at: http://workshop.rs/projects/coin-slider/

 * Licensed under MIT licence:
 *   http://www.opensource.org/licenses/mit-license.php
 **/

(function ($) {
    var params = new Array;
    var order = new Array;
    var images = new Array;
    var links = new Array;
    var linksTarget = new Array;
    var titles = new Array;
    var interval = new Array;
    var imagePos = new Array;
    var appInterval = new Array;
    var squarePos = new Array;
    var reverse = new Array;
    $.fn.coinslider = $.fn.CoinSlider = function (options) {
        init = function (el) {
            order[el.id] = new Array();
            images[el.id] = new Array();
            links[el.id] = new Array();
            linksTarget[el.id] = new Array();
            titles[el.id] = new Array();
            imagePos[el.id] = 0;
            squarePos[el.id] = 0;
            reverse[el.id] = 1;
            params[el.id] = $.extend({}, $.fn.coinslider.defaults, options);
            $.each($('#' + el.id + ' img'), function (i, item) {
                images[el.id][i] = $(item).attr('src');
                links[el.id][i] = $(item).parent().is('a') ? $(item).parent().attr('href') : '';
                linksTarget[el.id][i] = $(item).parent().is('a') ? $(item).parent().attr('target') : '';
                titles[el.id][i] = $(item).next().is('span') ? $(item).next().html() : '';
                $(item).hide();
                $(item).next().hide();
            });
            $(el).css({
                'background-image': 'url(' + images[el.id][0] + ')',
                'width': params[el.id].width,
                'height': params[el.id].height,
                'position': 'relative',
                'background-position': 'top left'
            }).wrap("<div class='coin-slider' id='coin-slider-" + el.id + "' />");
            $('#' + el.id).append("<div class='cs-title' id='cs-title-" + el.id + "' style='position: absolute; bottom:0; left: 0; z-index: 1000;'></div>");
            $.setFields(el);
            if (params[el.id].navigation)
                $.setNavigation(el);
            $.transition(el, 0);
            $.transitionCall(el);
        }
        $.setFields = function (el) {
            tWidth = sWidth = parseInt(params[el.id].width / params[el.id].spw);
            tHeight = sHeight = parseInt(params[el.id].height / params[el.id].sph);
            counter = sLeft = sTop = 0;
            tgapx = gapx = params[el.id].width - params[el.id].spw * sWidth;
            tgapy = gapy = params[el.id].height - params[el.id].sph * sHeight;
            for (i = 1; i <= params[el.id].sph; i++) {
                gapx = tgapx;
                if (gapy > 0) {
                    gapy--;
                    sHeight = tHeight + 1;
                } else {
                    sHeight = tHeight;
                }
                for (j = 1; j <= params[el.id].spw; j++) {
                    if (gapx > 0) {
                        gapx--;
                        sWidth = tWidth + 1;
                    } else {
                        sWidth = tWidth;
                    }
                    order[el.id][counter] = i + '' + j;
                    counter++;
                    if (params[el.id].links)
                        $('#' + el.id).append("<a href='" + links[el.id][0] + "' class='cs-" + el.id + "' id='cs-" + el.id + i + j + "' style='width:" + sWidth + "px; height:" + sHeight + "px; float: left; position: absolute;'></a>");
                    else
                        $('#' + el.id).append("<div class='cs-" + el.id + "' id='cs-" + el.id + i + j + "' style='width:" + sWidth + "px; height:" + sHeight + "px; float: left; position: absolute;'></div>");
                    $("#cs-" + el.id + i + j).css({
                        'background-position': -sLeft + 'px ' + (-sTop + 'px'),
                        'left': sLeft,
                        'top': sTop
                    });
                    sLeft += sWidth;
                }
                sTop += sHeight;
                sLeft = 0;
            }
            $('.cs-' + el.id).mouseover(function () {
                $('#cs-navigation-' + el.id).show();
            });
            $('.cs-' + el.id).mouseout(function () {
                $('#cs-navigation-' + el.id).hide();
            });
            $('#cs-title-' + el.id).mouseover(function () {
                $('#cs-navigation-' + el.id).show();
            });
            $('#cs-title-' + el.id).mouseout(function () {
                $('#cs-navigation-' + el.id).hide();
            });
            if (params[el.id].hoverPause) {
                $('.cs-' + el.id).mouseover(function () {
                    params[el.id].pause = true;
                });
                $('.cs-' + el.id).mouseout(function () {
                    params[el.id].pause = false;
                });
                $('#cs-title-' + el.id).mouseover(function () {
                    params[el.id].pause = true;
                });
                $('#cs-title-' + el.id).mouseout(function () {
                    params[el.id].pause = false;
                });
            }
        };
        $.transitionCall = function (el) {
            clearInterval(interval[el.id]);
            delay = params[el.id].delay + params[el.id].spw * params[el.id].sph * params[el.id].sDelay;
            interval[el.id] = setInterval(function () {
                $.transition(el)
            }, delay);
        }
        $.transition = function (el, direction) {
            if (params[el.id].pause == true) return;
            $.effect(el);
            squarePos[el.id] = 0;
            appInterval[el.id] = setInterval(function () {
                $.appereance(el, order[el.id][squarePos[el.id]])
            }, params[el.id].sDelay);
            $(el).css({
                'background-image': 'url(' + images[el.id][imagePos[el.id]] + ')'
            });
            if (typeof (direction) == "undefined")
                imagePos[el.id]++;
            else
            if (direction == 'prev')
                imagePos[el.id]--;
            else
                imagePos[el.id] = direction; if (imagePos[el.id] == images[el.id].length) {
                imagePos[el.id] = 0;
            }
            if (imagePos[el.id] == -1) {
                imagePos[el.id] = images[el.id].length - 1;
            }
            $('.cs-button-' + el.id).removeClass('cs-active');
            $('#cs-button-' + el.id + "-" + (imagePos[el.id] + 1)).addClass('cs-active');
            if (titles[el.id][imagePos[el.id]]) {
                $('#cs-title-' + el.id).css({
                    'opacity': 0
                }).animate({
                    'opacity': params[el.id].opacity
                }, params[el.id].titleSpeed);
                $('#cs-title-' + el.id).html(titles[el.id][imagePos[el.id]]);
            } else {
                $('#cs-title-' + el.id).css('opacity', 0);
            }
        };
        $.appereance = function (el, sid) {
            $('.cs-' + el.id).attr('href', links[el.id][imagePos[el.id]]).attr('target', linksTarget[el.id][imagePos[el.id]]);
            if (squarePos[el.id] == params[el.id].spw * params[el.id].sph) {
                clearInterval(appInterval[el.id]);
                return;
            }
            $('#cs-' + el.id + sid).css({
                opacity: 0,
                'background-image': 'url(' + images[el.id][imagePos[el.id]] + ')'
            });
            $('#cs-' + el.id + sid).animate({
                opacity: 1
            }, 300);
            squarePos[el.id]++;
        };
        $.setNavigation = function (el) {
            $(el).append("<div id='cs-navigation-" + el.id + "'></div>");
            $('#cs-navigation-' + el.id).hide();
            $('#cs-navigation-' + el.id).append("<a href='#' id='cs-prev-" + el.id + "' class='cs-prev'>prev</a>");
            $('#cs-navigation-' + el.id).append("<a href='#' id='cs-next-" + el.id + "' class='cs-next'>next</a>");
            $('#cs-prev-' + el.id).css({
                'position': 'absolute',
                'top': params[el.id].height / 2 - 15,
                'left': 0,
                'z-index': 1001,
                'line-height': '30px',
                'opacity': params[el.id].opacity
            }).click(function (e) {
                e.preventDefault();
                $.transition(el, 'prev');
                $.transitionCall(el);
            }).mouseover(function () {
                $('#cs-navigation-' + el.id).show()
            });
            $('#cs-next-' + el.id).css({
                'position': 'absolute',
                'top': params[el.id].height / 2 - 15,
                'right': 0,
                'z-index': 1001,
                'line-height': '30px',
                'opacity': params[el.id].opacity
            }).click(function (e) {
                e.preventDefault();
                $.transition(el);
                $.transitionCall(el);
            }).mouseover(function () {
                $('#cs-navigation-' + el.id).show()
            });
            $("<div id='cs-buttons-" + el.id + "' class='cs-buttons'></div>").appendTo($('#coin-slider-' + el.id));
            for (k = 1; k < images[el.id].length + 1; k++) {
                $('#cs-buttons-' + el.id).append("<a href='#' class='cs-button-" + el.id + "' id='cs-button-" + el.id + "-" + k + "'>" + k + "</a>");
            }
            $.each($('.cs-button-' + el.id), function (i, item) {
                $(item).click(function (e) {
                    $('.cs-button-' + el.id).removeClass('cs-active');
                    $(this).addClass('cs-active');
                    e.preventDefault();
                    $.transition(el, i);
                    $.transitionCall(el);
                })
            });
            $('#cs-navigation-' + el.id + ' a').mouseout(function () {
                $('#cs-navigation-' + el.id).hide();
                params[el.id].pause = false;
            });
            $("#cs-buttons-" + el.id).css({
                'left': '50%',
                'margin-left': -images[el.id].length * 15 / 2 - 5,
                'position': 'relative'
            });
        }
        $.effect = function (el) {
            effA = ['random', 'swirl', 'rain', 'straight'];
            if (params[el.id].effect == '')
                eff = effA[Math.floor(Math.random() * (effA.length))];
            else
                eff = params[el.id].effect;
            order[el.id] = new Array();
            if (eff == 'random') {
                counter = 0;
                for (i = 1; i <= params[el.id].sph; i++) {
                    for (j = 1; j <= params[el.id].spw; j++) {
                        order[el.id][counter] = i + '' + j;
                        counter++;
                    }
                }
                $.random(order[el.id]);
            }
            if (eff == 'rain') {
                $.rain(el);
            }
            if (eff == 'swirl')
                $.swirl(el);
            if (eff == 'straight')
                $.straight(el);
            reverse[el.id] *= -1;
            if (reverse[el.id] > 0) {
                order[el.id].reverse();
            }
        }
        $.random = function (arr) {
            var i = arr.length;
            if (i == 0) return false;
            while (--i) {
                var j = Math.floor(Math.random() * (i + 1));
                var tempi = arr[i];
                var tempj = arr[j];
                arr[i] = tempj;
                arr[j] = tempi;
            }
        }
        $.swirl = function (el) {
            var n = params[el.id].sph;
            var m = params[el.id].spw;
            var x = 1;
            var y = 1;
            var going = 0;
            var num = 0;
            var c = 0;
            var dowhile = true;
            while (dowhile) {
                num = (going == 0 || going == 2) ? m : n;
                for (i = 1; i <= num; i++) {
                    order[el.id][c] = x + '' + y;
                    c++;
                    if (i != num) {
                        switch (going) {
                        case 0:
                            y++;
                            break;
                        case 1:
                            x++;
                            break;
                        case 2:
                            y--;
                            break;
                        case 3:
                            x--;
                            break;
                        }
                    }
                }
                going = (going + 1) % 4;
                switch (going) {
                case 0:
                    m--;
                    y++;
                    break;
                case 1:
                    n--;
                    x++;
                    break;
                case 2:
                    m--;
                    y--;
                    break;
                case 3:
                    n--;
                    x--;
                    break;
                }
                check = $.max(n, m) - $.min(n, m);
                if (m <= check && n <= check)
                    dowhile = false;
            }
        }
        $.rain = function (el) {
            var n = params[el.id].sph;
            var m = params[el.id].spw;
            var c = 0;
            var to = to2 = from = 1;
            var dowhile = true;
            while (dowhile) {
                for (i = from; i <= to; i++) {
                    order[el.id][c] = i + '' + parseInt(to2 - i + 1);
                    c++;
                }
                to2++;
                if (to < n && to2 < m && n < m) {
                    to++;
                }
                if (to < n && n >= m) {
                    to++;
                }
                if (to2 > m) {
                    from++;
                }
                if (from > to) dowhile = false;
            }
        }
        $.straight = function (el) {
            counter = 0;
            for (i = 1; i <= params[el.id].sph; i++) {
                for (j = 1; j <= params[el.id].spw; j++) {
                    order[el.id][counter] = i + '' + j;
                    counter++;
                }
            }
        }
        $.min = function (n, m) {
            if (n > m) return m;
            else return n;
        }
        $.max = function (n, m) {
            if (n < m) return m;
            else return n;
        }
        this.each(function () {
            init(this);
        });
    };
    $.fn.coinslider.defaults = {
        width: 565,
        height: 290,
        spw: 7,
        sph: 5,
        delay: 3000,
        sDelay: 30,
        opacity: 0.7,
        titleSpeed: 500,
        effect: '',
        navigation: true,
        links: true,
        hoverPause: true
    };
})(jQuery);

/*!
 * FullCalendar v1.6.0
 * Docs & License: http://arshaw.com/fullcalendar/
 * (c) 2013 Adam Shaw
 */
(function (t, e) {
    function n(e) {
        t.extend(!0, ye, e)
    }

    function r(n, r, l) {
        function u(t) {
            G ? (S(), C(), R(), b(t)) : f()
        }

        function f() {
            K = r.theme ? "ui" : "fc", n.addClass("fc"), r.isRTL ? n.addClass("fc-rtl") : n.addClass("fc-ltr"), r.theme && n.addClass("ui-widget"), G = t("<div class='fc-content' style='position:relative'/>").prependTo(n), $ = new a(Z, r), Q = $.render(), Q && n.prepend(Q), y(r.defaultView), t(window).resize(x), m() || v()
        }

        function v() {
            setTimeout(function () {
                !te.start && m() && b()
            }, 0)
        }

        function h() {
            t(window).unbind("resize", x), $.destroy(), G.remove(), n.removeClass("fc fc-rtl ui-widget")
        }

        function g() {
            return 0 !== se.offsetWidth
        }

        function m() {
            return 0 !== t("body")[0].offsetWidth
        }

        function y(e) {
            if (!te || e != te.name) {
                ue++, W();
                var n, r = te;
                r ? ((r.beforeHide || I)(), q(G, G.height()), r.element.hide()) : q(G, 1), G.css("overflow", "hidden"), te = ce[e], te ? te.element.show() : te = ce[e] = new De[e](n = re = t("<div class='fc-view fc-view-" + e + "' style='position:absolute'/>").appendTo(G), Z), r && $.deactivateButton(r.name), $.activateButton(e), b(), G.css("overflow", ""), r && q(G, 1), n || (te.afterShow || I)(), ue--
            }
        }

        function b(t) {
            if (g()) {
                ue++, W(), ne === e && S();
                var r = !1;
                !te.start || t || te.start > fe || fe >= te.end ? (te.render(fe, t || 0), E(!0), r = !0) : te.sizeDirty ? (te.clearEvents(), E(), r = !0) : te.eventsDirty && (te.clearEvents(), r = !0), te.sizeDirty = !1, te.eventsDirty = !1, T(r), ee = n.outerWidth(), $.updateTitle(te.title);
                var a = new Date;
                a >= te.start && te.end > a ? $.disableButton("today") : $.enableButton("today"), ue--, te.trigger("viewDisplay", se)
            }
        }

        function M() {
            C(), g() && (S(), E(), W(), te.clearEvents(), te.renderEvents(de), te.sizeDirty = !1)
        }

        function C() {
            t.each(ce, function (t, e) {
                e.sizeDirty = !0
            })
        }

        function S() {
            ne = r.contentHeight ? r.contentHeight : r.height ? r.height - (Q ? Q.height() : 0) - L(G) : Math.round(G.width() / Math.max(r.aspectRatio, .5))
        }

        function E(t) {
            ue++, te.setHeight(ne, t), re && (re.css("position", "relative"), re = null), te.setWidth(G.width(), t), ue--
        }

        function x() {
            if (!ue)
                if (te.start) {
                    var t = ++le;
                    setTimeout(function () {
                        t == le && !ue && g() && ee != (ee = n.outerWidth()) && (ue++, M(), te.trigger("windowResize", se), ue--)
                    }, 200)
                } else v()
        }

        function T(t) {
            !r.lazyFetching || oe(te.visStart, te.visEnd) ? k() : t && F()
        }

        function k() {
            ie(te.visStart, te.visEnd)
        }

        function H(t) {
            de = t, F()
        }

        function z(t) {
            F(t)
        }

        function F(t) {
            R(), g() && (te.clearEvents(), te.renderEvents(de, t), te.eventsDirty = !1)
        }

        function R() {
            t.each(ce, function (t, e) {
                e.eventsDirty = !0
            })
        }

        function N(t, n, r) {
            te.select(t, n, r === e ? !0 : r)
        }

        function W() {
            te && te.unselect()
        }

        function A() {
            b(-1)
        }

        function _() {
            b(1)
        }

        function O() {
            i(fe, -1), b()
        }

        function B() {
            i(fe, 1), b()
        }

        function Y() {
            fe = new Date, b()
        }

        function j(t, e, n) {
            t instanceof Date ? fe = d(t) : p(fe, t, e, n), b()
        }

        function P(t, n, r) {
            t !== e && i(fe, t), n !== e && s(fe, n), r !== e && c(fe, r), b()
        }

        function J() {
            return d(fe)
        }

        function V() {
            return te
        }

        function X(t, n) {
            return n === e ? r[t] : (("height" == t || "contentHeight" == t || "aspectRatio" == t) && (r[t] = n, M()), e)
        }

        function U(t, n) {
            return r[t] ? r[t].apply(n || se, Array.prototype.slice.call(arguments, 2)) : e
        }
        var Z = this;
        Z.options = r, Z.render = u, Z.destroy = h, Z.refetchEvents = k, Z.reportEvents = H, Z.reportEventChange = z, Z.rerenderEvents = F, Z.changeView = y, Z.select = N, Z.unselect = W, Z.prev = A, Z.next = _, Z.prevYear = O, Z.nextYear = B, Z.today = Y, Z.gotoDate = j, Z.incrementDate = P, Z.formatDate = function (t, e) {
            return w(t, e, r)
        }, Z.formatDates = function (t, e, n) {
            return D(t, e, n, r)
        }, Z.getDate = J, Z.getView = V, Z.option = X, Z.trigger = U, o.call(Z, r, l);
        var $, Q, G, K, te, ee, ne, re, ae, oe = Z.isFetchNeeded,
            ie = Z.fetchEvents,
            se = n[0],
            ce = {}, le = 0,
            ue = 0,
            fe = new Date,
            de = [];
        p(fe, r.year, r.month, r.date), r.droppable && t(document).bind("dragstart", function (e, n) {
            var a = e.target,
                o = t(a);
            if (!o.parents(".fc").length) {
                var i = r.dropAccept;
                (t.isFunction(i) ? i.call(a, o) : o.is(i)) && (ae = a, te.dragStart(ae, e, n))
            }
        }).bind("dragstop", function (t, e) {
            ae && (te.dragStop(ae, t, e), ae = null)
        })
    }

    function a(n, r) {
        function a() {
            v = r.theme ? "ui" : "fc";
            var n = r.header;
            return n ? h = t("<table class='fc-header' style='width:100%'/>").append(t("<tr/>").append(i("left")).append(i("center")).append(i("right"))) : e
        }

        function o() {
            h.remove()
        }

        function i(e) {
            var a = t("<td class='fc-header-" + e + "'/>"),
                o = r.header[e];
            return o && t.each(o.split(" "), function (e) {
                e > 0 && a.append("<span class='fc-header-space'/>");
                var o;
                t.each(this.split(","), function (e, i) {
                    if ("title" == i) a.append("<span class='fc-header-title'><h2>&nbsp;</h2></span>"), o && o.addClass(v + "-corner-right"), o = null;
                    else {
                        var s;
                        if (n[i] ? s = n[i] : De[i] && (s = function () {
                            u.removeClass(v + "-state-hover"), n.changeView(i)
                        }), s) {
                            var c = r.theme ? J(r.buttonIcons, i) : null,
                                l = J(r.buttonText, i),
                                u = t("<span class='fc-button fc-button-" + i + " " + v + "-state-default'>" + (c ? "<span class='fc-icon-wrap'><span class='ui-icon ui-icon-" + c + "'/>" + "</span>" : l) + "</span>").click(function () {
                                    u.hasClass(v + "-state-disabled") || s()
                                }).mousedown(function () {
                                    u.not("." + v + "-state-active").not("." + v + "-state-disabled").addClass(v + "-state-down")
                                }).mouseup(function () {
                                    u.removeClass(v + "-state-down")
                                }).hover(function () {
                                    u.not("." + v + "-state-active").not("." + v + "-state-disabled").addClass(v + "-state-hover")
                                }, function () {
                                    u.removeClass(v + "-state-hover").removeClass(v + "-state-down")
                                }).appendTo(a);
                            U(u), o || u.addClass(v + "-corner-left"), o = u
                        }
                    }
                }), o && o.addClass(v + "-corner-right")
            }), a
        }

        function s(t) {
            h.find("h2").html(t)
        }

        function c(t) {
            h.find("span.fc-button-" + t).addClass(v + "-state-active")
        }

        function l(t) {
            h.find("span.fc-button-" + t).removeClass(v + "-state-active")
        }

        function u(t) {
            h.find("span.fc-button-" + t).addClass(v + "-state-disabled")
        }

        function f(t) {
            h.find("span.fc-button-" + t).removeClass(v + "-state-disabled")
        }
        var d = this;
        d.render = a, d.destroy = o, d.updateTitle = s, d.activateButton = c, d.deactivateButton = l, d.disableButton = u, d.enableButton = f;
        var v, h = t([])
    }

    function o(n, r) {
        function a(t, e) {
            return !S || S > t || e > E
        }

        function o(t, e) {
            S = t, E = e, W = [];
            var n = ++F,
                r = z.length;
            R = r;
            for (var a = 0; r > a; a++) i(z[a], n)
        }

        function i(e, r) {
            s(e, function (a) {
                if (r == F) {
                    if (a) {
                        n.eventDataTransform && (a = t.map(a, n.eventDataTransform)), e.eventDataTransform && (a = t.map(a, e.eventDataTransform));
                        for (var o = 0; a.length > o; o++) a[o].source = e, b(a[o]);
                        W = W.concat(a)
                    }
                    R--, R || k(W)
                }
            })
        }

        function s(r, a) {
            var o, i, c = we.sourceFetchers;
            for (o = 0; c.length > o; o++) {
                if (i = c[o](r, S, E, a), i === !0) return;
                if ("object" == typeof i) return s(i, a), e
            }
            var l = r.events;
            if (l) t.isFunction(l) ? (p(), l(d(S), d(E), function (t) {
                a(t), y()
            })) : t.isArray(l) ? a(l) : a();
            else {
                var u = r.url;
                if (u) {
                    var f = r.success,
                        v = r.error,
                        h = r.complete,
                        g = t.extend({}, r.data || {}),
                        m = K(r.startParam, n.startParam),
                        b = K(r.endParam, n.endParam);
                    m && (g[m] = Math.round(+S / 1e3)), b && (g[b] = Math.round(+E / 1e3)), p(), t.ajax(t.extend({}, Me, r, {
                        data: g,
                        success: function (e) {
                            e = e || [];
                            var n = G(f, this, arguments);
                            t.isArray(n) && (e = n), a(e)
                        },
                        error: function () {
                            G(v, this, arguments), a()
                        },
                        complete: function () {
                            G(h, this, arguments), y()
                        }
                    }))
                } else a()
            }
        }

        function c(t) {
            t = l(t), t && (R++, i(t, F))
        }

        function l(n) {
            return t.isFunction(n) || t.isArray(n) ? n = {
                events: n
            } : "string" == typeof n && (n = {
                url: n
            }), "object" == typeof n ? (w(n), z.push(n), n) : e
        }

        function u(e) {
            z = t.grep(z, function (t) {
                return !D(t, e)
            }), W = t.grep(W, function (t) {
                return !D(t.source, e)
            }), k(W)
        }

        function f(t) {
            var e, n, r = W.length,
                a = T().defaultEventEnd,
                o = t.start - t._start,
                i = t.end ? t.end - (t._end || a(t)) : 0;
            for (e = 0; r > e; e++) n = W[e], n._id == t._id && n != t && (n.start = new Date(+n.start + o), n.end = t.end ? n.end ? new Date(+n.end + i) : new Date(+a(n) + i) : null, n.title = t.title, n.url = t.url, n.allDay = t.allDay, n.className = t.className, n.editable = t.editable, n.color = t.color, n.backgroudColor = t.backgroudColor, n.borderColor = t.borderColor, n.textColor = t.textColor, b(n));
            b(t), k(W)
        }

        function v(t, e) {
            b(t), t.source || (e && (H.events.push(t), t.source = H), W.push(t)), k(W)
        }

        function h(e) {
            if (e) {
                if (!t.isFunction(e)) {
                    var n = e + "";
                    e = function (t) {
                        return t._id == n
                    }
                }
                W = t.grep(W, e, !0);
                for (var r = 0; z.length > r; r++) t.isArray(z[r].events) && (z[r].events = t.grep(z[r].events, e, !0))
            } else {
                W = [];
                for (var r = 0; z.length > r; r++) t.isArray(z[r].events) && (z[r].events = [])
            }
            k(W)
        }

        function g(e) {
            return t.isFunction(e) ? t.grep(W, e) : e ? (e += "", t.grep(W, function (t) {
                return t._id == e
            })) : W
        }

        function p() {
            N++ || x("loading", null, !0)
        }

        function y() {
            --N || x("loading", null, !1)
        }

        function b(t) {
            var r = t.source || {}, a = K(r.ignoreTimezone, n.ignoreTimezone);
            t._id = t._id || (t.id === e ? "_fc" + Ce++ : t.id + ""), t.date && (t.start || (t.start = t.date), delete t.date), t._start = d(t.start = m(t.start, a)), t.end = m(t.end, a), t.end && t.end <= t.start && (t.end = null), t._end = t.end ? d(t.end) : null, t.allDay === e && (t.allDay = K(r.allDayDefault, n.allDayDefault)), t.className ? "string" == typeof t.className && (t.className = t.className.split(/\s+/)) : t.className = []
        }

        function w(t) {
            t.className ? "string" == typeof t.className && (t.className = t.className.split(/\s+/)) : t.className = [];
            for (var e = we.sourceNormalizers, n = 0; e.length > n; n++) e[n](t)
        }

        function D(t, e) {
            return t && e && M(t) == M(e)
        }

        function M(t) {
            return ("object" == typeof t ? t.events || t.url : "") || t
        }
        var C = this;
        C.isFetchNeeded = a, C.fetchEvents = o, C.addEventSource = c, C.removeEventSource = u, C.updateEvent = f, C.renderEvent = v, C.removeEvents = h, C.clientEvents = g, C.normalizeEvent = b;
        for (var S, E, x = C.trigger, T = C.getView, k = C.reportEvents, H = {
                events: []
            }, z = [H], F = 0, R = 0, N = 0, W = [], A = 0; r.length > A; A++) l(r[A])
    }

    function i(t, e, n) {
        return t.setFullYear(t.getFullYear() + e), n || f(t), t
    }

    function s(t, e, n) {
        if (+t) {
            var r = t.getMonth() + e,
                a = d(t);
            for (a.setDate(1), a.setMonth(r), t.setMonth(r), n || f(t); t.getMonth() != a.getMonth();) t.setDate(t.getDate() + (a > t ? 1 : -1))
        }
        return t
    }

    function c(t, e, n) {
        if (+t) {
            var r = t.getDate() + e,
                a = d(t);
            a.setHours(9), a.setDate(r), t.setDate(r), n || f(t), l(t, a)
        }
        return t
    }

    function l(t, e) {
        if (+t)
            for (; t.getDate() != e.getDate();) t.setTime(+t + (e > t ? 1 : -1) * xe)
    }

    function u(t, e) {
        return t.setMinutes(t.getMinutes() + e), t
    }

    function f(t) {
        return t.setHours(0), t.setMinutes(0), t.setSeconds(0), t.setMilliseconds(0), t
    }

    function d(t, e) {
        return e ? f(new Date(+t)) : new Date(+t)
    }

    function v() {
        var t, e = 0;
        do t = new Date(1970, e++, 1); while (t.getHours());
        return t
    }

    function h(t, e, n) {
        for (e = e || 1; !t.getDay() || n && 1 == t.getDay() || !n && 6 == t.getDay();) c(t, e);
        return t
    }

    function g(t, e) {
        return Math.round((d(t, !0) - d(e, !0)) / Ee)
    }

    function p(t, n, r, a) {
        n !== e && n != t.getFullYear() && (t.setDate(1), t.setMonth(0), t.setFullYear(n)), r !== e && r != t.getMonth() && (t.setDate(1), t.setMonth(r)), a !== e && t.setDate(a)
    }

    function m(t, n) {
        return "object" == typeof t ? t : "number" == typeof t ? new Date(1e3 * t) : "string" == typeof t ? t.match(/^\d+(\.\d+)?$/) ? new Date(1e3 * parseFloat(t)) : (n === e && (n = !0), y(t, n) || (t ? new Date(t) : null)) : null
    }

    function y(t, e) {
        var n = t.match(/^([0-9]{4})(-([0-9]{2})(-([0-9]{2})([T ]([0-9]{2}):([0-9]{2})(:([0-9]{2})(\.([0-9]+))?)?(Z|(([-+])([0-9]{2})(:?([0-9]{2}))?))?)?)?)?$/);
        if (!n) return null;
        var r = new Date(n[1], 0, 1);
        if (e || !n[13]) {
            var a = new Date(n[1], 0, 1, 9, 0);
            n[3] && (r.setMonth(n[3] - 1), a.setMonth(n[3] - 1)), n[5] && (r.setDate(n[5]), a.setDate(n[5])), l(r, a), n[7] && r.setHours(n[7]), n[8] && r.setMinutes(n[8]), n[10] && r.setSeconds(n[10]), n[12] && r.setMilliseconds(1e3 * Number("0." + n[12])), l(r, a)
        } else if (r.setUTCFullYear(n[1], n[3] ? n[3] - 1 : 0, n[5] || 1), r.setUTCHours(n[7] || 0, n[8] || 0, n[10] || 0, n[12] ? 1e3 * Number("0." + n[12]) : 0), n[14]) {
            var o = 60 * Number(n[16]) + (n[18] ? Number(n[18]) : 0);
            o *= "-" == n[15] ? 1 : -1, r = new Date(+r + 1e3 * 60 * o)
        }
        return r
    }

    function b(t) {
        if ("number" == typeof t) return 60 * t;
        if ("object" == typeof t) return 60 * t.getHours() + t.getMinutes();
        var e = t.match(/(\d+)(?::(\d+))?\s*(\w+)?/);
        if (e) {
            var n = parseInt(e[1], 10);
            return e[3] && (n %= 12, "p" == e[3].toLowerCase().charAt(0) && (n += 12)), 60 * n + (e[2] ? parseInt(e[2], 10) : 0)
        }
    }

    function w(t, e, n) {
        return D(t, null, e, n)
    }

    function D(t, e, n, r) {
        r = r || ye;
        var a, o, i, s, c = t,
            l = e,
            u = n.length,
            f = "";
        for (a = 0; u > a; a++)
            if (o = n.charAt(a), "'" == o) {
                for (i = a + 1; u > i; i++)
                    if ("'" == n.charAt(i)) {
                        c && (f += i == a + 1 ? "'" : n.substring(a + 1, i), a = i);
                        break
                    }
            } else if ("(" == o) {
            for (i = a + 1; u > i; i++)
                if (")" == n.charAt(i)) {
                    var d = w(c, n.substring(a + 1, i), r);
                    parseInt(d.replace(/\D/, ""), 10) && (f += d), a = i;
                    break
                }
        } else if ("[" == o) {
            for (i = a + 1; u > i; i++)
                if ("]" == n.charAt(i)) {
                    var v = n.substring(a + 1, i),
                        d = w(c, v, r);
                    d != w(l, v, r) && (f += d), a = i;
                    break
                }
        } else if ("{" == o) c = e, l = t;
        else if ("}" == o) c = t, l = e;
        else {
            for (i = u; i > a; i--)
                if (s = ke[n.substring(a, i)]) {
                    c && (f += s(c, r)), a = i - 1;
                    break
                }
            i == a && c && (f += o)
        }
        return f
    }

    function M(t) {
        var e, n = new Date(t.getTime());
        return n.setDate(n.getDate() + 4 - (n.getDay() || 7)), e = n.getTime(), n.setMonth(0), n.setDate(1), Math.floor(Math.round((e - n) / 864e5) / 7) + 1
    }

    function C(t) {
        return t.end ? S(t.end, t.allDay) : c(d(t.start), 1)
    }

    function S(t, e) {
        return t = d(t), e || t.getHours() || t.getMinutes() ? c(t, 1) : f(t)
    }

    function E(t, e) {
        return 100 * (e.msLength - t.msLength) + (t.event.start - e.event.start)
    }

    function x(t, e) {
        return t.end > e.start && t.start < e.end
    }

    function T(t, e, n, r) {
        var a, o, i, s, c, l, u, f, v = [],
            h = t.length;
        for (a = 0; h > a; a++) o = t[a], i = o.start, s = e[a], s > n && r > i && (n > i ? (c = d(n), u = !1) : (c = i, u = !0), s > r ? (l = d(r), f = !1) : (l = s, f = !0), v.push({
            event: o,
            start: c,
            end: l,
            isStart: u,
            isEnd: f,
            msLength: l - c
        }));
        return v.sort(E)
    }

    function k(t) {
        var e, n, r, a, o, i = [],
            s = t.length;
        for (e = 0; s > e; e++) {
            for (n = t[e], r = 0;;) {
                if (a = !1, i[r])
                    for (o = 0; i[r].length > o; o++)
                        if (x(i[r][o], n)) {
                            a = !0;
                            break
                        }
                if (!a) break;
                r++
            }
            i[r] ? i[r].push(n) : i[r] = [n]
        }
        return i
    }

    function H(n, r, a) {
        n.unbind("mouseover").mouseover(function (n) {
            for (var o, i, s, c = n.target; c != this;) o = c, c = c.parentNode;
            (i = o._fci) !== e && (o._fci = e, s = r[i], a(s.event, s.element, s), t(n.target).trigger(n)), n.stopPropagation()
        })
    }

    function z(e, n, r) {
        for (var a, o = 0; e.length > o; o++) a = t(e[o]), a.width(Math.max(0, n - R(a, r)))
    }

    function F(e, n, r) {
        for (var a, o = 0; e.length > o; o++) a = t(e[o]), a.height(Math.max(0, n - L(a, r)))
    }

    function R(t, e) {
        return N(t) + A(t) + (e ? W(t) : 0)
    }

    function N(e) {
        return (parseFloat(t.css(e[0], "paddingLeft", !0)) || 0) + (parseFloat(t.css(e[0], "paddingRight", !0)) || 0)
    }

    function W(e) {
        return (parseFloat(t.css(e[0], "marginLeft", !0)) || 0) + (parseFloat(t.css(e[0], "marginRight", !0)) || 0)
    }

    function A(e) {
        return (parseFloat(t.css(e[0], "borderLeftWidth", !0)) || 0) + (parseFloat(t.css(e[0], "borderRightWidth", !0)) || 0)
    }

    function L(t, e) {
        return _(t) + B(t) + (e ? O(t) : 0)
    }

    function _(e) {
        return (parseFloat(t.css(e[0], "paddingTop", !0)) || 0) + (parseFloat(t.css(e[0], "paddingBottom", !0)) || 0)
    }

    function O(e) {
        return (parseFloat(t.css(e[0], "marginTop", !0)) || 0) + (parseFloat(t.css(e[0], "marginBottom", !0)) || 0)
    }

    function B(e) {
        return (parseFloat(t.css(e[0], "borderTopWidth", !0)) || 0) + (parseFloat(t.css(e[0], "borderBottomWidth", !0)) || 0)
    }

    function q(t, e) {
        e = "number" == typeof e ? e + "px" : e, t.each(function (t, n) {
            n.style.cssText += ";min-height:" + e + ";_height:" + e
        })
    }

    function I() {}

    function Y(t, e) {
        return t - e
    }

    function j(t) {
        return Math.max.apply(Math, t)
    }

    function P(t) {
        return (10 > t ? "0" : "") + t
    }

    function J(t, n) {
        if (t[n] !== e) return t[n];
        for (var r, a = n.split(/(?=[A-Z])/), o = a.length - 1; o >= 0; o--)
            if (r = t[a[o].toLowerCase()], r !== e) return r;
        return t[""]
    }

    function V(t) {
        return t.replace(/&/g, "&amp;").replace(/</g, "&lt;").replace(/>/g, "&gt;").replace(/'/g, "&#039;").replace(/"/g, "&quot;").replace(/\n/g, "<br />")
    }

    function X(t) {
        return t.id + "/" + t.className + "/" + t.style.cssText.replace(/(^|;)\s*(top|left|width|height)\s*:[^;]*/gi, "")
    }

    function U(t) {
        t.attr("unselectable", "on").css("MozUserSelect", "none").bind("selectstart.ui", function () {
            return !1
        })
    }

    function Z(t) {
        t.children().removeClass("fc-first fc-last").filter(":first-child").addClass("fc-first").end().filter(":last-child").addClass("fc-last")
    }

    function $(t, e) {
        t.each(function (t, n) {
            n.className = n.className.replace(/^fc-\w*/, "fc-" + Se[e.getDay()])
        })
    }

    function Q(t, e) {
        var n = t.source || {}, r = t.color,
            a = n.color,
            o = e("eventColor"),
            i = t.backgroundColor || r || n.backgroundColor || a || e("eventBackgroundColor") || o,
            s = t.borderColor || r || n.borderColor || a || e("eventBorderColor") || o,
            c = t.textColor || n.textColor || e("eventTextColor"),
            l = [];
        return i && l.push("background-color:" + i), s && l.push("border-color:" + s), c && l.push("color:" + c), l.join(";")
    }

    function G(e, n, r) {
        if (t.isFunction(e) && (e = [e]), e) {
            var a, o;
            for (a = 0; e.length > a; a++) o = e[a].apply(n, r) || o;
            return o
        }
    }

    function K() {
        for (var t = 0; arguments.length > t; t++)
            if (arguments[t] !== e) return arguments[t]
    }

    function te(t, e) {
        function n(t, e) {
            e && (s(t, e), t.setDate(1));
            var n = d(t, !0);
            n.setDate(1);
            var l = s(d(n), 1),
                u = d(n),
                f = d(l),
                v = a("firstDay"),
                g = a("weekends") ? 0 : 1;
            g && (h(u), h(f, -1, !0)), c(u, -((u.getDay() - Math.max(v, g) + 7) % 7)), c(f, (7 - f.getDay() + Math.max(v, g)) % 7);
            var p = Math.round((f - u) / (7 * Ee));
            "fixed" == a("weekMode") && (c(f, 7 * (6 - p)), p = 6), r.title = i(n, a("titleFormat")), r.start = n, r.end = l, r.visStart = u, r.visEnd = f, o(p, g ? 5 : 7, !0)
        }
        var r = this;
        r.render = n, re.call(r, t, e, "month");
        var a = r.opt,
            o = r.renderBasic,
            i = e.formatDate
    }

    function ee(t, e) {
        function n(t, e) {
            e && c(t, 7 * e);
            var n = c(d(t), -((t.getDay() - a("firstDay") + 7) % 7)),
                s = c(d(n), 7),
                l = d(n),
                u = d(s),
                f = a("weekends");
            f || (h(l), h(u, -1, !0)), r.title = i(l, c(d(u), -1), a("titleFormat")), r.start = n, r.end = s, r.visStart = l, r.visEnd = u, o(1, f ? 7 : 5, !1)
        }
        var r = this;
        r.render = n, re.call(r, t, e, "basicWeek");
        var a = r.opt,
            o = r.renderBasic,
            i = e.formatDates
    }

    function ne(t, e) {
        function n(t, e) {
            e && (c(t, e), a("weekends") || h(t, 0 > e ? -1 : 1)), r.title = i(t, a("titleFormat")), r.start = r.visStart = d(t, !0), r.end = r.visEnd = c(d(r.start), 1), o(1, 1, !1)
        }
        var r = this;
        r.render = n, re.call(r, t, e, "basicDay");
        var a = r.opt,
            o = r.renderBasic,
            i = e.formatDate
    }

    function re(e, n, r) {
        function a(t, e, n) {
            ne = t, re = e, o();
            var r = !P;
            r ? i() : Te(), s(n)
        }

        function o() {
            ce = Ee("isRTL"), ce ? (le = -1, fe = re - 1) : (le = 1, fe = 0), pe = Ee("firstDay"), ye = Ee("weekends") ? 0 : 1, be = Ee("theme") ? "ui" : "fc", we = Ee("columnFormat"), De = Ee("weekNumbers"), Me = Ee("weekNumberTitle"), Ce = "iso" != Ee("weekNumberCalculation") ? "w" : "W"
        }

        function i() {
            Q = t("<div style='position:absolute;z-index:8;top:0;left:0'/>").appendTo(e)
        }

        function s(n) {
            var r, a, o, i, s = "",
                c = be + "-widget-header",
                l = be + "-widget-content",
                u = B.start.getMonth(),
                d = f(new Date);
            for (s += "<table class='fc-border-separate' style='width:100%' cellspacing='0'><thead><tr>", De && (s += "<th class='fc-week-number " + c + "'/>"), r = 0; re > r; r++) s += "<th class='fc-day-header fc-" + Se[r] + " " + c + "'/>";
            for (s += "</tr></thead><tbody>", r = 0; ne > r; r++) {
                for (s += "<tr class='fc-week'>", De && (s += "<td class='fc-week-number " + l + "'>" + "<div/>" + "</td>"), a = 0; re > a; a++) o = F(r, a), i = ["fc-day", "fc-" + Se[o.getDay()], l], o.getMonth() != u && i.push("fc-other-month"), +o == +d && (i.push("fc-today"), i.push(be + "-state-highlight")), s += "<td class='" + i.join(" ") + "'" + " data-date='" + Fe(o, "yyyy-MM-dd") + "'" + ">" + "<div>", n && (s += "<div class='fc-day-number'>" + o.getDate() + "</div>"), s += "<div class='fc-day-content'><div style='position:relative'>&nbsp;</div></div></div></td>";
                s += "</tr>"
            }
            s += "</tbody></table>", _(), I && I.remove(), I = t(s).appendTo(e), Y = I.find("thead"), j = Y.find(".fc-day-header"), P = I.find("tbody"), J = P.find("tr"), V = P.find(".fc-day"), X = J.find("td:first-child"), $ = J.eq(0).find(".fc-day-content > div"), Z(Y.add(Y.find("tr"))), Z(J), J.eq(0).addClass("fc-first"), J.filter(":last").addClass("fc-last"), De && Y.find(".fc-week-number").text(Me), j.each(function (e, n) {
                var r = R(e);
                t(n).text(Fe(r, we))
            }), De && P.find(".fc-week-number > div").each(function (e, n) {
                var r = F(e, 0);
                t(n).text(Fe(r, Ce))
            }), V.each(function (e, n) {
                var r = R(e);
                xe("dayRender", B, r, t(n))
            }), v(V)
        }

        function l(e) {
            K = e;
            var n, r, a, o = K - Y.height();
            "variable" == Ee("weekMode") ? n = r = Math.floor(o / (1 == ne ? 2 : 6)) : (n = Math.floor(o / ne), r = o - n * (ne - 1)), X.each(function (e, o) {
                ne > e && (a = t(o), q(a.find("> div"), (e == ne - 1 ? r : n) - L(a)))
            }), O()
        }

        function u(t) {
            G = t, se.clear(), ee = 0, De && (ee = Y.find("th.fc-week-number").outerWidth()), te = Math.floor((G - ee) / re), z(j.slice(0, -1), te)
        }

        function v(t) {
            t.click(h).mousedown(ze)
        }

        function h(e) {
            if (!Ee("selectable")) {
                var n = y(t(this).data("date"));
                xe("dayClick", this, n, !0, e)
            }
        }

        function p(t, e, n) {
            n && oe.build();
            for (var r = d(B.visStart), a = c(d(r), re), o = 0; ne > o; o++) {
                var i = new Date(Math.max(r, t)),
                    s = new Date(Math.min(a, e));
                if (s > i) {
                    var l, u;
                    ce ? (l = g(s, r) * le + fe + 1, u = g(i, r) * le + fe + 1) : (l = g(i, r), u = g(s, r)), v(m(o, l, o, u - 1))
                }
                c(r, 7), c(a, 7)
            }
        }

        function m(t, n, r, a) {
            var o = oe.rect(t, n, r, a, e);
            return ke(o, e)
        }

        function b(t) {
            return d(t)
        }

        function w(t, e) {
            p(t, c(d(e), 1), !0)
        }

        function D() {
            He()
        }

        function M(t, e, n) {
            var r = k(t),
                a = V[r.row * re + r.col];
            xe("dayClick", a, t, e, n)
        }

        function C(t, e) {
            ie.start(function (t) {
                He(), t && m(t.row, t.col, t.row, t.col)
            }, e)
        }

        function S(t, e, n) {
            var r = ie.stop();
            if (He(), r) {
                var a = H(r);
                xe("drop", t, a, !0, e, n)
            }
        }

        function E(t) {
            return d(t.start)
        }

        function x(t) {
            return se.left(t)
        }

        function T(t) {
            return se.right(t)
        }

        function k(t) {
            return {
                row: Math.floor(g(t, B.visStart) / 7),
                col: N(t.getDay())
            }
        }

        function H(t) {
            return F(t.row, t.col)
        }

        function F(t, e) {
            return c(d(B.visStart), 7 * t + e * le + fe)
        }

        function R(t) {
            return F(Math.floor(t / re), t % re)
        }

        function N(t) {
            return (t - Math.max(pe, ye) + re) % re * le + fe
        }

        function W(t) {
            return J.eq(t)
        }

        function A() {
            var t = 0;
            return De && (t += ee), {
                left: t,
                right: G
            }
        }

        function _() {
            q(e, e.height())
        }

        function O() {
            q(e, 1)
        }
        var B = this;
        B.renderBasic = a, B.setHeight = l, B.setWidth = u, B.renderDayOverlay = p, B.defaultSelectionEnd = b, B.renderSelection = w, B.clearSelection = D, B.reportDayClick = M, B.dragStart = C, B.dragStop = S, B.defaultEventEnd = E, B.getHoverListener = function () {
            return ie
        }, B.colContentLeft = x, B.colContentRight = T, B.dayOfWeekCol = N, B.dateCell = k, B.cellDate = H, B.cellIsAllDay = function () {
            return !0
        }, B.allDayRow = W, B.allDayBounds = A, B.getRowCnt = function () {
            return ne
        }, B.getColCnt = function () {
            return re
        }, B.getColWidth = function () {
            return te
        }, B.getDaySegmentContainer = function () {
            return Q
        }, ue.call(B, e, n, r), ve.call(B), de.call(B), ae.call(B);
        var I, Y, j, P, J, V, X, $, Q, G, K, te, ee, ne, re, oe, ie, se, ce, le, fe, pe, ye, be, we, De, Me, Ce, Ee = B.opt,
            xe = B.trigger,
            Te = B.clearEvents,
            ke = B.renderOverlay,
            He = B.clearOverlays,
            ze = B.daySelectionMousedown,
            Fe = n.formatDate;
        U(e.addClass("fc-grid")), oe = new he(function (e, n) {
            var r, a, o;
            j.each(function (e, i) {
                r = t(i), a = r.offset().left, e && (o[1] = a), o = [a], n[e] = o
            }), o[1] = a + r.outerWidth(), J.each(function (n, i) {
                ne > n && (r = t(i), a = r.offset().top, n && (o[1] = a), o = [a], e[n] = o)
            }), o[1] = a + r.outerHeight()
        }), ie = new ge(oe), se = new me(function (t) {
            return $.eq(t)
        })
    }

    function ae() {
        function e(t, e) {
            v(t), x(r(t), e), l("eventAfterAllRender")
        }

        function n() {
            h(), b().empty()
        }

        function r(e) {
            var n, r, a, o, s, l, u = S(),
                f = E(),
                v = d(i.visStart),
                h = c(d(v), f),
                g = t.map(e, C),
                p = [];
            for (n = 0; u > n; n++) {
                for (r = k(T(e, g, v, h)), a = 0; r.length > a; a++)
                    for (o = r[a], s = 0; o.length > s; s++) l = o[s], l.row = n, l.level = a, p.push(l);
                c(v, 7), c(h, 7)
            }
            return p
        }

        function a(t, e, n) {
            u(t) && o(t, e), n.isEnd && f(t) && H(t, e, n), g(t, e)
        }

        function o(t, e) {
            var n, r = w();
            e.draggable({
                zIndex: 9,
                delay: 50,
                opacity: s("dragOpacity"),
                revertDuration: s("dragRevertDuration"),
                start: function (a, o) {
                    l("eventDragStart", e, t, a, o), m(t, e), r.start(function (r, a, o, i) {
                        e.draggable("option", "revert", !r || !o && !i), M(), r ? (n = 7 * o + i * (s("isRTL") ? -1 : 1), D(c(d(t.start), n), c(C(t), n))) : n = 0
                    }, a, "drag")
                },
                stop: function (a, o) {
                    r.stop(), M(), l("eventDragStop", e, t, a, o), n ? y(this, t, n, 0, t.allDay, a, o) : (e.css("filter", ""), p(t, e))
                }
            })
        }
        var i = this;
        i.renderEvents = e, i.compileDaySegs = r, i.clearEvents = n, i.bindDaySeg = a, fe.call(i);
        var s = i.opt,
            l = i.trigger,
            u = i.isEventDraggable,
            f = i.isEventResizable,
            v = i.reportEvents,
            h = i.reportEventClear,
            g = i.eventElementHandlers,
            p = i.showEvents,
            m = i.hideEvents,
            y = i.eventDrop,
            b = i.getDaySegmentContainer,
            w = i.getHoverListener,
            D = i.renderDayOverlay,
            M = i.clearOverlays,
            S = i.getRowCnt,
            E = i.getColCnt,
            x = i.renderDaySegs,
            H = i.resizableDayEvent
    }

    function oe(t, e) {
        function n(t, e) {
            e && c(t, 7 * e);
            var n = c(d(t), -((t.getDay() - a("firstDay") + 7) % 7)),
                s = c(d(n), 7),
                l = d(n),
                u = d(s),
                f = a("weekends");
            f || (h(l), h(u, -1, !0)), r.title = i(l, c(d(u), -1), a("titleFormat")), r.start = n, r.end = s, r.visStart = l, r.visEnd = u, o(f ? 7 : 5)
        }
        var r = this;
        r.render = n, se.call(r, t, e, "agendaWeek");
        var a = r.opt,
            o = r.renderAgenda,
            i = e.formatDates
    }

    function ie(t, e) {
        function n(t, e) {
            e && (c(t, e), a("weekends") || h(t, 0 > e ? -1 : 1));
            var n = d(t, !0),
                s = c(d(n), 1);
            r.title = i(t, a("titleFormat")), r.start = r.visStart = n, r.end = r.visEnd = s, o(1)
        }
        var r = this;
        r.render = n, se.call(r, t, e, "agendaDay");
        var a = r.opt,
            o = r.renderAgenda,
            i = e.formatDate
    }

    function se(n, r, a) {
        function o(t) {
            Le = t, i(), te ? nn() : s(), l()
        }

        function i() {
            Ye = tn("theme") ? "ui" : "fc", Pe = tn("weekends") ? 0 : 1, je = tn("firstDay"), (Je = tn("isRTL")) ? (Ve = -1, Xe = Le - 1) : (Ve = 1, Xe = 0), Ue = b(tn("minTime")), Ze = b(tn("maxTime")), $e = tn("columnFormat"), Qe = tn("weekNumbers"), Ge = tn("weekNumberTitle"), Ke = "iso" != tn("weekNumberCalculation") ? "w" : "W", Ne = tn("snapMinutes") || tn("slotMinutes")
        }

        function s() {
            var e, r, a, o, i, s = Ye + "-widget-header",
                c = Ye + "-widget-content",
                l = 0 == tn("slotMinutes") % 15;
            for (e = "<table style='width:100%' class='fc-agenda-days fc-border-separate' cellspacing='0'><thead><tr>", e += Qe ? "<th class='fc-agenda-axis fc-week-number " + s + "'/>" : "<th class='fc-agenda-axis " + s + "'>&nbsp;</th>", r = 0; Le > r; r++) e += "<th class='fc- fc-col" + r + " " + s + "'/>";
            for (e += "<th class='fc-agenda-gutter " + s + "'>&nbsp;</th>" + "</tr>" + "</thead>" + "<tbody>" + "<tr>" + "<th class='fc-agenda-axis " + s + "'>&nbsp;</th>", r = 0; Le > r; r++) e += "<td class='fc- fc-col" + r + " " + c + "'>" + "<div>" + "<div class='fc-day-content'>" + "<div style='position:relative'>&nbsp;</div>" + "</div>" + "</div>" + "</td>";
            for (e += "<td class='fc-agenda-gutter " + c + "'>&nbsp;</td>" + "</tr>" + "</tbody>" + "</table>", te = t(e).appendTo(n), ee = te.find("thead"), ne = ee.find("th").slice(1, -1), re = te.find("tbody"), ae = re.find("td").slice(0, -1), oe = ae.find("div.fc-day-content div"), ie = ae.eq(0), se = ie.find("> div"), Z(ee.add(ee.find("tr"))), Z(re.add(re.find("tr"))), Se = ee.find("th:first"), Ee = te.find(".fc-agenda-gutter"), le = t("<div style='position:absolute;z-index:2;left:0;width:100%'/>").appendTo(n), tn("allDaySlot") ? (fe = t("<div style='position:absolute;z-index:8;top:0;left:0'/>").appendTo(le), e = "<table style='width:100%' class='fc-agenda-allday' cellspacing='0'><tr><th class='" + s + " fc-agenda-axis'>" + tn("allDayText") + "</th>" + "<td>" + "<div class='fc-day-content'><div style='position:relative'/></div>" + "</td>" + "<th class='" + s + " fc-agenda-gutter'>&nbsp;</th>" + "</tr>" + "</table>", pe = t(e).appendTo(le), ye = pe.find("tr"), D(ye.find("td")), Se = Se.add(pe.find("th:first")), Ee = Ee.add(pe.find("th.fc-agenda-gutter")), le.append("<div class='fc-agenda-divider " + s + "'>" + "<div class='fc-agenda-divider-inner'/>" + "</div>")) : fe = t([]), be = t("<div style='position:absolute;width:100%;overflow-x:hidden;overflow-y:auto'/>").appendTo(le), we = t("<div style='position:relative;width:100%;overflow:hidden'/>").appendTo(be), De = t("<div style='position:absolute;z-index:8;top:0;left:0'/>").appendTo(we), e = "<table class='fc-agenda-slots' style='width:100%' cellspacing='0'><tbody>", a = v(), o = u(d(a), Ze), u(a, Ue), _e = 0, r = 0; o > a; r++) i = a.getMinutes(), e += "<tr class='fc-slot" + r + " " + (i ? "fc-minor" : "") + "'>" + "<th class='fc-agenda-axis " + s + "'>" + (l && i ? "&nbsp;" : un(a, tn("axisFormat"))) + "</th>" + "<td class='" + c + "'>" + "<div style='position:relative'>&nbsp;</div>" + "</td>" + "</tr>", u(a, tn("slotMinutes")), _e++;
            e += "</tbody></table>", Me = t(e).appendTo(we), Ce = Me.find("div:first"), M(Me.find("td")), Se = Se.add(Me.find("th:first"))
        }

        function l() {
            var t, e, n, r, a = f(new Date);
            if (Qe) {
                var o = un(N(0), Ke);
                Je ? o += Ge : o = Ge + o, ee.find(".fc-week-number").text(o)
            }
            for (t = 0; Le > t; t++) r = N(t), e = ne.eq(t), e.html(un(r, $e)), n = ae.eq(t), +r == +a ? n.addClass(Ye + "-state-highlight fc-today") : n.removeClass(Ye + "-state-highlight fc-today"), $(e.add(n), r)
        }

        function h(t, n) {
            t === e && (t = ke), ke = t, fn = {};
            var r = re.position().top,
                a = be.position().top,
                o = Math.min(t - r, Me.height() + a + 1);
            se.height(o - L(ie)), le.css("top", r), be.height(o - a - 1), Re = Ce.height() + 1, We = tn("slotMinutes") / Ne, Ae = Re / We, n && m()
        }

        function p(e) {
            Te = e, qe.clear(), He = 0, z(Se.width("").each(function (e, n) {
                He = Math.max(He, t(n).outerWidth())
            }), He);
            var n = be[0].clientWidth;
            Fe = be.width() - n, Fe ? (z(Ee, Fe), Ee.show().prev().removeClass("fc-last")) : Ee.hide().prev().addClass("fc-last"), ze = Math.floor((n - He) / Le), z(ne.slice(0, -1), ze)
        }

        function m() {
            function t() {
                be.scrollTop(r)
            }
            var e = v(),
                n = d(e);
            n.setHours(tn("firstHour"));
            var r = _(e, n) + 1;
            t(), setTimeout(t, 0)
        }

        function y() {
            Ie = be.scrollTop()
        }

        function w() {
            be.scrollTop(Ie)
        }

        function D(t) {
            t.click(C).mousedown(cn)
        }

        function M(t) {
            t.click(C).mousedown(V)
        }

        function C(t) {
            if (!tn("selectable")) {
                var e = Math.min(Le - 1, Math.floor((t.pageX - te.offset().left - He) / ze)),
                    n = N(e),
                    r = this.parentNode.className.match(/fc-slot(\d+)/);
                if (r) {
                    var a = parseInt(r[1]) * tn("slotMinutes"),
                        o = Math.floor(a / 60);
                    n.setHours(o), n.setMinutes(a % 60 + Ue), en("dayClick", ae[e], n, !1, t)
                } else en("dayClick", ae[e], n, !0, t)
            }
        }

        function S(t, e, n) {
            n && Oe.build();
            var r, a, o = d(K.visStart);
            Je ? (r = g(e, o) * Ve + Xe + 1, a = g(t, o) * Ve + Xe + 1) : (r = g(t, o), a = g(e, o)), r = Math.max(0, r), a = Math.min(Le, a), a > r && D(E(0, r, 0, a - 1))
        }

        function E(t, e, n, r) {
            var a = Oe.rect(t, e, n, r, le);
            return rn(a, le)
        }

        function x(t, e) {
            for (var n = d(K.visStart), r = c(d(n), 1), a = 0; Le > a; a++) {
                var o = new Date(Math.max(n, t)),
                    i = new Date(Math.min(r, e));
                if (i > o) {
                    var s = a * Ve + Xe,
                        l = Oe.rect(0, s, 0, s, we),
                        u = _(n, o),
                        f = _(n, i);
                    l.top = u, l.height = f - u, M(rn(l, we))
                }
                c(n, 1), c(r, 1)
            }
        }

        function T(t) {
            return qe.left(t)
        }

        function k(t) {
            return qe.right(t)
        }

        function H(t) {
            return {
                row: Math.floor(g(t, K.visStart) / 7),
                col: A(t.getDay())
            }
        }

        function R(t) {
            var e = N(t.col),
                n = t.row;
            return tn("allDaySlot") && n--, n >= 0 && u(e, Ue + n * Ne), e
        }

        function N(t) {
            return c(d(K.visStart), t * Ve + Xe)
        }

        function W(t) {
            return tn("allDaySlot") && !t.row
        }

        function A(t) {
            return (t - Math.max(je, Pe) + Le) % Le * Ve + Xe
        }

        function _(t, n) {
            if (t = d(t, !0), u(d(t), Ue) > n) return 0;
            if (n >= u(d(t), Ze)) return Me.height();
            var r = tn("slotMinutes"),
                a = 60 * n.getHours() + n.getMinutes() - Ue,
                o = Math.floor(a / r),
                i = fn[o];
            return i === e && (i = fn[o] = Me.find("tr:eq(" + o + ") td div")[0].offsetTop), Math.max(0, Math.round(i - 1 + Re * (a % r / r)))
        }

        function O() {
            return {
                left: He,
                right: Te - Fe
            }
        }

        function B() {
            return ye
        }

        function q(t) {
            var e = d(t.start);
            return t.allDay ? e : u(e, tn("defaultEventMinutes"))
        }

        function I(t, e) {
            return e ? d(t) : u(d(t), tn("slotMinutes"))
        }

        function j(t, e, n) {
            n ? tn("allDaySlot") && S(t, c(d(e), 1), !0) : P(t, e)
        }

        function P(e, n) {
            var r = tn("selectHelper");
            if (Oe.build(), r) {
                var a = g(e, K.visStart) * Ve + Xe;
                if (a >= 0 && Le > a) {
                    var o = Oe.rect(0, a, 0, a, we),
                        i = _(e, e),
                        s = _(e, n);
                    if (s > i) {
                        if (o.top = i, o.height = s - i, o.left += 2, o.width -= 5, t.isFunction(r)) {
                            var c = r(e, n);
                            c && (o.position = "absolute", o.zIndex = 8, xe = t(c).css(o).appendTo(we))
                        } else o.isStart = !0, o.isEnd = !0, xe = t(ln({
                            title: "",
                            start: e,
                            end: n,
                            className: ["fc-select-helper"],
                            editable: !1
                        }, o)), xe.css("opacity", tn("dragOpacity"));
                        xe && (M(xe), we.append(xe), z(xe, o.width, !0), F(xe, o.height, !0))
                    }
                }
            } else x(e, n)
        }

        function J() {
            an(), xe && (xe.remove(), xe = null)
        }

        function V(e) {
            if (1 == e.which && tn("selectable")) {
                sn(e);
                var n;
                Be.start(function (t, e) {
                    if (J(), t && t.col == e.col && !W(t)) {
                        var r = R(e),
                            a = R(t);
                        n = [r, u(d(r), Ne), a, u(d(a), Ne)].sort(Y), P(n[0], n[3])
                    } else n = null
                }, e), t(document).one("mouseup", function (t) {
                    Be.stop(), n && (+n[0] == +n[1] && X(n[0], !1, t), on(n[0], n[3], !1, t))
                })
            }
        }

        function X(t, e, n) {
            en("dayClick", ae[A(t.getDay())], t, e, n)
        }

        function Q(t, e) {
            Be.start(function (t) {
                if (an(), t)
                    if (W(t)) E(t.row, t.col, t.row, t.col);
                    else {
                        var e = R(t),
                            n = u(d(e), tn("defaultEventMinutes"));
                        x(e, n)
                    }
            }, e)
        }

        function G(t, e, n) {
            var r = Be.stop();
            an(), r && en("drop", t, R(r), W(r), e, n)
        }
        var K = this;
        K.renderAgenda = o, K.setWidth = p, K.setHeight = h, K.beforeHide = y, K.afterShow = w, K.defaultEventEnd = q, K.timePosition = _, K.dayOfWeekCol = A, K.dateCell = H, K.cellDate = R, K.cellIsAllDay = W, K.allDayRow = B, K.allDayBounds = O, K.getHoverListener = function () {
            return Be
        }, K.colContentLeft = T, K.colContentRight = k, K.getDaySegmentContainer = function () {
            return fe
        }, K.getSlotSegmentContainer = function () {
            return De
        }, K.getMinMinute = function () {
            return Ue
        }, K.getMaxMinute = function () {
            return Ze
        }, K.getBodyContent = function () {
            return we
        }, K.getRowCnt = function () {
            return 1
        }, K.getColCnt = function () {
            return Le
        }, K.getColWidth = function () {
            return ze
        }, K.getSnapHeight = function () {
            return Ae
        }, K.getSnapMinutes = function () {
            return Ne
        }, K.defaultSelectionEnd = I, K.renderDayOverlay = S, K.renderSelection = j, K.clearSelection = J, K.reportDayClick = X, K.dragStart = Q, K.dragStop = G, ue.call(K, n, r, a), ve.call(K), de.call(K), ce.call(K);
        var te, ee, ne, re, ae, oe, ie, se, le, fe, pe, ye, be, we, De, Me, Ce, Se, Ee, xe, Te, ke, He, ze, Fe, Re, Ne, We, Ae, Le, _e, Oe, Be, qe, Ie, Ye, je, Pe, Je, Ve, Xe, Ue, Ze, $e, Qe, Ge, Ke, tn = K.opt,
            en = K.trigger,
            nn = K.clearEvents,
            rn = K.renderOverlay,
            an = K.clearOverlays,
            on = K.reportSelection,
            sn = K.unselect,
            cn = K.daySelectionMousedown,
            ln = K.slotSegHtml,
            un = r.formatDate,
            fn = {};
        U(n.addClass("fc-agenda")), Oe = new he(function (e, n) {
            function r(t) {
                return Math.max(c, Math.min(l, t))
            }
            var a, o, i;
            ne.each(function (e, r) {
                a = t(r), o = a.offset().left, e && (i[1] = o), i = [o], n[e] = i
            }), i[1] = o + a.outerWidth(), tn("allDaySlot") && (a = ye, o = a.offset().top, e[0] = [o, o + a.outerHeight()]);
            for (var s = we.offset().top, c = be.offset().top, l = c + be.outerHeight(), u = 0; _e * We > u; u++) e.push([r(s + Ae * u), r(s + Ae * (u + 1))])
        }), Be = new ge(Oe), qe = new me(function (t) {
            return oe.eq(t)
        })
    }

    function ce() {
        function n(t, e) {
            S(t);
            var n, r = t.length,
                i = [],
                c = [];
            for (n = 0; r > n; n++) t[n].allDay ? i.push(t[n]) : c.push(t[n]);
            y("allDaySlot") && (Y(a(i), e), z()), s(o(c), e), b("eventAfterAllRender")
        }

        function r() {
            E(), N().empty(), W().empty()
        }

        function a(e) {
            var n, r, a, o, i = k(T(e, t.map(e, C), m.visStart, m.visEnd)),
                s = i.length,
                c = [];
            for (n = 0; s > n; n++)
                for (r = i[n], a = 0; r.length > a; a++) o = r[a], o.row = 0, o.level = n, c.push(o);
            return c
        }

        function o(e) {
            var n, r, a, o, s, l, f = P(),
                v = O(),
                h = _(),
                g = u(d(m.visStart), v),
                p = t.map(e, i),
                y = [];
            for (n = 0; f > n; n++) {
                for (r = k(T(e, p, g, u(d(g), h - v))), le(r), a = 0; r.length > a; a++)
                    for (o = r[a], s = 0; o.length > s; s++) l = o[s], l.col = n, l.level = a, y.push(l);
                c(g, 1, !0)
            }
            return y
        }

        function i(t) {
            return t.end ? d(t.end) : u(d(t.start), y("defaultEventMinutes"))
        }

        function s(n, r) {
            var a, o, i, s, c, u, f, d, h, g, p, m, w, D, M, C, S, E, x, T, k, z, F = n.length,
                N = "",
                A = {}, _ = {}, O = W(),
                Y = P();
            for ((T = y("isRTL")) ? (k = -1, z = Y - 1) : (k = 1, z = 0), a = 0; F > a; a++) o = n[a], i = o.event, s = B(o.start, o.start), c = B(o.start, o.end), u = o.col, f = o.level, d = o.forward || 0, h = q(u * k + z), g = I(u * k + z) - h, g = Math.min(g - 6, .95 * g), p = f ? g / (f + d + 1) : d ? 2 * (g / (d + 1) - 6) : g, m = h + g / (f + d + 1) * f * k + (T ? g - p : 0), o.top = s, o.left = m, o.outerWidth = p, o.outerHeight = c - s, N += l(i, o);
            for (O[0].innerHTML = N, w = O.children(), a = 0; F > a; a++) o = n[a], i = o.event, D = t(w[a]), M = b("eventRender", i, i, D), M === !1 ? D.remove() : (M && M !== !0 && (D.remove(), D = t(M).css({
                position: "absolute",
                top: o.top,
                left: o.left
            }).appendTo(O)), o.element = D, i._id === r ? v(i, D, o) : D[0]._fci = a, G(i, D));
            for (H(O, n, v), a = 0; F > a; a++) o = n[a], (D = o.element) && (S = A[C = o.key = X(D[0])], o.vsides = S === e ? A[C] = L(D, !0) : S, S = _[C], o.hsides = S === e ? _[C] = R(D, !0) : S, E = D.find(".fc-event-title"), E.length && (o.contentTop = E[0].offsetTop));
            for (a = 0; F > a; a++) o = n[a], (D = o.element) && (D[0].style.width = Math.max(0, o.outerWidth - o.hsides) + "px", x = Math.max(0, o.outerHeight - o.vsides), D[0].style.height = x + "px", i = o.event, o.contentTop !== e && 10 > x - o.contentTop && (D.find("div.fc-event-time").text(ie(i.start, y("timeFormat")) + " - " + i.title), D.find("div.fc-event-title").remove()), b("eventAfterRender", i, i, D))
        }

        function l(t, e) {
            var n = "<",
                r = t.url,
                a = Q(t, y),
                o = ["fc-event", "fc-event-vert"];
            return w(t) && o.push("fc-event-draggable"), e.isStart && o.push("fc-event-start"), e.isEnd && o.push("fc-event-end"), o = o.concat(t.className), t.source && (o = o.concat(t.source.className || [])), n += r ? "a href='" + V(t.url) + "'" : "div", n += " class='" + o.join(" ") + "'" + " style='position:absolute;z-index:8;top:" + e.top + "px;left:" + e.left + "px;" + a + "'" + ">" + "<div class='fc-event-inner'>" + "<div class='fc-event-time'>" + V(se(t.start, t.end, y("timeFormat"))) + "</div>" + "<div class='fc-event-title'>" + V(t.title) + "</div>" + "</div>" + "<div class='fc-event-bg'></div>", e.isEnd && D(t) && (n += "<div class='ui-resizable-handle ui-resizable-s'>=</div>"), n += "</" + (r ? "a" : "div") + ">"
        }

        function f(t, e, n) {
            w(t) && h(t, e, n.isStart), n.isEnd && D(t) && j(t, e, n), x(t, e)
        }

        function v(t, e, n) {
            var r = e.find("div.fc-event-time");
            w(t) && g(t, e, r), n.isEnd && D(t) && p(t, e, r), x(t, e)
        }

        function h(t, e, n) {
            function r() {
                s || (e.width(a).height("").draggable("option", "grid", null), s = !0)
            }
            var a, o, i, s = !0,
                l = y("isRTL") ? -1 : 1,
                u = A(),
                f = J(),
                v = U(),
                h = Z(),
                g = O();
            e.draggable({
                zIndex: 9,
                opacity: y("dragOpacity", "month"),
                revertDuration: y("dragRevertDuration"),
                start: function (g, p) {
                    b("eventDragStart", e, t, g, p), te(t, e), a = e.width(), u.start(function (a, u, g, p) {
                        ae(), a ? (o = !1, i = p * l, a.row ? n ? s && (e.width(f - 10), F(e, v * Math.round((t.end ? (t.end - t.start) / Te : y("defaultEventMinutes")) / h)), e.draggable("option", "grid", [f, 1]), s = !1) : o = !0 : (re(c(d(t.start), i), c(C(t), i)), r()), o = o || s && !i) : (r(), o = !0), e.draggable("option", "revert", o)
                    }, g, "drag")
                },
                stop: function (n, a) {
                    if (u.stop(), ae(), b("eventDragStop", e, t, n, a), o) r(), e.css("filter", ""), K(t, e);
                    else {
                        var c = 0;
                        s || (c = Math.round((e.offset().top - $().offset().top) / v) * h + g - (60 * t.start.getHours() + t.start.getMinutes())), ee(this, t, i, c, s, n, a)
                    }
                }
            })
        }

        function g(t, e, n) {
            function r(e) {
                var r, a = u(d(t.start), e);
                t.end && (r = u(d(t.end), e)), n.text(se(a, r, y("timeFormat")))
            }

            function a() {
                f && (n.css("display", ""), e.draggable("option", "grid", [p, m]), f = !1)
            }
            var o, i, s, l, f = !1,
                v = y("isRTL") ? -1 : 1,
                h = A(),
                g = P(),
                p = J(),
                m = U(),
                w = Z();
            e.draggable({
                zIndex: 9,
                scroll: !1,
                grid: [p, m],
                axis: 1 == g ? "y" : !1,
                opacity: y("dragOpacity"),
                revertDuration: y("dragRevertDuration"),
                start: function (r, u) {
                    b("eventDragStart", e, t, r, u), te(t, e), o = e.position(), s = l = 0, h.start(function (r, o, s, l) {
                        e.draggable("option", "revert", !r), ae(), r && (i = l * v, y("allDaySlot") && !r.row ? (f || (f = !0, n.hide(), e.draggable("option", "grid", null)), re(c(d(t.start), i), c(C(t), i))) : a())
                    }, r, "drag")
                },
                drag: function (t, e) {
                    s = Math.round((e.position.top - o.top) / m) * w, s != l && (f || r(s), l = s)
                },
                stop: function (n, c) {
                    var l = h.stop();
                    ae(), b("eventDragStop", e, t, n, c), l && (i || s || f) ? ee(this, t, i, f ? 0 : s, f, n, c) : (a(), e.css("filter", ""), e.css(o), r(0), K(t, e))
                }
            })
        }

        function p(t, e, n) {
            var r, a, o = U(),
                i = Z();
            e.resizable({
                handles: {
                    s: ".ui-resizable-handle"
                },
                grid: o,
                start: function (n, o) {
                    r = a = 0, te(t, e), e.css("z-index", 9), b("eventResizeStart", this, t, n, o)
                },
                resize: function (s, c) {
                    r = Math.round((Math.max(o, e.height()) - c.originalSize.height) / o), r != a && (n.text(se(t.start, r || t.end ? u(M(t), i * r) : null, y("timeFormat"))), a = r)
                },
                stop: function (n, a) {
                    b("eventResizeStop", this, t, n, a), r ? ne(this, t, 0, i * r, n, a) : (e.css("z-index", 8), K(t, e))
                }
            })
        }
        var m = this;
        m.renderEvents = n, m.compileDaySegs = a, m.clearEvents = r, m.slotSegHtml = l, m.bindDaySeg = f, fe.call(m);
        var y = m.opt,
            b = m.trigger,
            w = m.isEventDraggable,
            D = m.isEventResizable,
            M = m.eventEnd,
            S = m.reportEvents,
            E = m.reportEventClear,
            x = m.eventElementHandlers,
            z = m.setHeight,
            N = m.getDaySegmentContainer,
            W = m.getSlotSegmentContainer,
            A = m.getHoverListener,
            _ = m.getMaxMinute,
            O = m.getMinMinute,
            B = m.timePosition,
            q = m.colContentLeft,
            I = m.colContentRight,
            Y = m.renderDaySegs,
            j = m.resizableDayEvent,
            P = m.getColCnt,
            J = m.getColWidth,
            U = m.getSnapHeight,
            Z = m.getSnapMinutes,
            $ = m.getBodyContent,
            G = m.reportEventElement,
            K = m.showEvents,
            te = m.hideEvents,
            ee = m.eventDrop,
            ne = m.eventResize,
            re = m.renderDayOverlay,
            ae = m.clearOverlays,
            oe = m.calendar,
            ie = oe.formatDate,
            se = oe.formatDates
    }

    function le(t) {
        var e, n, r, a, o, i;
        for (e = t.length - 1; e > 0; e--)
            for (a = t[e], n = 0; a.length > n; n++)
                for (o = a[n], r = 0; t[e - 1].length > r; r++) i = t[e - 1][r], x(o, i) && (i.forward = Math.max(i.forward || 0, (o.forward || 0) + 1))
    }

    function ue(t, n, r) {
        function a(t, e) {
            var n = F[t];
            return "object" == typeof n ? J(n, e || r) : n
        }

        function o(t, e) {
            return n.trigger.apply(n, [t, e || S].concat(Array.prototype.slice.call(arguments, 2), [S]))
        }

        function i(t) {
            return l(t) && !a("disableDragging")
        }

        function s(t) {
            return l(t) && !a("disableResizing")
        }

        function l(t) {
            return K(t.editable, (t.source || {}).editable, a("editable"))
        }

        function f(t) {
            k = {};
            var e, n, r = t.length;
            for (e = 0; r > e; e++) n = t[e], k[n._id] ? k[n._id].push(n) : k[n._id] = [n]
        }

        function v(t) {
            return t.end ? d(t.end) : E(t)
        }

        function h(t, e) {
            H.push(e), z[t._id] ? z[t._id].push(e) : z[t._id] = [e]
        }

        function g() {
            H = [], z = {}
        }

        function p(t, n) {
            n.click(function (r) {
                return n.hasClass("ui-draggable-dragging") || n.hasClass("ui-resizable-resizing") ? e : o("eventClick", this, t, r)
            }).hover(function (e) {
                o("eventMouseover", this, t, e)
            }, function (e) {
                o("eventMouseout", this, t, e)
            })
        }

        function m(t, e) {
            b(t, e, "show")
        }

        function y(t, e) {
            b(t, e, "hide")
        }

        function b(t, e, n) {
            var r, a = z[t._id],
                o = a.length;
            for (r = 0; o > r; r++) e && a[r][0] == e[0] || a[r][n]()
        }

        function w(t, e, n, r, a, i, s) {
            var c = e.allDay,
                l = e._id;
            M(k[l], n, r, a), o("eventDrop", t, e, n, r, a, function () {
                M(k[l], -n, -r, c), T(l)
            }, i, s), T(l)
        }

        function D(t, e, n, r, a, i) {
            var s = e._id;
            C(k[s], n, r), o("eventResize", t, e, n, r, function () {
                C(k[s], -n, -r), T(s)
            }, a, i), T(s)
        }

        function M(t, n, r, a) {
            r = r || 0;
            for (var o, i = t.length, s = 0; i > s; s++) o = t[s], a !== e && (o.allDay = a), u(c(o.start, n, !0), r), o.end && (o.end = u(c(o.end, n, !0), r)), x(o, F)
        }

        function C(t, e, n) {
            n = n || 0;
            for (var r, a = t.length, o = 0; a > o; o++) r = t[o], r.end = u(c(v(r), e, !0), n), x(r, F)
        }
        var S = this;
        S.element = t, S.calendar = n, S.name = r, S.opt = a, S.trigger = o, S.isEventDraggable = i, S.isEventResizable = s, S.reportEvents = f, S.eventEnd = v, S.reportEventElement = h, S.reportEventClear = g, S.eventElementHandlers = p, S.showEvents = m, S.hideEvents = y, S.eventDrop = w, S.eventResize = D;
        var E = S.defaultEventEnd,
            x = n.normalizeEvent,
            T = n.reportEventChange,
            k = {}, H = [],
            z = {}, F = n.options
    }

    function fe() {
        function n(t, e) {
            var n, r, c, d, p, m, y, b, w = B(),
                D = T(),
                M = k(),
                C = 0,
                S = t.length;
            for (w[0].innerHTML = a(t), o(t, w.children()), i(t), s(t, w, e), l(t), u(t), f(t), n = v(), r = 0; D > r; r++) {
                for (c = 0, d = [], p = 0; M > p; p++) d[p] = 0;
                for (; S > C && (m = t[C]).row == r;) {
                    for (y = j(d.slice(m.startCol, m.endCol)), m.top = y, y += m.outerHeight, b = m.startCol; m.endCol > b; b++) d[b] = y;
                    C++
                }
                n[r].height(j(d))
            }
            g(t, h(n))
        }

        function r(e, n, r) {
            var i, s, c, d = t("<div/>"),
                p = B(),
                m = e.length;
            for (d[0].innerHTML = a(e), i = d.children(), p.append(i), o(e, i), l(e), u(e), f(e), g(e, h(v())), i = [], s = 0; m > s; s++) c = e[s].element, c && (e[s].row === n && c.css("top", r), i.push(c[0]));
            return t(i)
        }

        function a(t) {
            var e, n, r, a, o, i, s, c, l, u, f = y("isRTL"),
                d = t.length,
                v = F(),
                h = v.left,
                g = v.right,
                p = "";
            for (e = 0; d > e; e++) n = t[e], r = n.event, o = ["fc-event", "fc-event-hori"], w(r) && o.push("fc-event-draggable"), n.isStart && o.push("fc-event-start"), n.isEnd && o.push("fc-event-end"), f ? (i = A(n.end.getDay() - 1), s = A(n.start.getDay()), c = n.isEnd ? N(i) : h, l = n.isStart ? W(s) : g) : (i = A(n.start.getDay()), s = A(n.end.getDay() - 1), c = n.isStart ? N(i) : h, l = n.isEnd ? W(s) : g), o = o.concat(r.className), r.source && (o = o.concat(r.source.className || [])), a = r.url, u = Q(r, y), p += a ? "<a href='" + V(a) + "'" : "<div", p += " class='" + o.join(" ") + "'" + " style='position:absolute;z-index:8;left:" + c + "px;" + u + "'" + ">" + "<div class='fc-event-inner'>", !r.allDay && n.isStart && (p += "<span class='fc-event-time'>" + V(I(r.start, r.end, y("timeFormat"))) + "</span>"), p += "<span class='fc-event-title'>" + V(r.title) + "</span>" + "</div>", n.isEnd && D(r) && (p += "<div class='ui-resizable-handle ui-resizable-" + (f ? "w" : "e") + "'>" + "&nbsp;&nbsp;&nbsp;" + "</div>"), p += "</" + (a ? "a" : "div") + ">", n.left = c, n.outerWidth = l - c, n.startCol = i, n.endCol = s + 1;
            return p
        }

        function o(e, n) {
            var r, a, o, i, s, c = e.length;
            for (r = 0; c > r; r++) a = e[r], o = a.event, i = t(n[r]), s = b("eventRender", o, o, i), s === !1 ? i.remove() : (s && s !== !0 && (s = t(s).css({
                position: "absolute",
                left: a.left
            }), i.replaceWith(s), i = s), a.element = i)
        }

        function i(t) {
            var e, n, r, a = t.length;
            for (e = 0; a > e; e++) n = t[e], r = n.element, r && C(n.event, r)
        }

        function s(t, e, n) {
            var r, a, o, i, s = t.length;
            for (r = 0; s > r; r++) a = t[r], o = a.element, o && (i = a.event, i._id === n ? q(i, o, a) : o[0]._fci = r);
            H(e, t, q)
        }

        function l(t) {
            var n, r, a, o, i, s = t.length,
                c = {};
            for (n = 0; s > n; n++) r = t[n], a = r.element, a && (o = r.key = X(a[0]), i = c[o], i === e && (i = c[o] = R(a, !0)), r.hsides = i)
        }

        function u(t) {
            var e, n, r, a = t.length;
            for (e = 0; a > e; e++) n = t[e], r = n.element, r && (r[0].style.width = Math.max(0, n.outerWidth - n.hsides) + "px")
        }

        function f(t) {
            var n, r, a, o, i, s = t.length,
                c = {};
            for (n = 0; s > n; n++) r = t[n], a = r.element, a && (o = r.key, i = c[o], i === e && (i = c[o] = O(a)), r.outerHeight = a[0].offsetHeight + i)
        }

        function v() {
            var t, e = T(),
                n = [];
            for (t = 0; e > t; t++) n[t] = z(t).find("div.fc-day-content > div");
            return n
        }

        function h(t) {
            var e, n = t.length,
                r = [];
            for (e = 0; n > e; e++) r[e] = t[e][0].offsetTop;
            return r
        }

        function g(t, e) {
            var n, r, a, o, i = t.length;
            for (n = 0; i > n; n++) r = t[n], a = r.element, a && (a[0].style.top = e[r.row] + (r.top || 0) + "px", o = r.event, b("eventAfterRender", o, o, a))
        }

        function p(e, n, a) {
            var o = y("isRTL"),
                i = o ? "w" : "e",
                s = n.find(".ui-resizable-" + i),
                l = !1;
            U(n), n.mousedown(function (t) {
                t.preventDefault()
            }).click(function (t) {
                l && (t.preventDefault(), t.stopImmediatePropagation())
            }), s.mousedown(function (s) {
                function u(n) {
                    b("eventResizeStop", this, e, n), t("body").css("cursor", ""), h.stop(), P(), f && x(this, e, f, 0, n), setTimeout(function () {
                        l = !1
                    }, 0)
                }
                if (1 == s.which) {
                    l = !0;
                    var f, v, h = m.getHoverListener(),
                        g = T(),
                        p = k(),
                        y = o ? -1 : 1,
                        w = o ? p - 1 : 0,
                        D = n.css("top"),
                        C = t.extend({}, e),
                        H = L(e.start);
                    J(), t("body").css("cursor", i + "-resize").one("mouseup", u), b("eventResizeStart", this, e, s), h.start(function (t, n) {
                        if (t) {
                            var s = Math.max(H.row, t.row),
                                l = t.col;
                            1 == g && (s = 0), s == H.row && (l = o ? Math.min(H.col, l) : Math.max(H.col, l)), f = 7 * s + l * y + w - (7 * n.row + n.col * y + w);
                            var u = c(M(e), f, !0);
                            if (f) {
                                C.end = u;
                                var h = v;
                                v = r(_([C]), a.row, D), v.find("*").css("cursor", i + "-resize"), h && h.remove(), E(e)
                            } else v && (S(e), v.remove(), v = null);
                            P(), Y(e.start, c(d(u), 1))
                        }
                    }, s)
                }
            })
        }
        var m = this;
        m.renderDaySegs = n, m.resizableDayEvent = p;
        var y = m.opt,
            b = m.trigger,
            w = m.isEventDraggable,
            D = m.isEventResizable,
            M = m.eventEnd,
            C = m.reportEventElement,
            S = m.showEvents,
            E = m.hideEvents,
            x = m.eventResize,
            T = m.getRowCnt,
            k = m.getColCnt;
        m.getColWidth;
        var z = m.allDayRow,
            F = m.allDayBounds,
            N = m.colContentLeft,
            W = m.colContentRight,
            A = m.dayOfWeekCol,
            L = m.dateCell,
            _ = m.compileDaySegs,
            B = m.getDaySegmentContainer,
            q = m.bindDaySeg,
            I = m.calendar.formatDates,
            Y = m.renderDayOverlay,
            P = m.clearOverlays,
            J = m.clearSelection
    }

    function de() {
        function e(t, e, a) {
            n(), e || (e = c(t, a)), l(t, e, a), r(t, e, a)
        }

        function n(t) {
            f && (f = !1, u(), s("unselect", null, t))
        }

        function r(t, e, n, r) {
            f = !0, s("select", null, t, e, n, r)
        }

        function a(e) {
            var a = o.cellDate,
                s = o.cellIsAllDay,
                c = o.getHoverListener(),
                f = o.reportDayClick;
            if (1 == e.which && i("selectable")) {
                n(e);
                var d;
                c.start(function (t, e) {
                    u(), t && s(t) ? (d = [a(e), a(t)].sort(Y), l(d[0], d[1], !0)) : d = null
                }, e), t(document).one("mouseup", function (t) {
                    c.stop(), d && (+d[0] == +d[1] && f(d[0], !0, t), r(d[0], d[1], !0, t))
                })
            }
        }
        var o = this;
        o.select = e, o.unselect = n, o.reportSelection = r, o.daySelectionMousedown = a;
        var i = o.opt,
            s = o.trigger,
            c = o.defaultSelectionEnd,
            l = o.renderSelection,
            u = o.clearSelection,
            f = !1;
        i("selectable") && i("unselectAuto") && t(document).mousedown(function (e) {
            var r = i("unselectCancel");
            r && t(e.target).parents(r).length || n(e)
        })
    }

    function ve() {
        function e(e, n) {
            var r = o.shift();
            return r || (r = t("<div class='fc-cell-overlay' style='position:absolute;z-index:3'/>")), r[0].parentNode != n[0] && r.appendTo(n), a.push(r.css(e).show()), r
        }

        function n() {
            for (var t; t = a.shift();) o.push(t.hide().unbind())
        }
        var r = this;
        r.renderOverlay = e, r.clearOverlays = n;
        var a = [],
            o = []
    }

    function he(t) {
        var e, n, r = this;
        r.build = function () {
            e = [], n = [], t(e, n)
        }, r.cell = function (t, r) {
            var a, o = e.length,
                i = n.length,
                s = -1,
                c = -1;
            for (a = 0; o > a; a++)
                if (r >= e[a][0] && e[a][1] > r) {
                    s = a;
                    break
                }
            for (a = 0; i > a; a++)
                if (t >= n[a][0] && n[a][1] > t) {
                    c = a;
                    break
                }
            return s >= 0 && c >= 0 ? {
                row: s,
                col: c
            } : null
        }, r.rect = function (t, r, a, o, i) {
            var s = i.offset();
            return {
                top: e[t][0] - s.top,
                left: n[r][0] - s.left,
                width: n[o][1] - n[r][0],
                height: e[a][1] - e[t][0]
            }
        }
    }

    function ge(e) {
        function n(t) {
            pe(t);
            var n = e.cell(t.pageX, t.pageY);
            (!n != !i || n && (n.row != i.row || n.col != i.col)) && (n ? (o || (o = n), a(n, o, n.row - o.row, n.col - o.col)) : a(n, o), i = n)
        }
        var r, a, o, i, s = this;
        s.start = function (s, c, l) {
            a = s, o = i = null, e.build(), n(c), r = l || "mousemove", t(document).bind(r, n)
        }, s.stop = function () {
            return t(document).unbind(r, n), i
        }
    }

    function pe(t) {
        t.pageX === e && (t.pageX = t.originalEvent.pageX, t.pageY = t.originalEvent.pageY)
    }

    function me(t) {
        function n(e) {
            return a[e] = a[e] || t(e)
        }
        var r = this,
            a = {}, o = {}, i = {};
        r.left = function (t) {
            return o[t] = o[t] === e ? n(t).position().left : o[t]
        }, r.right = function (t) {
            return i[t] = i[t] === e ? r.left(t) + n(t).width() : i[t]
        }, r.clear = function () {
            a = {}, o = {}, i = {}
        }
    }
    var ye = {
        defaultView: "month",
        aspectRatio: 1.35,
        header: {
            left: "title",
            center: "",
            right: "today prev,next"
        },
        weekends: !0,
        weekNumbers: !1,
        weekNumberCalculation: "iso",
        weekNumberTitle: "W",
        allDayDefault: !0,
        ignoreTimezone: !0,
        lazyFetching: !0,
        startParam: "start",
        endParam: "end",
        titleFormat: {
            month: "MMMM yyyy",
            week: "MMM d[ yyyy]{ '&#8212;'[ MMM] d yyyy}",
            day: "dddd, MMM d, yyyy"
        },
        columnFormat: {
            month: "ddd",
            week: "ddd M/d",
            day: "dddd M/d"
        },
        timeFormat: {
            "": "h(:mm)t"
        },
        isRTL: !1,
        firstDay: 0,
        monthNames: ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"],
        monthNamesShort: ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"],
        dayNames: ["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"],
        dayNamesShort: ["Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat"],
        buttonText: {
            prev: "<span class='fc-text-arrow'>&lsaquo;</span>",
            next: "<span class='fc-text-arrow'>&rsaquo;</span>",
            prevYear: "<span class='fc-text-arrow'>&laquo;</span>",
            nextYear: "<span class='fc-text-arrow'>&raquo;</span>",
            today: "today",
            month: "month",
            week: "week",
            day: "day"
        },
        theme: !1,
        buttonIcons: {
            prev: "circle-triangle-w",
            next: "circle-triangle-e"
        },
        unselectAuto: !0,
        dropAccept: "*"
    }, be = {
            header: {
                left: "next,prev today",
                center: "",
                right: "title"
            },
            buttonText: {
                prev: "<span class='fc-text-arrow'>&rsaquo;</span>",
                next: "<span class='fc-text-arrow'>&lsaquo;</span>",
                prevYear: "<span class='fc-text-arrow'>&raquo;</span>",
                nextYear: "<span class='fc-text-arrow'>&laquo;</span>"
            },
            buttonIcons: {
                prev: "circle-triangle-e",
                next: "circle-triangle-w"
            }
        }, we = t.fullCalendar = {
            version: "1.6.0"
        }, De = we.views = {};
    t.fn.fullCalendar = function (n) {
        if ("string" == typeof n) {
            var a, o = Array.prototype.slice.call(arguments, 1);
            return this.each(function () {
                var r = t.data(this, "fullCalendar");
                if (r && t.isFunction(r[n])) {
                    var i = r[n].apply(r, o);
                    a === e && (a = i), "destroy" == n && t.removeData(this, "fullCalendar")
                }
            }), a !== e ? a : this
        }
        var i = n.eventSources || [];
        return delete n.eventSources, n.events && (i.push(n.events), delete n.events), n = t.extend(!0, {}, ye, n.isRTL || n.isRTL === e && ye.isRTL ? be : {}, n), this.each(function (e, a) {
            var o = t(a),
                s = new r(o, n, i);
            o.data("fullCalendar", s), s.render()
        }), this
    }, we.sourceNormalizers = [], we.sourceFetchers = [];
    var Me = {
        dataType: "json",
        cache: !1
    }, Ce = 1;
    we.addDays = c, we.cloneDate = d, we.parseDate = m, we.parseISO8601 = y, we.parseTime = b, we.formatDate = w, we.formatDates = D;
    var Se = ["sun", "mon", "tue", "wed", "thu", "fri", "sat"],
        Ee = 864e5,
        xe = 36e5,
        Te = 6e4,
        ke = {
            s: function (t) {
                return t.getSeconds()
            },
            ss: function (t) {
                return P(t.getSeconds())
            },
            m: function (t) {
                return t.getMinutes()
            },
            mm: function (t) {
                return P(t.getMinutes())
            },
            h: function (t) {
                return t.getHours() % 12 || 12
            },
            hh: function (t) {
                return P(t.getHours() % 12 || 12)
            },
            H: function (t) {
                return t.getHours()
            },
            HH: function (t) {
                return P(t.getHours())
            },
            d: function (t) {
                return t.getDate()
            },
            dd: function (t) {
                return P(t.getDate())
            },
            ddd: function (t, e) {
                return e.dayNamesShort[t.getDay()]
            },
            dddd: function (t, e) {
                return e.dayNames[t.getDay()]
            },
            M: function (t) {
                return t.getMonth() + 1
            },
            MM: function (t) {
                return P(t.getMonth() + 1)
            },
            MMM: function (t, e) {
                return e.monthNamesShort[t.getMonth()]
            },
            MMMM: function (t, e) {
                return e.monthNames[t.getMonth()]
            },
            yy: function (t) {
                return (t.getFullYear() + "").substring(2)
            },
            yyyy: function (t) {
                return t.getFullYear()
            },
            t: function (t) {
                return 12 > t.getHours() ? "a" : "p"
            },
            tt: function (t) {
                return 12 > t.getHours() ? "am" : "pm"
            },
            T: function (t) {
                return 12 > t.getHours() ? "A" : "P"
            },
            TT: function (t) {
                return 12 > t.getHours() ? "AM" : "PM"
            },
            u: function (t) {
                return w(t, "yyyy-MM-dd'T'HH:mm:ss'Z'")
            },
            S: function (t) {
                var e = t.getDate();
                return e > 10 && 20 > e ? "th" : ["st", "nd", "rd"][e % 10 - 1] || "th"
            },
            w: function (t, e) {
                return e.weekNumberCalculation(t)
            },
            W: function (t) {
                return M(t)
            }
        };
    we.dateFormatters = ke, we.applyAll = G, De.month = te, De.basicWeek = ee, De.basicDay = ne, n({
        weekMode: "fixed"
    }), De.agendaWeek = oe, De.agendaDay = ie, n({
        allDaySlot: !0,
        allDayText: "all-day",
        firstHour: 6,
        slotMinutes: 30,
        defaultEventMinutes: 120,
        axisFormat: "h(:mm)tt",
        timeFormat: {
            agenda: "h:mm{ - h:mm}"
        },
        dragOpacity: {
            agenda: .5
        },
        minTime: 0,
        maxTime: 24
    })
})(jQuery);

/*! jQuery UI Core- v1.10.2 - 2013-03-14
 * http://jqueryui.com
 * Copyright 2013 jQuery Foundation and other contributors; Licensed MIT */
(function (e, t) {
    function i(t, i) {
        var a, n, r, o = t.nodeName.toLowerCase();
        return "area" === o ? (a = t.parentNode, n = a.name, t.href && n && "map" === a.nodeName.toLowerCase() ? (r = e("img[usemap=#" + n + "]")[0], !! r && s(r)) : !1) : (/input|select|textarea|button|object/.test(o) ? !t.disabled : "a" === o ? t.href || i : i) && s(t)
    }

    function s(t) {
        return e.expr.filters.visible(t) && !e(t).parents().addBack().filter(function () {
            return "hidden" === e.css(this, "visibility")
        }).length
    }
    var a = 0,
        n = /^ui-id-\d+$/;
    e.ui = e.ui || {}, e.extend(e.ui, {
        version: "1.10.2",
        keyCode: {
            BACKSPACE: 8,
            COMMA: 188,
            DELETE: 46,
            DOWN: 40,
            END: 35,
            ENTER: 13,
            ESCAPE: 27,
            HOME: 36,
            LEFT: 37,
            NUMPAD_ADD: 107,
            NUMPAD_DECIMAL: 110,
            NUMPAD_DIVIDE: 111,
            NUMPAD_ENTER: 108,
            NUMPAD_MULTIPLY: 106,
            NUMPAD_SUBTRACT: 109,
            PAGE_DOWN: 34,
            PAGE_UP: 33,
            PERIOD: 190,
            RIGHT: 39,
            SPACE: 32,
            TAB: 9,
            UP: 38
        }
    }), e.fn.extend({
        focus: function (t) {
            return function (i, s) {
                return "number" == typeof i ? this.each(function () {
                    var t = this;
                    setTimeout(function () {
                        e(t).focus(), s && s.call(t)
                    }, i)
                }) : t.apply(this, arguments)
            }
        }(e.fn.focus),
        scrollParent: function () {
            var t;
            return t = e.ui.ie && /(static|relative)/.test(this.css("position")) || /absolute/.test(this.css("position")) ? this.parents().filter(function () {
                return /(relative|absolute|fixed)/.test(e.css(this, "position")) && /(auto|scroll)/.test(e.css(this, "overflow") + e.css(this, "overflow-y") + e.css(this, "overflow-x"))
            }).eq(0) : this.parents().filter(function () {
                return /(auto|scroll)/.test(e.css(this, "overflow") + e.css(this, "overflow-y") + e.css(this, "overflow-x"))
            }).eq(0), /fixed/.test(this.css("position")) || !t.length ? e(document) : t
        },
        zIndex: function (i) {
            if (i !== t) return this.css("zIndex", i);
            if (this.length)
                for (var s, a, n = e(this[0]); n.length && n[0] !== document;) {
                    if (s = n.css("position"), ("absolute" === s || "relative" === s || "fixed" === s) && (a = parseInt(n.css("zIndex"), 10), !isNaN(a) && 0 !== a)) return a;
                    n = n.parent()
                }
            return 0
        },
        uniqueId: function () {
            return this.each(function () {
                this.id || (this.id = "ui-id-" + ++a)
            })
        },
        removeUniqueId: function () {
            return this.each(function () {
                n.test(this.id) && e(this).removeAttr("id")
            })
        }
    }), e.extend(e.expr[":"], {
        data: e.expr.createPseudo ? e.expr.createPseudo(function (t) {
            return function (i) {
                return !!e.data(i, t)
            }
        }) : function (t, i, s) {
            return !!e.data(t, s[3])
        },
        focusable: function (t) {
            return i(t, !isNaN(e.attr(t, "tabindex")))
        },
        tabbable: function (t) {
            var s = e.attr(t, "tabindex"),
                a = isNaN(s);
            return (a || s >= 0) && i(t, !a)
        }
    }), e("<a>").outerWidth(1).jquery || e.each(["Width", "Height"], function (i, s) {
        function a(t, i, s, a) {
            return e.each(n, function () {
                i -= parseFloat(e.css(t, "padding" + this)) || 0, s && (i -= parseFloat(e.css(t, "border" + this + "Width")) || 0), a && (i -= parseFloat(e.css(t, "margin" + this)) || 0)
            }), i
        }
        var n = "Width" === s ? ["Left", "Right"] : ["Top", "Bottom"],
            r = s.toLowerCase(),
            o = {
                innerWidth: e.fn.innerWidth,
                innerHeight: e.fn.innerHeight,
                outerWidth: e.fn.outerWidth,
                outerHeight: e.fn.outerHeight
            };
        e.fn["inner" + s] = function (i) {
            return i === t ? o["inner" + s].call(this) : this.each(function () {
                e(this).css(r, a(this, i) + "px")
            })
        }, e.fn["outer" + s] = function (t, i) {
            return "number" != typeof t ? o["outer" + s].call(this, t) : this.each(function () {
                e(this).css(r, a(this, t, !0, i) + "px")
            })
        }
    }), e.fn.addBack || (e.fn.addBack = function (e) {
        return this.add(null == e ? this.prevObject : this.prevObject.filter(e))
    }), e("<a>").data("a-b", "a").removeData("a-b").data("a-b") && (e.fn.removeData = function (t) {
        return function (i) {
            return arguments.length ? t.call(this, e.camelCase(i)) : t.call(this)
        }
    }(e.fn.removeData)), e.ui.ie = !! /msie [\w.]+/.exec(navigator.userAgent.toLowerCase()), e.support.selectstart = "onselectstart" in document.createElement("div"), e.fn.extend({
        disableSelection: function () {
            return this.bind((e.support.selectstart ? "selectstart" : "mousedown") + ".ui-disableSelection", function (e) {
                e.preventDefault()
            })
        },
        enableSelection: function () {
            return this.unbind(".ui-disableSelection")
        }
    }), e.extend(e.ui, {
        plugin: {
            add: function (t, i, s) {
                var a, n = e.ui[t].prototype;
                for (a in s) n.plugins[a] = n.plugins[a] || [], n.plugins[a].push([i, s[a]])
            },
            call: function (e, t, i) {
                var s, a = e.plugins[t];
                if (a && e.element[0].parentNode && 11 !== e.element[0].parentNode.nodeType)
                    for (s = 0; a.length > s; s++) e.options[a[s][0]] && a[s][1].apply(e.element, i)
            }
        },
        hasScroll: function (t, i) {
            if ("hidden" === e(t).css("overflow")) return !1;
            var s = i && "left" === i ? "scrollLeft" : "scrollTop",
                a = !1;
            return t[s] > 0 ? !0 : (t[s] = 1, a = t[s] > 0, t[s] = 0, a)
        }
    })
})(jQuery);


/*! jQuery UI  Datepicker- v1.10.2 - 2013-03-14
 * http://jqueryui.comzt
 * Copyright 2013 jQuery Foundation and other contributors; Licensed MIT */
(function (t, e) {
    function i() {
        this._curInst = null, this._keyEvent = !1, this._disabledInputs = [], this._datepickerShowing = !1, this._inDialog = !1, this._mainDivId = "ui-datepicker-div", this._inlineClass = "ui-datepicker-inline", this._appendClass = "ui-datepicker-append", this._triggerClass = "ui-datepicker-trigger", this._dialogClass = "ui-datepicker-dialog", this._disableClass = "ui-datepicker-disabled", this._unselectableClass = "ui-datepicker-unselectable", this._currentClass = "ui-datepicker-current-day", this._dayOverClass = "ui-datepicker-days-cell-over", this.regional = [], this.regional[""] = {
            closeText: "Done",
            prevText: "Prev",
            nextText: "Next",
            currentText: "Today",
            monthNames: ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"],
            monthNamesShort: ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"],
            dayNames: ["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"],
            dayNamesShort: ["Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat"],
            dayNamesMin: ["Su", "Mo", "Tu", "We", "Th", "Fr", "Sa"],
            weekHeader: "Wk",
            dateFormat: "mm/dd/yy",
            firstDay: 0,
            isRTL: !1,
            showMonthAfterYear: !1,
            yearSuffix: ""
        }, this._defaults = {
            showOn: "focus",
            showAnim: "fadeIn",
            showOptions: {},
            defaultDate: null,
            appendText: "",
            buttonText: "...",
            buttonImage: "",
            buttonImageOnly: !1,
            hideIfNoPrevNext: !1,
            navigationAsDateFormat: !1,
            gotoCurrent: !1,
            changeMonth: !1,
            changeYear: !1,
            yearRange: "c-10:c+10",
            showOtherMonths: !1,
            selectOtherMonths: !1,
            showWeek: !1,
            calculateWeek: this.iso8601Week,
            shortYearCutoff: "+10",
            minDate: null,
            maxDate: null,
            duration: "fast",
            beforeShowDay: null,
            beforeShow: null,
            onSelect: null,
            onChangeMonthYear: null,
            onClose: null,
            numberOfMonths: 1,
            showCurrentAtPos: 0,
            stepMonths: 1,
            stepBigMonths: 12,
            altField: "",
            altFormat: "",
            constrainInput: !0,
            showButtonPanel: !1,
            autoSize: !1,
            disabled: !1
        }, t.extend(this._defaults, this.regional[""]), this.dpDiv = s(t("<div id='" + this._mainDivId + "' class='ui-datepicker ui-widget ui-widget-content ui-helper-clearfix ui-corner-all'></div>"))
    }

    function s(e) {
        var i = "button, .ui-datepicker-prev, .ui-datepicker-next, .ui-datepicker-calendar td a";
        return e.delegate(i, "mouseout", function () {
            t(this).removeClass("ui-state-hover"), -1 !== this.className.indexOf("ui-datepicker-prev") && t(this).removeClass("ui-datepicker-prev-hover"), -1 !== this.className.indexOf("ui-datepicker-next") && t(this).removeClass("ui-datepicker-next-hover")
        }).delegate(i, "mouseover", function () {
            t.datepicker._isDisabledDatepicker(a.inline ? e.parent()[0] : a.input[0]) || (t(this).parents(".ui-datepicker-calendar").find("a").removeClass("ui-state-hover"), t(this).addClass("ui-state-hover"), -1 !== this.className.indexOf("ui-datepicker-prev") && t(this).addClass("ui-datepicker-prev-hover"), -1 !== this.className.indexOf("ui-datepicker-next") && t(this).addClass("ui-datepicker-next-hover"))
        })
    }

    function n(e, i) {
        t.extend(e, i);
        for (var s in i) null == i[s] && (e[s] = i[s]);
        return e
    }
    t.extend(t.ui, {
        datepicker: {
            version: "1.10.2"
        }
    });
    var a, r = "datepicker",
        o = (new Date).getTime();
    t.extend(i.prototype, {
        markerClassName: "hasDatepicker",
        maxRows: 4,
        _widgetDatepicker: function () {
            return this.dpDiv
        },
        setDefaults: function (t) {
            return n(this._defaults, t || {}), this
        },
        _attachDatepicker: function (e, i) {
            var s, n, a;
            s = e.nodeName.toLowerCase(), n = "div" === s || "span" === s, e.id || (this.uuid += 1, e.id = "dp" + this.uuid), a = this._newInst(t(e), n), a.settings = t.extend({}, i || {}), "input" === s ? this._connectDatepicker(e, a) : n && this._inlineDatepicker(e, a)
        },
        _newInst: function (e, i) {
            var n = e[0].id.replace(/([^A-Za-z0-9_\-])/g, "\\\\$1");
            return {
                id: n,
                input: e,
                selectedDay: 0,
                selectedMonth: 0,
                selectedYear: 0,
                drawMonth: 0,
                drawYear: 0,
                inline: i,
                dpDiv: i ? s(t("<div class='" + this._inlineClass + " ui-datepicker ui-widget ui-widget-content ui-helper-clearfix ui-corner-all'></div>")) : this.dpDiv
            }
        },
        _connectDatepicker: function (e, i) {
            var s = t(e);
            i.append = t([]), i.trigger = t([]), s.hasClass(this.markerClassName) || (this._attachments(s, i), s.addClass(this.markerClassName).keydown(this._doKeyDown).keypress(this._doKeyPress).keyup(this._doKeyUp), this._autoSize(i), t.data(e, r, i), i.settings.disabled && this._disableDatepicker(e))
        },
        _attachments: function (e, i) {
            var s, n, a, r = this._get(i, "appendText"),
                o = this._get(i, "isRTL");
            i.append && i.append.remove(), r && (i.append = t("<span class='" + this._appendClass + "'>" + r + "</span>"), e[o ? "before" : "after"](i.append)), e.unbind("focus", this._showDatepicker), i.trigger && i.trigger.remove(), s = this._get(i, "showOn"), ("focus" === s || "both" === s) && e.focus(this._showDatepicker), ("button" === s || "both" === s) && (n = this._get(i, "buttonText"), a = this._get(i, "buttonImage"), i.trigger = t(this._get(i, "buttonImageOnly") ? t("<img/>").addClass(this._triggerClass).attr({
                src: a,
                alt: n,
                title: n
            }) : t("<button type='button'></button>").addClass(this._triggerClass).html(a ? t("<img/>").attr({
                src: a,
                alt: n,
                title: n
            }) : n)), e[o ? "before" : "after"](i.trigger), i.trigger.click(function () {
                return t.datepicker._datepickerShowing && t.datepicker._lastInput === e[0] ? t.datepicker._hideDatepicker() : t.datepicker._datepickerShowing && t.datepicker._lastInput !== e[0] ? (t.datepicker._hideDatepicker(), t.datepicker._showDatepicker(e[0])) : t.datepicker._showDatepicker(e[0]), !1
            }))
        },
        _autoSize: function (t) {
            if (this._get(t, "autoSize") && !t.inline) {
                var e, i, s, n, a = new Date(2009, 11, 20),
                    r = this._get(t, "dateFormat");
                r.match(/[DM]/) && (e = function (t) {
                    for (i = 0, s = 0, n = 0; t.length > n; n++) t[n].length > i && (i = t[n].length, s = n);
                    return s
                }, a.setMonth(e(this._get(t, r.match(/MM/) ? "monthNames" : "monthNamesShort"))), a.setDate(e(this._get(t, r.match(/DD/) ? "dayNames" : "dayNamesShort")) + 20 - a.getDay())), t.input.attr("size", this._formatDate(t, a).length)
            }
        },
        _inlineDatepicker: function (e, i) {
            var s = t(e);
            s.hasClass(this.markerClassName) || (s.addClass(this.markerClassName).append(i.dpDiv), t.data(e, r, i), this._setDate(i, this._getDefaultDate(i), !0), this._updateDatepicker(i), this._updateAlternate(i), i.settings.disabled && this._disableDatepicker(e), i.dpDiv.css("display", "block"))
        },
        _dialogDatepicker: function (e, i, s, a, o) {
            var h, l, c, u, d, p = this._dialogInst;
            return p || (this.uuid += 1, h = "dp" + this.uuid, this._dialogInput = t("<input type='text' id='" + h + "' style='position: absolute; top: -100px; width: 0px;'/>"), this._dialogInput.keydown(this._doKeyDown), t("body").append(this._dialogInput), p = this._dialogInst = this._newInst(this._dialogInput, !1), p.settings = {}, t.data(this._dialogInput[0], r, p)), n(p.settings, a || {}), i = i && i.constructor === Date ? this._formatDate(p, i) : i, this._dialogInput.val(i), this._pos = o ? o.length ? o : [o.pageX, o.pageY] : null, this._pos || (l = document.documentElement.clientWidth, c = document.documentElement.clientHeight, u = document.documentElement.scrollLeft || document.body.scrollLeft, d = document.documentElement.scrollTop || document.body.scrollTop, this._pos = [l / 2 - 100 + u, c / 2 - 150 + d]), this._dialogInput.css("left", this._pos[0] + 20 + "px").css("top", this._pos[1] + "px"), p.settings.onSelect = s, this._inDialog = !0, this.dpDiv.addClass(this._dialogClass), this._showDatepicker(this._dialogInput[0]), t.blockUI && t.blockUI(this.dpDiv), t.data(this._dialogInput[0], r, p), this
        },
        _destroyDatepicker: function (e) {
            var i, s = t(e),
                n = t.data(e, r);
            s.hasClass(this.markerClassName) && (i = e.nodeName.toLowerCase(), t.removeData(e, r), "input" === i ? (n.append.remove(), n.trigger.remove(), s.removeClass(this.markerClassName).unbind("focus", this._showDatepicker).unbind("keydown", this._doKeyDown).unbind("keypress", this._doKeyPress).unbind("keyup", this._doKeyUp)) : ("div" === i || "span" === i) && s.removeClass(this.markerClassName).empty())
        },
        _enableDatepicker: function (e) {
            var i, s, n = t(e),
                a = t.data(e, r);
            n.hasClass(this.markerClassName) && (i = e.nodeName.toLowerCase(), "input" === i ? (e.disabled = !1, a.trigger.filter("button").each(function () {
                this.disabled = !1
            }).end().filter("img").css({
                opacity: "1.0",
                cursor: ""
            })) : ("div" === i || "span" === i) && (s = n.children("." + this._inlineClass), s.children().removeClass("ui-state-disabled"), s.find("select.ui-datepicker-month, select.ui-datepicker-year").prop("disabled", !1)), this._disabledInputs = t.map(this._disabledInputs, function (t) {
                return t === e ? null : t
            }))
        },
        _disableDatepicker: function (e) {
            var i, s, n = t(e),
                a = t.data(e, r);
            n.hasClass(this.markerClassName) && (i = e.nodeName.toLowerCase(), "input" === i ? (e.disabled = !0, a.trigger.filter("button").each(function () {
                this.disabled = !0
            }).end().filter("img").css({
                opacity: "0.5",
                cursor: "default"
            })) : ("div" === i || "span" === i) && (s = n.children("." + this._inlineClass), s.children().addClass("ui-state-disabled"), s.find("select.ui-datepicker-month, select.ui-datepicker-year").prop("disabled", !0)), this._disabledInputs = t.map(this._disabledInputs, function (t) {
                return t === e ? null : t
            }), this._disabledInputs[this._disabledInputs.length] = e)
        },
        _isDisabledDatepicker: function (t) {
            if (!t) return !1;
            for (var e = 0; this._disabledInputs.length > e; e++)
                if (this._disabledInputs[e] === t) return !0;
            return !1
        },
        _getInst: function (e) {
            try {
                return t.data(e, r)
            } catch (i) {
                throw "Missing instance data for this datepicker"
            }
        },
        _optionDatepicker: function (i, s, a) {
            var r, o, h, l, c = this._getInst(i);
            return 2 === arguments.length && "string" == typeof s ? "defaults" === s ? t.extend({}, t.datepicker._defaults) : c ? "all" === s ? t.extend({}, c.settings) : this._get(c, s) : null : (r = s || {}, "string" == typeof s && (r = {}, r[s] = a), c && (this._curInst === c && this._hideDatepicker(), o = this._getDateDatepicker(i, !0), h = this._getMinMaxDate(c, "min"), l = this._getMinMaxDate(c, "max"), n(c.settings, r), null !== h && r.dateFormat !== e && r.minDate === e && (c.settings.minDate = this._formatDate(c, h)), null !== l && r.dateFormat !== e && r.maxDate === e && (c.settings.maxDate = this._formatDate(c, l)), "disabled" in r && (r.disabled ? this._disableDatepicker(i) : this._enableDatepicker(i)), this._attachments(t(i), c), this._autoSize(c), this._setDate(c, o), this._updateAlternate(c), this._updateDatepicker(c)), e)
        },
        _changeDatepicker: function (t, e, i) {
            this._optionDatepicker(t, e, i)
        },
        _refreshDatepicker: function (t) {
            var e = this._getInst(t);
            e && this._updateDatepicker(e)
        },
        _setDateDatepicker: function (t, e) {
            var i = this._getInst(t);
            i && (this._setDate(i, e), this._updateDatepicker(i), this._updateAlternate(i))
        },
        _getDateDatepicker: function (t, e) {
            var i = this._getInst(t);
            return i && !i.inline && this._setDateFromField(i, e), i ? this._getDate(i) : null
        },
        _doKeyDown: function (e) {
            var i, s, n, a = t.datepicker._getInst(e.target),
                r = !0,
                o = a.dpDiv.is(".ui-datepicker-rtl");
            if (a._keyEvent = !0, t.datepicker._datepickerShowing) switch (e.keyCode) {
            case 9:
                t.datepicker._hideDatepicker(), r = !1;
                break;
            case 13:
                return n = t("td." + t.datepicker._dayOverClass + ":not(." + t.datepicker._currentClass + ")", a.dpDiv), n[0] && t.datepicker._selectDay(e.target, a.selectedMonth, a.selectedYear, n[0]), i = t.datepicker._get(a, "onSelect"), i ? (s = t.datepicker._formatDate(a), i.apply(a.input ? a.input[0] : null, [s, a])) : t.datepicker._hideDatepicker(), !1;
            case 27:
                t.datepicker._hideDatepicker();
                break;
            case 33:
                t.datepicker._adjustDate(e.target, e.ctrlKey ? -t.datepicker._get(a, "stepBigMonths") : -t.datepicker._get(a, "stepMonths"), "M");
                break;
            case 34:
                t.datepicker._adjustDate(e.target, e.ctrlKey ? +t.datepicker._get(a, "stepBigMonths") : +t.datepicker._get(a, "stepMonths"), "M");
                break;
            case 35:
                (e.ctrlKey || e.metaKey) && t.datepicker._clearDate(e.target), r = e.ctrlKey || e.metaKey;
                break;
            case 36:
                (e.ctrlKey || e.metaKey) && t.datepicker._gotoToday(e.target), r = e.ctrlKey || e.metaKey;
                break;
            case 37:
                (e.ctrlKey || e.metaKey) && t.datepicker._adjustDate(e.target, o ? 1 : -1, "D"), r = e.ctrlKey || e.metaKey, e.originalEvent.altKey && t.datepicker._adjustDate(e.target, e.ctrlKey ? -t.datepicker._get(a, "stepBigMonths") : -t.datepicker._get(a, "stepMonths"), "M");
                break;
            case 38:
                (e.ctrlKey || e.metaKey) && t.datepicker._adjustDate(e.target, -7, "D"), r = e.ctrlKey || e.metaKey;
                break;
            case 39:
                (e.ctrlKey || e.metaKey) && t.datepicker._adjustDate(e.target, o ? -1 : 1, "D"), r = e.ctrlKey || e.metaKey, e.originalEvent.altKey && t.datepicker._adjustDate(e.target, e.ctrlKey ? +t.datepicker._get(a, "stepBigMonths") : +t.datepicker._get(a, "stepMonths"), "M");
                break;
            case 40:
                (e.ctrlKey || e.metaKey) && t.datepicker._adjustDate(e.target, 7, "D"), r = e.ctrlKey || e.metaKey;
                break;
            default:
                r = !1
            } else 36 === e.keyCode && e.ctrlKey ? t.datepicker._showDatepicker(this) : r = !1;
            r && (e.preventDefault(), e.stopPropagation())
        },
        _doKeyPress: function (i) {
            var s, n, a = t.datepicker._getInst(i.target);
            return t.datepicker._get(a, "constrainInput") ? (s = t.datepicker._possibleChars(t.datepicker._get(a, "dateFormat")), n = String.fromCharCode(null == i.charCode ? i.keyCode : i.charCode), i.ctrlKey || i.metaKey || " " > n || !s || s.indexOf(n) > -1) : e
        },
        _doKeyUp: function (e) {
            var i, s = t.datepicker._getInst(e.target);
            if (s.input.val() !== s.lastVal) try {
                i = t.datepicker.parseDate(t.datepicker._get(s, "dateFormat"), s.input ? s.input.val() : null, t.datepicker._getFormatConfig(s)), i && (t.datepicker._setDateFromField(s), t.datepicker._updateAlternate(s), t.datepicker._updateDatepicker(s))
            } catch (n) {}
            return !0
        },
        _showDatepicker: function (e) {
            if (e = e.target || e, "input" !== e.nodeName.toLowerCase() && (e = t("input", e.parentNode)[0]), !t.datepicker._isDisabledDatepicker(e) && t.datepicker._lastInput !== e) {
                var i, s, a, r, o, h, l;
                i = t.datepicker._getInst(e), t.datepicker._curInst && t.datepicker._curInst !== i && (t.datepicker._curInst.dpDiv.stop(!0, !0), i && t.datepicker._datepickerShowing && t.datepicker._hideDatepicker(t.datepicker._curInst.input[0])), s = t.datepicker._get(i, "beforeShow"), a = s ? s.apply(e, [e, i]) : {}, a !== !1 && (n(i.settings, a), i.lastVal = null, t.datepicker._lastInput = e, t.datepicker._setDateFromField(i), t.datepicker._inDialog && (e.value = ""), t.datepicker._pos || (t.datepicker._pos = t.datepicker._findPos(e), t.datepicker._pos[1] += e.offsetHeight), r = !1, t(e).parents().each(function () {
                    return r |= "fixed" === t(this).css("position"), !r
                }), o = {
                    left: t.datepicker._pos[0],
                    top: t.datepicker._pos[1]
                }, t.datepicker._pos = null, i.dpDiv.empty(), i.dpDiv.css({
                    position: "absolute",
                    display: "block",
                    top: "-1000px"
                }), t.datepicker._updateDatepicker(i), o = t.datepicker._checkOffset(i, o, r), i.dpDiv.css({
                    position: t.datepicker._inDialog && t.blockUI ? "static" : r ? "fixed" : "absolute",
                    display: "none",
                    left: o.left + "px",
                    top: o.top + "px"
                }), i.inline || (h = t.datepicker._get(i, "showAnim"), l = t.datepicker._get(i, "duration"), i.dpDiv.zIndex(t(e).zIndex() + 1), t.datepicker._datepickerShowing = !0, t.effects && t.effects.effect[h] ? i.dpDiv.show(h, t.datepicker._get(i, "showOptions"), l) : i.dpDiv[h || "show"](h ? l : null), i.input.is(":visible") && !i.input.is(":disabled") && i.input.focus(), t.datepicker._curInst = i))
            }
        },
        _updateDatepicker: function (e) {
            this.maxRows = 4, a = e, e.dpDiv.empty().append(this._generateHTML(e)), this._attachHandlers(e), e.dpDiv.find("." + this._dayOverClass + " a").mouseover();
            var i, s = this._getNumberOfMonths(e),
                n = s[1],
                r = 17;
            e.dpDiv.removeClass("ui-datepicker-multi-2 ui-datepicker-multi-3 ui-datepicker-multi-4").width(""), n > 1 && e.dpDiv.addClass("ui-datepicker-multi-" + n).css("width", r * n + "em"), e.dpDiv[(1 !== s[0] || 1 !== s[1] ? "add" : "remove") + "Class"]("ui-datepicker-multi"), e.dpDiv[(this._get(e, "isRTL") ? "add" : "remove") + "Class"]("ui-datepicker-rtl"), e === t.datepicker._curInst && t.datepicker._datepickerShowing && e.input && e.input.is(":visible") && !e.input.is(":disabled") && e.input[0] !== document.activeElement && e.input.focus(), e.yearshtml && (i = e.yearshtml, setTimeout(function () {
                i === e.yearshtml && e.yearshtml && e.dpDiv.find("select.ui-datepicker-year:first").replaceWith(e.yearshtml), i = e.yearshtml = null
            }, 0))
        },
        _getBorders: function (t) {
            var e = function (t) {
                return {
                    thin: 1,
                    medium: 2,
                    thick: 3
                }[t] || t
            };
            return [parseFloat(e(t.css("border-left-width"))), parseFloat(e(t.css("border-top-width")))]
        },
        _checkOffset: function (e, i, s) {
            var n = e.dpDiv.outerWidth(),
                a = e.dpDiv.outerHeight(),
                r = e.input ? e.input.outerWidth() : 0,
                o = e.input ? e.input.outerHeight() : 0,
                h = document.documentElement.clientWidth + (s ? 0 : t(document).scrollLeft()),
                l = document.documentElement.clientHeight + (s ? 0 : t(document).scrollTop());
            return i.left -= this._get(e, "isRTL") ? n - r : 0, i.left -= s && i.left === e.input.offset().left ? t(document).scrollLeft() : 0, i.top -= s && i.top === e.input.offset().top + o ? t(document).scrollTop() : 0, i.left -= Math.min(i.left, i.left + n > h && h > n ? Math.abs(i.left + n - h) : 0), i.top -= Math.min(i.top, i.top + a > l && l > a ? Math.abs(a + o) : 0), i
        },
        _findPos: function (e) {
            for (var i, s = this._getInst(e), n = this._get(s, "isRTL"); e && ("hidden" === e.type || 1 !== e.nodeType || t.expr.filters.hidden(e));) e = e[n ? "previousSibling" : "nextSibling"];
            return i = t(e).offset(), [i.left, i.top]
        },
        _hideDatepicker: function (e) {
            var i, s, n, a, o = this._curInst;
            !o || e && o !== t.data(e, r) || this._datepickerShowing && (i = this._get(o, "showAnim"), s = this._get(o, "duration"), n = function () {
                t.datepicker._tidyDialog(o)
            }, t.effects && (t.effects.effect[i] || t.effects[i]) ? o.dpDiv.hide(i, t.datepicker._get(o, "showOptions"), s, n) : o.dpDiv["slideDown" === i ? "slideUp" : "fadeIn" === i ? "fadeOut" : "hide"](i ? s : null, n), i || n(), this._datepickerShowing = !1, a = this._get(o, "onClose"), a && a.apply(o.input ? o.input[0] : null, [o.input ? o.input.val() : "", o]), this._lastInput = null, this._inDialog && (this._dialogInput.css({
                position: "absolute",
                left: "0",
                top: "-100px"
            }), t.blockUI && (t.unblockUI(), t("body").append(this.dpDiv))), this._inDialog = !1)
        },
        _tidyDialog: function (t) {
            t.dpDiv.removeClass(this._dialogClass).unbind(".ui-datepicker-calendar")
        },
        _checkExternalClick: function (e) {
            if (t.datepicker._curInst) {
                var i = t(e.target),
                    s = t.datepicker._getInst(i[0]);
                (i[0].id !== t.datepicker._mainDivId && 0 === i.parents("#" + t.datepicker._mainDivId).length && !i.hasClass(t.datepicker.markerClassName) && !i.closest("." + t.datepicker._triggerClass).length && t.datepicker._datepickerShowing && (!t.datepicker._inDialog || !t.blockUI) || i.hasClass(t.datepicker.markerClassName) && t.datepicker._curInst !== s) && t.datepicker._hideDatepicker()
            }
        },
        _adjustDate: function (e, i, s) {
            var n = t(e),
                a = this._getInst(n[0]);
            this._isDisabledDatepicker(n[0]) || (this._adjustInstDate(a, i + ("M" === s ? this._get(a, "showCurrentAtPos") : 0), s), this._updateDatepicker(a))
        },
        _gotoToday: function (e) {
            var i, s = t(e),
                n = this._getInst(s[0]);
            this._get(n, "gotoCurrent") && n.currentDay ? (n.selectedDay = n.currentDay, n.drawMonth = n.selectedMonth = n.currentMonth, n.drawYear = n.selectedYear = n.currentYear) : (i = new Date, n.selectedDay = i.getDate(), n.drawMonth = n.selectedMonth = i.getMonth(), n.drawYear = n.selectedYear = i.getFullYear()), this._notifyChange(n), this._adjustDate(s)
        },
        _selectMonthYear: function (e, i, s) {
            var n = t(e),
                a = this._getInst(n[0]);
            a["selected" + ("M" === s ? "Month" : "Year")] = a["draw" + ("M" === s ? "Month" : "Year")] = parseInt(i.options[i.selectedIndex].value, 10), this._notifyChange(a), this._adjustDate(n)
        },
        _selectDay: function (e, i, s, n) {
            var a, r = t(e);
            t(n).hasClass(this._unselectableClass) || this._isDisabledDatepicker(r[0]) || (a = this._getInst(r[0]), a.selectedDay = a.currentDay = t("a", n).html(), a.selectedMonth = a.currentMonth = i, a.selectedYear = a.currentYear = s, this._selectDate(e, this._formatDate(a, a.currentDay, a.currentMonth, a.currentYear)))
        },
        _clearDate: function (e) {
            var i = t(e);
            this._selectDate(i, "")
        },
        _selectDate: function (e, i) {
            var s, n = t(e),
                a = this._getInst(n[0]);
            i = null != i ? i : this._formatDate(a), a.input && a.input.val(i), this._updateAlternate(a), s = this._get(a, "onSelect"), s ? s.apply(a.input ? a.input[0] : null, [i, a]) : a.input && a.input.trigger("change"), a.inline ? this._updateDatepicker(a) : (this._hideDatepicker(), this._lastInput = a.input[0], "object" != typeof a.input[0] && a.input.focus(), this._lastInput = null)
        },
        _updateAlternate: function (e) {
            var i, s, n, a = this._get(e, "altField");
            a && (i = this._get(e, "altFormat") || this._get(e, "dateFormat"), s = this._getDate(e), n = this.formatDate(i, s, this._getFormatConfig(e)), t(a).each(function () {
                t(this).val(n)
            }))
        },
        noWeekends: function (t) {
            var e = t.getDay();
            return [e > 0 && 6 > e, ""]
        },
        iso8601Week: function (t) {
            var e, i = new Date(t.getTime());
            return i.setDate(i.getDate() + 4 - (i.getDay() || 7)), e = i.getTime(), i.setMonth(0), i.setDate(1), Math.floor(Math.round((e - i) / 864e5) / 7) + 1
        },
        parseDate: function (i, s, n) {
            if (null == i || null == s) throw "Invalid arguments";
            if (s = "object" == typeof s ? "" + s : s + "", "" === s) return null;
            var a, r, o, h, l = 0,
                c = (n ? n.shortYearCutoff : null) || this._defaults.shortYearCutoff,
                u = "string" != typeof c ? c : (new Date).getFullYear() % 100 + parseInt(c, 10),
                d = (n ? n.dayNamesShort : null) || this._defaults.dayNamesShort,
                p = (n ? n.dayNames : null) || this._defaults.dayNames,
                f = (n ? n.monthNamesShort : null) || this._defaults.monthNamesShort,
                m = (n ? n.monthNames : null) || this._defaults.monthNames,
                g = -1,
                v = -1,
                _ = -1,
                b = -1,
                y = !1,
                w = function (t) {
                    var e = i.length > a + 1 && i.charAt(a + 1) === t;
                    return e && a++, e
                }, k = function (t) {
                    var e = w(t),
                        i = "@" === t ? 14 : "!" === t ? 20 : "y" === t && e ? 4 : "o" === t ? 3 : 2,
                        n = RegExp("^\\d{1," + i + "}"),
                        a = s.substring(l).match(n);
                    if (!a) throw "Missing number at position " + l;
                    return l += a[0].length, parseInt(a[0], 10)
                }, x = function (i, n, a) {
                    var r = -1,
                        o = t.map(w(i) ? a : n, function (t, e) {
                            return [[e, t]]
                        }).sort(function (t, e) {
                            return -(t[1].length - e[1].length)
                        });
                    if (t.each(o, function (t, i) {
                        var n = i[1];
                        return s.substr(l, n.length).toLowerCase() === n.toLowerCase() ? (r = i[0], l += n.length, !1) : e
                    }), -1 !== r) return r + 1;
                    throw "Unknown name at position " + l
                }, D = function () {
                    if (s.charAt(l) !== i.charAt(a)) throw "Unexpected literal at position " + l;
                    l++
                };
            for (a = 0; i.length > a; a++)
                if (y) "'" !== i.charAt(a) || w("'") ? D() : y = !1;
                else switch (i.charAt(a)) {
                case "d":
                    _ = k("d");
                    break;
                case "D":
                    x("D", d, p);
                    break;
                case "o":
                    b = k("o");
                    break;
                case "m":
                    v = k("m");
                    break;
                case "M":
                    v = x("M", f, m);
                    break;
                case "y":
                    g = k("y");
                    break;
                case "@":
                    h = new Date(k("@")), g = h.getFullYear(), v = h.getMonth() + 1, _ = h.getDate();
                    break;
                case "!":
                    h = new Date((k("!") - this._ticksTo1970) / 1e4), g = h.getFullYear(), v = h.getMonth() + 1, _ = h.getDate();
                    break;
                case "'":
                    w("'") ? D() : y = !0;
                    break;
                default:
                    D()
                }
                if (s.length > l && (o = s.substr(l), !/^\s+/.test(o))) throw "Extra/unparsed characters found in date: " + o;
            if (-1 === g ? g = (new Date).getFullYear() : 100 > g && (g += (new Date).getFullYear() - (new Date).getFullYear() % 100 + (u >= g ? 0 : -100)), b > -1)
                for (v = 1, _ = b;;) {
                    if (r = this._getDaysInMonth(g, v - 1), r >= _) break;
                    v++, _ -= r
                }
            if (h = this._daylightSavingAdjust(new Date(g, v - 1, _)), h.getFullYear() !== g || h.getMonth() + 1 !== v || h.getDate() !== _) throw "Invalid date";
            return h
        },
        ATOM: "yy-mm-dd",
        COOKIE: "D, dd M yy",
        ISO_8601: "yy-mm-dd",
        RFC_822: "D, d M y",
        RFC_850: "DD, dd-M-y",
        RFC_1036: "D, d M y",
        RFC_1123: "D, d M yy",
        RFC_2822: "D, d M yy",
        RSS: "D, d M y",
        TICKS: "!",
        TIMESTAMP: "@",
        W3C: "yy-mm-dd",
        _ticksTo1970: 1e7 * 60 * 60 * 24 * (718685 + Math.floor(492.5) - Math.floor(19.7) + Math.floor(4.925)),
        formatDate: function (t, e, i) {
            if (!e) return "";
            var s, n = (i ? i.dayNamesShort : null) || this._defaults.dayNamesShort,
                a = (i ? i.dayNames : null) || this._defaults.dayNames,
                r = (i ? i.monthNamesShort : null) || this._defaults.monthNamesShort,
                o = (i ? i.monthNames : null) || this._defaults.monthNames,
                h = function (e) {
                    var i = t.length > s + 1 && t.charAt(s + 1) === e;
                    return i && s++, i
                }, l = function (t, e, i) {
                    var s = "" + e;
                    if (h(t))
                        for (; i > s.length;) s = "0" + s;
                    return s
                }, c = function (t, e, i, s) {
                    return h(t) ? s[e] : i[e]
                }, u = "",
                d = !1;
            if (e)
                for (s = 0; t.length > s; s++)
                    if (d) "'" !== t.charAt(s) || h("'") ? u += t.charAt(s) : d = !1;
                    else switch (t.charAt(s)) {
                    case "d":
                        u += l("d", e.getDate(), 2);
                        break;
                    case "D":
                        u += c("D", e.getDay(), n, a);
                        break;
                    case "o":
                        u += l("o", Math.round((new Date(e.getFullYear(), e.getMonth(), e.getDate()).getTime() - new Date(e.getFullYear(), 0, 0).getTime()) / 864e5), 3);
                        break;
                    case "m":
                        u += l("m", e.getMonth() + 1, 2);
                        break;
                    case "M":
                        u += c("M", e.getMonth(), r, o);
                        break;
                    case "y":
                        u += h("y") ? e.getFullYear() : (10 > e.getYear() % 100 ? "0" : "") + e.getYear() % 100;
                        break;
                    case "@":
                        u += e.getTime();
                        break;
                    case "!":
                        u += 1e4 * e.getTime() + this._ticksTo1970;
                        break;
                    case "'":
                        h("'") ? u += "'" : d = !0;
                        break;
                    default:
                        u += t.charAt(s)
                    }
                    return u
        },
        _possibleChars: function (t) {
            var e, i = "",
                s = !1,
                n = function (i) {
                    var s = t.length > e + 1 && t.charAt(e + 1) === i;
                    return s && e++, s
                };
            for (e = 0; t.length > e; e++)
                if (s) "'" !== t.charAt(e) || n("'") ? i += t.charAt(e) : s = !1;
                else switch (t.charAt(e)) {
                case "d":
                case "m":
                case "y":
                case "@":
                    i += "0123456789";
                    break;
                case "D":
                case "M":
                    return null;
                case "'":
                    n("'") ? i += "'" : s = !0;
                    break;
                default:
                    i += t.charAt(e)
                }
                return i
        },
        _get: function (t, i) {
            return t.settings[i] !== e ? t.settings[i] : this._defaults[i]
        },
        _setDateFromField: function (t, e) {
            if (t.input.val() !== t.lastVal) {
                var i = this._get(t, "dateFormat"),
                    s = t.lastVal = t.input ? t.input.val() : null,
                    n = this._getDefaultDate(t),
                    a = n,
                    r = this._getFormatConfig(t);
                try {
                    a = this.parseDate(i, s, r) || n
                } catch (o) {
                    s = e ? "" : s
                }
                t.selectedDay = a.getDate(), t.drawMonth = t.selectedMonth = a.getMonth(), t.drawYear = t.selectedYear = a.getFullYear(), t.currentDay = s ? a.getDate() : 0, t.currentMonth = s ? a.getMonth() : 0, t.currentYear = s ? a.getFullYear() : 0, this._adjustInstDate(t)
            }
        },
        _getDefaultDate: function (t) {
            return this._restrictMinMax(t, this._determineDate(t, this._get(t, "defaultDate"), new Date))
        },
        _determineDate: function (e, i, s) {
            var n = function (t) {
                var e = new Date;
                return e.setDate(e.getDate() + t), e
            }, a = function (i) {
                    try {
                        return t.datepicker.parseDate(t.datepicker._get(e, "dateFormat"), i, t.datepicker._getFormatConfig(e))
                    } catch (s) {}
                    for (var n = (i.toLowerCase().match(/^c/) ? t.datepicker._getDate(e) : null) || new Date, a = n.getFullYear(), r = n.getMonth(), o = n.getDate(), h = /([+\-]?[0-9]+)\s*(d|D|w|W|m|M|y|Y)?/g, l = h.exec(i); l;) {
                        switch (l[2] || "d") {
                        case "d":
                        case "D":
                            o += parseInt(l[1], 10);
                            break;
                        case "w":
                        case "W":
                            o += 7 * parseInt(l[1], 10);
                            break;
                        case "m":
                        case "M":
                            r += parseInt(l[1], 10), o = Math.min(o, t.datepicker._getDaysInMonth(a, r));
                            break;
                        case "y":
                        case "Y":
                            a += parseInt(l[1], 10), o = Math.min(o, t.datepicker._getDaysInMonth(a, r))
                        }
                        l = h.exec(i)
                    }
                    return new Date(a, r, o)
                }, r = null == i || "" === i ? s : "string" == typeof i ? a(i) : "number" == typeof i ? isNaN(i) ? s : n(i) : new Date(i.getTime());
            return r = r && "Invalid Date" == "" + r ? s : r, r && (r.setHours(0), r.setMinutes(0), r.setSeconds(0), r.setMilliseconds(0)), this._daylightSavingAdjust(r)
        },
        _daylightSavingAdjust: function (t) {
            return t ? (t.setHours(t.getHours() > 12 ? t.getHours() + 2 : 0), t) : null
        },
        _setDate: function (t, e, i) {
            var s = !e,
                n = t.selectedMonth,
                a = t.selectedYear,
                r = this._restrictMinMax(t, this._determineDate(t, e, new Date));
            t.selectedDay = t.currentDay = r.getDate(), t.drawMonth = t.selectedMonth = t.currentMonth = r.getMonth(), t.drawYear = t.selectedYear = t.currentYear = r.getFullYear(), n === t.selectedMonth && a === t.selectedYear || i || this._notifyChange(t), this._adjustInstDate(t), t.input && t.input.val(s ? "" : this._formatDate(t))
        },
        _getDate: function (t) {
            var e = !t.currentYear || t.input && "" === t.input.val() ? null : this._daylightSavingAdjust(new Date(t.currentYear, t.currentMonth, t.currentDay));
            return e
        },
        _attachHandlers: function (e) {
            var i = this._get(e, "stepMonths"),
                s = "#" + e.id.replace(/\\\\/g, "\\");
            e.dpDiv.find("[data-handler]").map(function () {
                var e = {
                    prev: function () {
                        window["DP_jQuery_" + o].datepicker._adjustDate(s, -i, "M")
                    },
                    next: function () {
                        window["DP_jQuery_" + o].datepicker._adjustDate(s, +i, "M")
                    },
                    hide: function () {
                        window["DP_jQuery_" + o].datepicker._hideDatepicker()
                    },
                    today: function () {
                        window["DP_jQuery_" + o].datepicker._gotoToday(s)
                    },
                    selectDay: function () {
                        return window["DP_jQuery_" + o].datepicker._selectDay(s, +this.getAttribute("data-month"), +this.getAttribute("data-year"), this), !1
                    },
                    selectMonth: function () {
                        return window["DP_jQuery_" + o].datepicker._selectMonthYear(s, this, "M"), !1
                    },
                    selectYear: function () {
                        return window["DP_jQuery_" + o].datepicker._selectMonthYear(s, this, "Y"), !1
                    }
                };
                t(this).bind(this.getAttribute("data-event"), e[this.getAttribute("data-handler")])
            })
        },
        _generateHTML: function (t) {
            var e, i, s, n, a, r, o, h, l, c, u, d, p, f, m, g, v, _, b, y, w, k, x, D, T, C, S, M, N, I, P, A, z, H, E, F, O, W, j, R = new Date,
                L = this._daylightSavingAdjust(new Date(R.getFullYear(), R.getMonth(), R.getDate())),
                Y = this._get(t, "isRTL"),
                B = this._get(t, "showButtonPanel"),
                J = this._get(t, "hideIfNoPrevNext"),
                Q = this._get(t, "navigationAsDateFormat"),
                K = this._getNumberOfMonths(t),
                V = this._get(t, "showCurrentAtPos"),
                U = this._get(t, "stepMonths"),
                q = 1 !== K[0] || 1 !== K[1],
                X = this._daylightSavingAdjust(t.currentDay ? new Date(t.currentYear, t.currentMonth, t.currentDay) : new Date(9999, 9, 9)),
                G = this._getMinMaxDate(t, "min"),
                $ = this._getMinMaxDate(t, "max"),
                Z = t.drawMonth - V,
                te = t.drawYear;
            if (0 > Z && (Z += 12, te--), $)
                for (e = this._daylightSavingAdjust(new Date($.getFullYear(), $.getMonth() - K[0] * K[1] + 1, $.getDate())), e = G && G > e ? G : e; this._daylightSavingAdjust(new Date(te, Z, 1)) > e;) Z--, 0 > Z && (Z = 11, te--);
            for (t.drawMonth = Z, t.drawYear = te, i = this._get(t, "prevText"), i = Q ? this.formatDate(i, this._daylightSavingAdjust(new Date(te, Z - U, 1)), this._getFormatConfig(t)) : i, s = this._canAdjustMonth(t, -1, te, Z) ? "<a class='ui-datepicker-prev ui-corner-all' data-handler='prev' data-event='click' title='" + i + "'><span class='ui-icon ui-icon-circle-triangle-" + (Y ? "e" : "w") + "'>" + i + "</span></a>" : J ? "" : "<a class='ui-datepicker-prev ui-corner-all ui-state-disabled' title='" + i + "'><span class='ui-icon ui-icon-circle-triangle-" + (Y ? "e" : "w") + "'>" + i + "</span></a>", n = this._get(t, "nextText"), n = Q ? this.formatDate(n, this._daylightSavingAdjust(new Date(te, Z + U, 1)), this._getFormatConfig(t)) : n, a = this._canAdjustMonth(t, 1, te, Z) ? "<a class='ui-datepicker-next ui-corner-all' data-handler='next' data-event='click' title='" + n + "'><span class='ui-icon ui-icon-circle-triangle-" + (Y ? "w" : "e") + "'>" + n + "</span></a>" : J ? "" : "<a class='ui-datepicker-next ui-corner-all ui-state-disabled' title='" + n + "'><span class='ui-icon ui-icon-circle-triangle-" + (Y ? "w" : "e") + "'>" + n + "</span></a>", r = this._get(t, "currentText"), o = this._get(t, "gotoCurrent") && t.currentDay ? X : L, r = Q ? this.formatDate(r, o, this._getFormatConfig(t)) : r, h = t.inline ? "" : "<button type='button' class='ui-datepicker-close ui-state-default ui-priority-primary ui-corner-all' data-handler='hide' data-event='click'>" + this._get(t, "closeText") + "</button>", l = B ? "<div class='ui-datepicker-buttonpane ui-widget-content'>" + (Y ? h : "") + (this._isInRange(t, o) ? "<button type='button' class='ui-datepicker-current ui-state-default ui-priority-secondary ui-corner-all' data-handler='today' data-event='click'>" + r + "</button>" : "") + (Y ? "" : h) + "</div>" : "", c = parseInt(this._get(t, "firstDay"), 10), c = isNaN(c) ? 0 : c, u = this._get(t, "showWeek"), d = this._get(t, "dayNames"), p = this._get(t, "dayNamesMin"), f = this._get(t, "monthNames"), m = this._get(t, "monthNamesShort"), g = this._get(t, "beforeShowDay"), v = this._get(t, "showOtherMonths"), _ = this._get(t, "selectOtherMonths"), b = this._getDefaultDate(t), y = "", k = 0; K[0] > k; k++) {
                for (x = "", this.maxRows = 4, D = 0; K[1] > D; D++) {
                    if (T = this._daylightSavingAdjust(new Date(te, Z, t.selectedDay)), C = " ui-corner-all", S = "", q) {
                        if (S += "<div class='ui-datepicker-group", K[1] > 1) switch (D) {
                        case 0:
                            S += " ui-datepicker-group-first", C = " ui-corner-" + (Y ? "right" : "left");
                            break;
                        case K[1] - 1:
                            S += " ui-datepicker-group-last", C = " ui-corner-" + (Y ? "left" : "right");
                            break;
                        default:
                            S += " ui-datepicker-group-middle", C = ""
                        }
                        S += "'>"
                    }
                    for (S += "<div class='ui-datepicker-header ui-widget-header ui-helper-clearfix" + C + "'>" + (/all|left/.test(C) && 0 === k ? Y ? a : s : "") + (/all|right/.test(C) && 0 === k ? Y ? s : a : "") + this._generateMonthYearHeader(t, Z, te, G, $, k > 0 || D > 0, f, m) + "</div><table class='ui-datepicker-calendar'><thead>" + "<tr>", M = u ? "<th class='ui-datepicker-week-col'>" + this._get(t, "weekHeader") + "</th>" : "", w = 0; 7 > w; w++) N = (w + c) % 7, M += "<th" + ((w + c + 6) % 7 >= 5 ? " class='ui-datepicker-week-end'" : "") + ">" + "<span title='" + d[N] + "'>" + p[N] + "</span></th>";
                    for (S += M + "</tr></thead><tbody>", I = this._getDaysInMonth(te, Z), te === t.selectedYear && Z === t.selectedMonth && (t.selectedDay = Math.min(t.selectedDay, I)), P = (this._getFirstDayOfMonth(te, Z) - c + 7) % 7, A = Math.ceil((P + I) / 7), z = q ? this.maxRows > A ? this.maxRows : A : A, this.maxRows = z, H = this._daylightSavingAdjust(new Date(te, Z, 1 - P)), E = 0; z > E; E++) {
                        for (S += "<tr>", F = u ? "<td class='ui-datepicker-week-col'>" + this._get(t, "calculateWeek")(H) + "</td>" : "", w = 0; 7 > w; w++) O = g ? g.apply(t.input ? t.input[0] : null, [H]) : [!0, ""], W = H.getMonth() !== Z, j = W && !_ || !O[0] || G && G > H || $ && H > $, F += "<td class='" + ((w + c + 6) % 7 >= 5 ? " ui-datepicker-week-end" : "") + (W ? " ui-datepicker-other-month" : "") + (H.getTime() === T.getTime() && Z === t.selectedMonth && t._keyEvent || b.getTime() === H.getTime() && b.getTime() === T.getTime() ? " " + this._dayOverClass : "") + (j ? " " + this._unselectableClass + " ui-state-disabled" : "") + (W && !v ? "" : " " + O[1] + (H.getTime() === X.getTime() ? " " + this._currentClass : "") + (H.getTime() === L.getTime() ? " ui-datepicker-today" : "")) + "'" + (W && !v || !O[2] ? "" : " title='" + O[2].replace(/'/g, "&#39;") + "'") + (j ? "" : " data-handler='selectDay' data-event='click' data-month='" + H.getMonth() + "' data-year='" + H.getFullYear() + "'") + ">" + (W && !v ? "&#xa0;" : j ? "<span class='ui-state-default'>" + H.getDate() + "</span>" : "<a class='ui-state-default" + (H.getTime() === L.getTime() ? " ui-state-highlight" : "") + (H.getTime() === X.getTime() ? " ui-state-active" : "") + (W ? " ui-priority-secondary" : "") + "' href='#'>" + H.getDate() + "</a>") + "</td>", H.setDate(H.getDate() + 1), H = this._daylightSavingAdjust(H);
                        S += F + "</tr>"
                    }
                    Z++, Z > 11 && (Z = 0, te++), S += "</tbody></table>" + (q ? "</div>" + (K[0] > 0 && D === K[1] - 1 ? "<div class='ui-datepicker-row-break'></div>" : "") : ""), x += S
                }
                y += x
            }
            return y += l, t._keyEvent = !1, y
        },
        _generateMonthYearHeader: function (t, e, i, s, n, a, r, o) {
            var h, l, c, u, d, p, f, m, g = this._get(t, "changeMonth"),
                v = this._get(t, "changeYear"),
                _ = this._get(t, "showMonthAfterYear"),
                b = "<div class='ui-datepicker-title'>",
                y = "";
            if (a || !g) y += "<span class='ui-datepicker-month'>" + r[e] + "</span>";
            else {
                for (h = s && s.getFullYear() === i, l = n && n.getFullYear() === i, y += "<select class='ui-datepicker-month' data-handler='selectMonth' data-event='change'>", c = 0; 12 > c; c++)(!h || c >= s.getMonth()) && (!l || n.getMonth() >= c) && (y += "<option value='" + c + "'" + (c === e ? " selected='selected'" : "") + ">" + o[c] + "</option>");
                y += "</select>"
            } if (_ || (b += y + (!a && g && v ? "" : "&#xa0;")), !t.yearshtml)
                if (t.yearshtml = "", a || !v) b += "<span class='ui-datepicker-year'>" + i + "</span>";
                else {
                    for (u = this._get(t, "yearRange").split(":"), d = (new Date).getFullYear(), p = function (t) {
                        var e = t.match(/c[+\-].*/) ? i + parseInt(t.substring(1), 10) : t.match(/[+\-].*/) ? d + parseInt(t, 10) : parseInt(t, 10);
                        return isNaN(e) ? d : e
                    }, f = p(u[0]), m = Math.max(f, p(u[1] || "")), f = s ? Math.max(f, s.getFullYear()) : f, m = n ? Math.min(m, n.getFullYear()) : m, t.yearshtml += "<select class='ui-datepicker-year' data-handler='selectYear' data-event='change'>"; m >= f; f++) t.yearshtml += "<option value='" + f + "'" + (f === i ? " selected='selected'" : "") + ">" + f + "</option>";
                    t.yearshtml += "</select>", b += t.yearshtml, t.yearshtml = null
                }
            return b += this._get(t, "yearSuffix"), _ && (b += (!a && g && v ? "" : "&#xa0;") + y), b += "</div>"
        },
        _adjustInstDate: function (t, e, i) {
            var s = t.drawYear + ("Y" === i ? e : 0),
                n = t.drawMonth + ("M" === i ? e : 0),
                a = Math.min(t.selectedDay, this._getDaysInMonth(s, n)) + ("D" === i ? e : 0),
                r = this._restrictMinMax(t, this._daylightSavingAdjust(new Date(s, n, a)));
            t.selectedDay = r.getDate(), t.drawMonth = t.selectedMonth = r.getMonth(), t.drawYear = t.selectedYear = r.getFullYear(), ("M" === i || "Y" === i) && this._notifyChange(t)
        },
        _restrictMinMax: function (t, e) {
            var i = this._getMinMaxDate(t, "min"),
                s = this._getMinMaxDate(t, "max"),
                n = i && i > e ? i : e;
            return s && n > s ? s : n
        },
        _notifyChange: function (t) {
            var e = this._get(t, "onChangeMonthYear");
            e && e.apply(t.input ? t.input[0] : null, [t.selectedYear, t.selectedMonth + 1, t])
        },
        _getNumberOfMonths: function (t) {
            var e = this._get(t, "numberOfMonths");
            return null == e ? [1, 1] : "number" == typeof e ? [1, e] : e
        },
        _getMinMaxDate: function (t, e) {
            return this._determineDate(t, this._get(t, e + "Date"), null)
        },
        _getDaysInMonth: function (t, e) {
            return 32 - this._daylightSavingAdjust(new Date(t, e, 32)).getDate()
        },
        _getFirstDayOfMonth: function (t, e) {
            return new Date(t, e, 1).getDay()
        },
        _canAdjustMonth: function (t, e, i, s) {
            var n = this._getNumberOfMonths(t),
                a = this._daylightSavingAdjust(new Date(i, s + (0 > e ? e : n[0] * n[1]), 1));
            return 0 > e && a.setDate(this._getDaysInMonth(a.getFullYear(), a.getMonth())), this._isInRange(t, a)
        },
        _isInRange: function (t, e) {
            var i, s, n = this._getMinMaxDate(t, "min"),
                a = this._getMinMaxDate(t, "max"),
                r = null,
                o = null,
                h = this._get(t, "yearRange");
            return h && (i = h.split(":"), s = (new Date).getFullYear(), r = parseInt(i[0], 10), o = parseInt(i[1], 10), i[0].match(/[+\-].*/) && (r += s), i[1].match(/[+\-].*/) && (o += s)), (!n || e.getTime() >= n.getTime()) && (!a || e.getTime() <= a.getTime()) && (!r || e.getFullYear() >= r) && (!o || o >= e.getFullYear())
        },
        _getFormatConfig: function (t) {
            var e = this._get(t, "shortYearCutoff");
            return e = "string" != typeof e ? e : (new Date).getFullYear() % 100 + parseInt(e, 10), {
                shortYearCutoff: e,
                dayNamesShort: this._get(t, "dayNamesShort"),
                dayNames: this._get(t, "dayNames"),
                monthNamesShort: this._get(t, "monthNamesShort"),
                monthNames: this._get(t, "monthNames")
            }
        },
        _formatDate: function (t, e, i, s) {
            e || (t.currentDay = t.selectedDay, t.currentMonth = t.selectedMonth, t.currentYear = t.selectedYear);
            var n = e ? "object" == typeof e ? e : this._daylightSavingAdjust(new Date(s, i, e)) : this._daylightSavingAdjust(new Date(t.currentYear, t.currentMonth, t.currentDay));
            return this.formatDate(this._get(t, "dateFormat"), n, this._getFormatConfig(t))
        }
    }), t.fn.datepicker = function (e) {
        if (!this.length) return this;
        t.datepicker.initialized || (t(document).mousedown(t.datepicker._checkExternalClick), t.datepicker.initialized = !0), 0 === t("#" + t.datepicker._mainDivId).length && t("body").append(t.datepicker.dpDiv);
        var i = Array.prototype.slice.call(arguments, 1);
        return "string" != typeof e || "isDisabled" !== e && "getDate" !== e && "widget" !== e ? "option" === e && 2 === arguments.length && "string" == typeof arguments[1] ? t.datepicker["_" + e + "Datepicker"].apply(t.datepicker, [this[0]].concat(i)) : this.each(function () {
            "string" == typeof e ? t.datepicker["_" + e + "Datepicker"].apply(t.datepicker, [this].concat(i)) : t.datepicker._attachDatepicker(this, e)
        }) : t.datepicker["_" + e + "Datepicker"].apply(t.datepicker, [this[0]].concat(i))
    }, t.datepicker = new i, t.datepicker.initialized = !1, t.datepicker.uuid = (new Date).getTime(), t.datepicker.version = "1.10.2", window["DP_jQuery_" + o] = t
})(jQuery);


/*!
 * Bootstrap.js by @fat & @mdo
 * Copyright 2012 Twitter, Inc.
 * http://www.apache.org/licenses/LICENSE-2.0.txt
 */
! function (e) {
    "use strict";
    e(function () {
        e.support.transition = function () {
            var e = function () {
                var e = document.createElement("bootstrap"),
                    t = {
                        WebkitTransition: "webkitTransitionEnd",
                        MozTransition: "transitionend",
                        OTransition: "oTransitionEnd otransitionend",
                        transition: "transitionend"
                    }, n;
                for (n in t)
                    if (e.style[n] !== undefined) return t[n]
            }();
            return e && {
                end: e
            }
        }()
    })
}(window.jQuery), ! function (e) {
    "use strict";
    var t = '[data-dismiss="alert"]',
        n = function (n) {
            e(n).on("click", t, this.close)
        };
    n.prototype.close = function (t) {
        function s() {
            i.trigger("closed").remove()
        }
        var n = e(this),
            r = n.attr("data-target"),
            i;
        r || (r = n.attr("href"), r = r && r.replace(/.*(?=#[^\s]*$)/, "")), i = e(r), t && t.preventDefault(), i.length || (i = n.hasClass("alert") ? n : n.parent()), i.trigger(t = e.Event("close"));
        if (t.isDefaultPrevented()) return;
        i.removeClass("in"), e.support.transition && i.hasClass("fade") ? i.on(e.support.transition.end, s) : s()
    };
    var r = e.fn.alert;
    e.fn.alert = function (t) {
        return this.each(function () {
            var r = e(this),
                i = r.data("alert");
            i || r.data("alert", i = new n(this)), typeof t == "string" && i[t].call(r)
        })
    }, e.fn.alert.Constructor = n, e.fn.alert.noConflict = function () {
        return e.fn.alert = r, this
    }, e(document).on("click.alert.data-api", t, n.prototype.close)
}(window.jQuery), ! function (e) {
    "use strict";
    var t = function (t, n) {
        this.$element = e(t), this.options = e.extend({}, e.fn.button.defaults, n)
    };
    t.prototype.setState = function (e) {
        var t = "disabled",
            n = this.$element,
            r = n.data(),
            i = n.is("input") ? "val" : "html";
        e += "Text", r.resetText || n.data("resetText", n[i]()), n[i](r[e] || this.options[e]), setTimeout(function () {
            e == "loadingText" ? n.addClass(t).attr(t, t) : n.removeClass(t).removeAttr(t)
        }, 0)
    }, t.prototype.toggle = function () {
        var e = this.$element.closest('[data-toggle="buttons-radio"]');
        e && e.find(".active").removeClass("active"), this.$element.toggleClass("active")
    };
    var n = e.fn.button;
    e.fn.button = function (n) {
        return this.each(function () {
            var r = e(this),
                i = r.data("button"),
                s = typeof n == "object" && n;
            i || r.data("button", i = new t(this, s)), n == "toggle" ? i.toggle() : n && i.setState(n)
        })
    }, e.fn.button.defaults = {
        loadingText: "loading..."
    }, e.fn.button.Constructor = t, e.fn.button.noConflict = function () {
        return e.fn.button = n, this
    }, e(document).on("click.button.data-api", "[data-toggle^=button]", function (t) {
        var n = e(t.target);
        n.hasClass("btn") || (n = n.closest(".btn")), n.button("toggle")
    })
}(window.jQuery), ! function (e) {
    "use strict";
    var t = function (t, n) {
        this.$element = e(t), this.$indicators = this.$element.find(".carousel-indicators"), this.options = n, this.options.pause == "hover" && this.$element.on("mouseenter", e.proxy(this.pause, this)).on("mouseleave", e.proxy(this.cycle, this))
    };
    t.prototype = {
        cycle: function (t) {
            return t || (this.paused = !1), this.interval && clearInterval(this.interval), this.options.interval && !this.paused && (this.interval = setInterval(e.proxy(this.next, this), this.options.interval)), this
        },
        getActiveIndex: function () {
            return this.$active = this.$element.find(".item.active"), this.$items = this.$active.parent().children(), this.$items.index(this.$active)
        },
        to: function (t) {
            var n = this.getActiveIndex(),
                r = this;
            if (t > this.$items.length - 1 || t < 0) return;
            return this.sliding ? this.$element.one("slid", function () {
                r.to(t)
            }) : n == t ? this.pause().cycle() : this.slide(t > n ? "next" : "prev", e(this.$items[t]))
        },
        pause: function (t) {
            return t || (this.paused = !0), this.$element.find(".next, .prev").length && e.support.transition.end && (this.$element.trigger(e.support.transition.end), this.cycle(!0)), clearInterval(this.interval), this.interval = null, this
        },
        next: function () {
            if (this.sliding) return;
            return this.slide("next")
        },
        prev: function () {
            if (this.sliding) return;
            return this.slide("prev")
        },
        slide: function (t, n) {
            var r = this.$element.find(".item.active"),
                i = n || r[t](),
                s = this.interval,
                o = t == "next" ? "left" : "right",
                u = t == "next" ? "first" : "last",
                a = this,
                f;
            this.sliding = !0, s && this.pause(), i = i.length ? i : this.$element.find(".item")[u](), f = e.Event("slide", {
                relatedTarget: i[0],
                direction: o
            });
            if (i.hasClass("active")) return;
            this.$indicators.length && (this.$indicators.find(".active").removeClass("active"), this.$element.one("slid", function () {
                var t = e(a.$indicators.children()[a.getActiveIndex()]);
                t && t.addClass("active")
            }));
            if (e.support.transition && this.$element.hasClass("slide")) {
                this.$element.trigger(f);
                if (f.isDefaultPrevented()) return;
                i.addClass(t), i[0].offsetWidth, r.addClass(o), i.addClass(o), this.$element.one(e.support.transition.end, function () {
                    i.removeClass([t, o].join(" ")).addClass("active"), r.removeClass(["active", o].join(" ")), a.sliding = !1, setTimeout(function () {
                        a.$element.trigger("slid")
                    }, 0)
                })
            } else {
                this.$element.trigger(f);
                if (f.isDefaultPrevented()) return;
                r.removeClass("active"), i.addClass("active"), this.sliding = !1, this.$element.trigger("slid")
            }
            return s && this.cycle(), this
        }
    };
    var n = e.fn.carousel;
    e.fn.carousel = function (n) {
        return this.each(function () {
            var r = e(this),
                i = r.data("carousel"),
                s = e.extend({}, e.fn.carousel.defaults, typeof n == "object" && n),
                o = typeof n == "string" ? n : s.slide;
            i || r.data("carousel", i = new t(this, s)), typeof n == "number" ? i.to(n) : o ? i[o]() : s.interval && i.pause().cycle()
        })
    }, e.fn.carousel.defaults = {
        interval: 5e3,
        pause: "hover"
    }, e.fn.carousel.Constructor = t, e.fn.carousel.noConflict = function () {
        return e.fn.carousel = n, this
    }, e(document).on("click.carousel.data-api", "[data-slide], [data-slide-to]", function (t) {
        var n = e(this),
            r, i = e(n.attr("data-target") || (r = n.attr("href")) && r.replace(/.*(?=#[^\s]+$)/, "")),
            s = e.extend({}, i.data(), n.data()),
            o;
        i.carousel(s), (o = n.attr("data-slide-to")) && i.data("carousel").pause().to(o).cycle(), t.preventDefault()
    })
}(window.jQuery), ! function (e) {
    "use strict";
    var t = function (t, n) {
        this.$element = e(t), this.options = e.extend({}, e.fn.collapse.defaults, n), this.options.parent && (this.$parent = e(this.options.parent)), this.options.toggle && this.toggle()
    };
    t.prototype = {
        constructor: t,
        dimension: function () {
            var e = this.$element.hasClass("width");
            return e ? "width" : "height"
        },
        show: function () {
            var t, n, r, i;
            if (this.transitioning || this.$element.hasClass("in")) return;
            t = this.dimension(), n = e.camelCase(["scroll", t].join("-")), r = this.$parent && this.$parent.find("> .accordion-group > .in");
            if (r && r.length) {
                i = r.data("collapse");
                if (i && i.transitioning) return;
                r.collapse("hide"), i || r.data("collapse", null)
            }
            this.$element[t](0), this.transition("addClass", e.Event("show"), "shown"), e.support.transition && this.$element[t](this.$element[0][n])
        },
        hide: function () {
            var t;
            if (this.transitioning || !this.$element.hasClass("in")) return;
            t = this.dimension(), this.reset(this.$element[t]()), this.transition("removeClass", e.Event("hide"), "hidden"), this.$element[t](0)
        },
        reset: function (e) {
            var t = this.dimension();
            return this.$element.removeClass("collapse")[t](e || "auto")[0].offsetWidth, this.$element[e !== null ? "addClass" : "removeClass"]("collapse"), this
        },
        transition: function (t, n, r) {
            var i = this,
                s = function () {
                    n.type == "show" && i.reset(), i.transitioning = 0, i.$element.trigger(r)
                };
            this.$element.trigger(n);
            if (n.isDefaultPrevented()) return;
            this.transitioning = 1, this.$element[t]("in"), e.support.transition && this.$element.hasClass("collapse") ? this.$element.one(e.support.transition.end, s) : s()
        },
        toggle: function () {
            this[this.$element.hasClass("in") ? "hide" : "show"]()
        }
    };
    var n = e.fn.collapse;
    e.fn.collapse = function (n) {
        return this.each(function () {
            var r = e(this),
                i = r.data("collapse"),
                s = e.extend({}, e.fn.collapse.defaults, r.data(), typeof n == "object" && n);
            i || r.data("collapse", i = new t(this, s)), typeof n == "string" && i[n]()
        })
    }, e.fn.collapse.defaults = {
        toggle: !0
    }, e.fn.collapse.Constructor = t, e.fn.collapse.noConflict = function () {
        return e.fn.collapse = n, this
    }, e(document).on("click.collapse.data-api", "[data-toggle=collapse]", function (t) {
        var n = e(this),
            r, i = n.attr("data-target") || t.preventDefault() || (r = n.attr("href")) && r.replace(/.*(?=#[^\s]+$)/, ""),
            s = e(i).data("collapse") ? "toggle" : n.data();
        n[e(i).hasClass("in") ? "addClass" : "removeClass"]("collapsed"), e(i).collapse(s)
    })
}(window.jQuery), ! function (e) {
    "use strict";

    function r() {
        e(t).each(function () {
            i(e(this)).removeClass("open")
        })
    }

    function i(t) {
        var n = t.attr("data-target"),
            r;
        n || (n = t.attr("href"), n = n && /#/.test(n) && n.replace(/.*(?=#[^\s]*$)/, "")), r = n && e(n);
        if (!r || !r.length) r = t.parent();
        return r
    }
    var t = "[data-toggle=dropdown]",
        n = function (t) {
            var n = e(t).on("click.dropdown.data-api", this.toggle);
            e("html").on("click.dropdown.data-api", function () {
                n.parent().removeClass("open")
            })
        };
    n.prototype = {
        constructor: n,
        toggle: function (t) {
            var n = e(this),
                s, o;
            if (n.is(".disabled, :disabled")) return;
            return s = i(n), o = s.hasClass("open"), r(), o || s.toggleClass("open"), n.focus(), !1
        },
        keydown: function (n) {
            var r, s, o, u, a, f;
            if (!/(38|40|27)/.test(n.keyCode)) return;
            r = e(this), n.preventDefault(), n.stopPropagation();
            if (r.is(".disabled, :disabled")) return;
            u = i(r), a = u.hasClass("open");
            if (!a || a && n.keyCode == 27) return n.which == 27 && u.find(t).focus(), r.click();
            s = e("[role=menu] li:not(.divider):visible a", u);
            if (!s.length) return;
            f = s.index(s.filter(":focus")), n.keyCode == 38 && f > 0 && f--, n.keyCode == 40 && f < s.length - 1 && f++, ~f || (f = 0), s.eq(f).focus()
        }
    };
    var s = e.fn.dropdown;
    e.fn.dropdown = function (t) {
        return this.each(function () {
            var r = e(this),
                i = r.data("dropdown");
            i || r.data("dropdown", i = new n(this)), typeof t == "string" && i[t].call(r)
        })
    }, e.fn.dropdown.Constructor = n, e.fn.dropdown.noConflict = function () {
        return e.fn.dropdown = s, this
    }, e(document).on("click.dropdown.data-api", r).on("click.dropdown.data-api", ".dropdown form", function (e) {
        e.stopPropagation()
    }).on("click.dropdown-menu", function (e) {
        e.stopPropagation()
    }).on("click.dropdown.data-api", t, n.prototype.toggle).on("keydown.dropdown.data-api", t + ", [role=menu]", n.prototype.keydown)
}(window.jQuery), ! function (e) {
    "use strict";
    var t = function (t, n) {
        this.options = n, this.$element = e(t).delegate('[data-dismiss="modal"]', "click.dismiss.modal", e.proxy(this.hide, this)), this.options.remote && this.$element.find(".modal-body").load(this.options.remote)
    };
    t.prototype = {
        constructor: t,
        toggle: function () {
            return this[this.isShown ? "hide" : "show"]()
        },
        show: function () {
            var t = this,
                n = e.Event("show");
            this.$element.trigger(n);
            if (this.isShown || n.isDefaultPrevented()) return;
            this.isShown = !0, this.escape(), this.backdrop(function () {
                var n = e.support.transition && t.$element.hasClass("fade");
                t.$element.parent().length || t.$element.appendTo(document.body), t.$element.show(), n && t.$element[0].offsetWidth, t.$element.addClass("in").attr("aria-hidden", !1), t.enforceFocus(), n ? t.$element.one(e.support.transition.end, function () {
                    t.$element.focus().trigger("shown")
                }) : t.$element.focus().trigger("shown")
            })
        },
        hide: function (t) {
            t && t.preventDefault();
            var n = this;
            t = e.Event("hide"), this.$element.trigger(t);
            if (!this.isShown || t.isDefaultPrevented()) return;
            this.isShown = !1, this.escape(), e(document).off("focusin.modal"), this.$element.removeClass("in").attr("aria-hidden", !0), e.support.transition && this.$element.hasClass("fade") ? this.hideWithTransition() : this.hideModal()
        },
        enforceFocus: function () {
            var t = this;
            e(document).on("focusin.modal", function (e) {
                t.$element[0] !== e.target && !t.$element.has(e.target).length && t.$element.focus()
            })
        },
        escape: function () {
            var e = this;
            this.isShown && this.options.keyboard ? this.$element.on("keyup.dismiss.modal", function (t) {
                t.which == 27 && e.hide()
            }) : this.isShown || this.$element.off("keyup.dismiss.modal")
        },
        hideWithTransition: function () {
            var t = this,
                n = setTimeout(function () {
                    t.$element.off(e.support.transition.end), t.hideModal()
                }, 500);
            this.$element.one(e.support.transition.end, function () {
                clearTimeout(n), t.hideModal()
            })
        },
        hideModal: function () {
            var e = this;
            this.$element.hide(), this.backdrop(function () {
                e.removeBackdrop(), e.$element.trigger("hidden")
            })
        },
        removeBackdrop: function () {
            this.$backdrop && this.$backdrop.remove(), this.$backdrop = null
        },
        backdrop: function (t) {
            var n = this,
                r = this.$element.hasClass("fade") ? "fade" : "";
            if (this.isShown && this.options.backdrop) {
                var i = e.support.transition && r;
                this.$backdrop = e('<div class="modal-backdrop ' + r + '" />').appendTo(document.body), this.$backdrop.click(this.options.backdrop == "static" ? e.proxy(this.$element[0].focus, this.$element[0]) : e.proxy(this.hide, this)), i && this.$backdrop[0].offsetWidth, this.$backdrop.addClass("in");
                if (!t) return;
                i ? this.$backdrop.one(e.support.transition.end, t) : t()
            } else !this.isShown && this.$backdrop ? (this.$backdrop.removeClass("in"), e.support.transition && this.$element.hasClass("fade") ? this.$backdrop.one(e.support.transition.end, t) : t()) : t && t()
        }
    };
    var n = e.fn.modal;
    e.fn.modal = function (n) {
        return this.each(function () {
            var r = e(this),
                i = r.data("modal"),
                s = e.extend({}, e.fn.modal.defaults, r.data(), typeof n == "object" && n);
            i || r.data("modal", i = new t(this, s)), typeof n == "string" ? i[n]() : s.show && i.show()
        })
    }, e.fn.modal.defaults = {
        backdrop: !0,
        keyboard: !0,
        show: !0
    }, e.fn.modal.Constructor = t, e.fn.modal.noConflict = function () {
        return e.fn.modal = n, this
    }, e(document).on("click.modal.data-api", '[data-toggle="modal"]', function (t) {
        var n = e(this),
            r = n.attr("href"),
            i = e(n.attr("data-target") || r && r.replace(/.*(?=#[^\s]+$)/, "")),
            s = i.data("modal") ? "toggle" : e.extend({
                remote: !/#/.test(r) && r
            }, i.data(), n.data());
        t.preventDefault(), i.modal(s).one("hide", function () {
            n.focus()
        })
    })
}(window.jQuery), ! function (e) {
    "use strict";
    var t = function (e, t) {
        this.init("tooltip", e, t)
    };
    t.prototype = {
        constructor: t,
        init: function (t, n, r) {
            var i, s, o, u, a;
            this.type = t, this.$element = e(n), this.options = this.getOptions(r), this.enabled = !0, o = this.options.trigger.split(" ");
            for (a = o.length; a--;) u = o[a], u == "click" ? this.$element.on("click." + this.type, this.options.selector, e.proxy(this.toggle, this)) : u != "manual" && (i = u == "hover" ? "mouseenter" : "focus", s = u == "hover" ? "mouseleave" : "blur", this.$element.on(i + "." + this.type, this.options.selector, e.proxy(this.enter, this)), this.$element.on(s + "." + this.type, this.options.selector, e.proxy(this.leave, this)));
            this.options.selector ? this._options = e.extend({}, this.options, {
                trigger: "manual",
                selector: ""
            }) : this.fixTitle()
        },
        getOptions: function (t) {
            return t = e.extend({}, e.fn[this.type].defaults, this.$element.data(), t), t.delay && typeof t.delay == "number" && (t.delay = {
                show: t.delay,
                hide: t.delay
            }), t
        },
        enter: function (t) {
            var n = e.fn[this.type].defaults,
                r = {}, i;
            this._options && e.each(this._options, function (e, t) {
                n[e] != t && (r[e] = t)
            }, this), i = e(t.currentTarget)[this.type](r).data(this.type);
            if (!i.options.delay || !i.options.delay.show) return i.show();
            clearTimeout(this.timeout), i.hoverState = "in", this.timeout = setTimeout(function () {
                i.hoverState == "in" && i.show()
            }, i.options.delay.show)
        },
        leave: function (t) {
            var n = e(t.currentTarget)[this.type](this._options).data(this.type);
            this.timeout && clearTimeout(this.timeout);
            if (!n.options.delay || !n.options.delay.hide) return n.hide();
            n.hoverState = "out", this.timeout = setTimeout(function () {
                n.hoverState == "out" && n.hide()
            }, n.options.delay.hide)
        },
        show: function () {
            var t, n, r, i, s, o, u = e.Event("show");
            if (this.hasContent() && this.enabled) {
                this.$element.trigger(u);
                if (u.isDefaultPrevented()) return;
                t = this.tip(), this.setContent(), this.options.animation && t.addClass("fade"), s = typeof this.options.placement == "function" ? this.options.placement.call(this, t[0], this.$element[0]) : this.options.placement, t.detach().css({
                    top: 0,
                    left: 0,
                    display: "block"
                }), this.options.container ? t.appendTo(this.options.container) : t.insertAfter(this.$element), n = this.getPosition(), r = t[0].offsetWidth, i = t[0].offsetHeight;
                switch (s) {
                case "bottom":
                    o = {
                        top: n.top + n.height,
                        left: n.left + n.width / 2 - r / 2
                    };
                    break;
                case "top":
                    o = {
                        top: n.top - i,
                        left: n.left + n.width / 2 - r / 2
                    };
                    break;
                case "left":
                    o = {
                        top: n.top + n.height / 2 - i / 2,
                        left: n.left - r
                    };
                    break;
                case "right":
                    o = {
                        top: n.top + n.height / 2 - i / 2,
                        left: n.left + n.width
                    }
                }
                this.applyPlacement(o, s), this.$element.trigger("shown")
            }
        },
        applyPlacement: function (e, t) {
            var n = this.tip(),
                r = n[0].offsetWidth,
                i = n[0].offsetHeight,
                s, o, u, a;
            n.offset(e).addClass(t).addClass("in"), s = n[0].offsetWidth, o = n[0].offsetHeight, t == "top" && o != i && (e.top = e.top + i - o, a = !0), t == "bottom" || t == "top" ? (u = 0, e.left < 0 && (u = e.left * -2, e.left = 0, n.offset(e), s = n[0].offsetWidth, o = n[0].offsetHeight), this.replaceArrow(u - r + s, s, "left")) : this.replaceArrow(o - i, o, "top"), a && n.offset(e)
        },
        replaceArrow: function (e, t, n) {
            this.arrow().css(n, e ? 50 * (1 - e / t) + "%" : "")
        },
        setContent: function () {
            var e = this.tip(),
                t = this.getTitle();
            e.find(".tooltip-inner")[this.options.html ? "html" : "text"](t), e.removeClass("fade in top bottom left right")
        },
        hide: function () {
            function i() {
                var t = setTimeout(function () {
                    n.off(e.support.transition.end).detach()
                }, 500);
                n.one(e.support.transition.end, function () {
                    clearTimeout(t), n.detach()
                })
            }
            var t = this,
                n = this.tip(),
                r = e.Event("hide");
            this.$element.trigger(r);
            if (r.isDefaultPrevented()) return;
            return n.removeClass("in"), e.support.transition && this.$tip.hasClass("fade") ? i() : n.detach(), this.$element.trigger("hidden"), this
        },
        fixTitle: function () {
            var e = this.$element;
            (e.attr("title") || typeof e.attr("data-original-title") != "string") && e.attr("data-original-title", e.attr("title") || "").attr("title", "")
        },
        hasContent: function () {
            return this.getTitle()
        },
        getPosition: function () {
            var t = this.$element[0];
            return e.extend({}, typeof t.getBoundingClientRect == "function" ? t.getBoundingClientRect() : {
                width: t.offsetWidth,
                height: t.offsetHeight
            }, this.$element.offset())
        },
        getTitle: function () {
            var e, t = this.$element,
                n = this.options;
            return e = t.attr("data-original-title") || (typeof n.title == "function" ? n.title.call(t[0]) : n.title), e
        },
        tip: function () {
            return this.$tip = this.$tip || e(this.options.template)
        },
        arrow: function () {
            return this.$arrow = this.$arrow || this.tip().find(".tooltip-arrow")
        },
        validate: function () {
            this.$element[0].parentNode || (this.hide(), this.$element = null, this.options = null)
        },
        enable: function () {
            this.enabled = !0
        },
        disable: function () {
            this.enabled = !1
        },
        toggleEnabled: function () {
            this.enabled = !this.enabled
        },
        toggle: function (t) {
            var n = t ? e(t.currentTarget)[this.type](this._options).data(this.type) : this;
            n.tip().hasClass("in") ? n.hide() : n.show()
        },
        destroy: function () {
            this.hide().$element.off("." + this.type).removeData(this.type)
        }
    };
    var n = e.fn.tooltip;
    e.fn.tooltip = function (n) {
        return this.each(function () {
            var r = e(this),
                i = r.data("tooltip"),
                s = typeof n == "object" && n;
            i || r.data("tooltip", i = new t(this, s)), typeof n == "string" && i[n]()
        })
    }, e.fn.tooltip.Constructor = t, e.fn.tooltip.defaults = {
        animation: !0,
        placement: "top",
        selector: !1,
        template: '<div class="tooltip"><div class="tooltip-arrow"></div><div class="tooltip-inner"></div></div>',
        trigger: "hover focus",
        title: "",
        delay: 0,
        html: !1,
        container: !1
    }, e.fn.tooltip.noConflict = function () {
        return e.fn.tooltip = n, this
    }
}(window.jQuery), ! function (e) {
    "use strict";
    var t = function (e, t) {
        this.init("popover", e, t)
    };
    t.prototype = e.extend({}, e.fn.tooltip.Constructor.prototype, {
        constructor: t,
        setContent: function () {
            var e = this.tip(),
                t = this.getTitle(),
                n = this.getContent();
            e.find(".popover-title")[this.options.html ? "html" : "text"](t), e.find(".popover-content")[this.options.html ? "html" : "text"](n), e.removeClass("fade top bottom left right in")
        },
        hasContent: function () {
            return this.getTitle() || this.getContent()
        },
        getContent: function () {
            var e, t = this.$element,
                n = this.options;
            return e = (typeof n.content == "function" ? n.content.call(t[0]) : n.content) || t.attr("data-content"), e
        },
        tip: function () {
            return this.$tip || (this.$tip = e(this.options.template)), this.$tip
        },
        destroy: function () {
            this.hide().$element.off("." + this.type).removeData(this.type)
        }
    });
    var n = e.fn.popover;
    e.fn.popover = function (n) {
        return this.each(function () {
            var r = e(this),
                i = r.data("popover"),
                s = typeof n == "object" && n;
            i || r.data("popover", i = new t(this, s)), typeof n == "string" && i[n]()
        })
    }, e.fn.popover.Constructor = t, e.fn.popover.defaults = e.extend({}, e.fn.tooltip.defaults, {
        placement: "right",
        trigger: "click",
        content: "",
        template: '<div class="popover"><div class="arrow"></div><h3 class="popover-title"></h3><div class="popover-content"></div></div>'
    }), e.fn.popover.noConflict = function () {
        return e.fn.popover = n, this
    }
}(window.jQuery), ! function (e) {
    "use strict";

    function t(t, n) {
        var r = e.proxy(this.process, this),
            i = e(t).is("body") ? e(window) : e(t),
            s;
        this.options = e.extend({}, e.fn.scrollspy.defaults, n), this.$scrollElement = i.on("scroll.scroll-spy.data-api", r), this.selector = (this.options.target || (s = e(t).attr("href")) && s.replace(/.*(?=#[^\s]+$)/, "") || "") + " .nav li > a", this.$body = e("body"), this.refresh(), this.process()
    }
    t.prototype = {
        constructor: t,
        refresh: function () {
            var t = this,
                n;
            this.offsets = e([]), this.targets = e([]), n = this.$body.find(this.selector).map(function () {
                var n = e(this),
                    r = n.data("target") || n.attr("href"),
                    i = /^#\w/.test(r) && e(r);
                return i && i.length && [
                    [i.position().top + (!e.isWindow(t.$scrollElement.get(0)) && t.$scrollElement.scrollTop()), r]
                ] || null
            }).sort(function (e, t) {
                return e[0] - t[0]
            }).each(function () {
                t.offsets.push(this[0]), t.targets.push(this[1])
            })
        },
        process: function () {
            var e = this.$scrollElement.scrollTop() + this.options.offset,
                t = this.$scrollElement[0].scrollHeight || this.$body[0].scrollHeight,
                n = t - this.$scrollElement.height(),
                r = this.offsets,
                i = this.targets,
                s = this.activeTarget,
                o;
            if (e >= n) return s != (o = i.last()[0]) && this.activate(o);
            for (o = r.length; o--;) s != i[o] && e >= r[o] && (!r[o + 1] || e <= r[o + 1]) && this.activate(i[o])
        },
        activate: function (t) {
            var n, r;
            this.activeTarget = t, e(this.selector).parent(".active").removeClass("active"), r = this.selector + '[data-target="' + t + '"],' + this.selector + '[href="' + t + '"]', n = e(r).parent("li").addClass("active"), n.parent(".dropdown-menu").length && (n = n.closest("li.dropdown").addClass("active")), n.trigger("activate")
        }
    };
    var n = e.fn.scrollspy;
    e.fn.scrollspy = function (n) {
        return this.each(function () {
            var r = e(this),
                i = r.data("scrollspy"),
                s = typeof n == "object" && n;
            i || r.data("scrollspy", i = new t(this, s)), typeof n == "string" && i[n]()
        })
    }, e.fn.scrollspy.Constructor = t, e.fn.scrollspy.defaults = {
        offset: 10
    }, e.fn.scrollspy.noConflict = function () {
        return e.fn.scrollspy = n, this
    }, e(window).on("load", function () {
        e('[data-spy="scroll"]').each(function () {
            var t = e(this);
            t.scrollspy(t.data())
        })
    })
}(window.jQuery), ! function (e) {
    "use strict";
    var t = function (t) {
        this.element = e(t)
    };
    t.prototype = {
        constructor: t,
        show: function () {
            var t = this.element,
                n = t.closest("ul:not(.dropdown-menu)"),
                r = t.attr("data-target"),
                i, s, o;
            r || (r = t.attr("href"), r = r && r.replace(/.*(?=#[^\s]*$)/, ""));
            if (t.parent("li").hasClass("active")) return;
            i = n.find(".active:last a")[0], o = e.Event("show", {
                relatedTarget: i
            }), t.trigger(o);
            if (o.isDefaultPrevented()) return;
            s = e(r), this.activate(t.parent("li"), n), this.activate(s, s.parent(), function () {
                t.trigger({
                    type: "shown",
                    relatedTarget: i
                })
            })
        },
        activate: function (t, n, r) {
            function o() {
                i.removeClass("active").find("> .dropdown-menu > .active").removeClass("active"), t.addClass("active"), s ? (t[0].offsetWidth, t.addClass("in")) : t.removeClass("fade"), t.parent(".dropdown-menu") && t.closest("li.dropdown").addClass("active"), r && r()
            }
            var i = n.find("> .active"),
                s = r && e.support.transition && i.hasClass("fade");
            s ? i.one(e.support.transition.end, o) : o(), i.removeClass("in")
        }
    };
    var n = e.fn.tab;
    e.fn.tab = function (n) {
        return this.each(function () {
            var r = e(this),
                i = r.data("tab");
            i || r.data("tab", i = new t(this)), typeof n == "string" && i[n]()
        })
    }, e.fn.tab.Constructor = t, e.fn.tab.noConflict = function () {
        return e.fn.tab = n, this
    }, e(document).on("click.tab.data-api", '[data-toggle="tab"], [data-toggle="pill"]', function (t) {
        t.preventDefault(), e(this).tab("show")
    })
}(window.jQuery), ! function (e) {
    "use strict";
    var t = function (t, n) {
        this.$element = e(t), this.options = e.extend({}, e.fn.typeahead.defaults, n), this.matcher = this.options.matcher || this.matcher, this.sorter = this.options.sorter || this.sorter, this.highlighter = this.options.highlighter || this.highlighter, this.updater = this.options.updater || this.updater, this.source = this.options.source, this.$menu = e(this.options.menu), this.shown = !1, this.listen()
    };
    t.prototype = {
        constructor: t,
        select: function () {
            var e = this.$menu.find(".active").attr("data-value");
            return this.$element.val(this.updater(e)).change(), this.hide()
        },
        updater: function (e) {
            return e
        },
        show: function () {
            var t = e.extend({}, this.$element.position(), {
                height: this.$element[0].offsetHeight
            });
            return this.$menu.insertAfter(this.$element).css({
                top: t.top + t.height,
                left: t.left
            }).show(), this.shown = !0, this
        },
        hide: function () {
            return this.$menu.hide(), this.shown = !1, this
        },
        lookup: function (t) {
            var n;
            return this.query = this.$element.val(), !this.query || this.query.length < this.options.minLength ? this.shown ? this.hide() : this : (n = e.isFunction(this.source) ? this.source(this.query, e.proxy(this.process, this)) : this.source, n ? this.process(n) : this)
        },
        process: function (t) {
            var n = this;
            return t = e.grep(t, function (e) {
                return n.matcher(e)
            }), t = this.sorter(t), t.length ? this.render(t.slice(0, this.options.items)).show() : this.shown ? this.hide() : this
        },
        matcher: function (e) {
            return~ e.toLowerCase().indexOf(this.query.toLowerCase())
        },
        sorter: function (e) {
            var t = [],
                n = [],
                r = [],
                i;
            while (i = e.shift()) i.toLowerCase().indexOf(this.query.toLowerCase()) ? ~i.indexOf(this.query) ? n.push(i) : r.push(i) : t.push(i);
            return t.concat(n, r)
        },
        highlighter: function (e) {
            var t = this.query.replace(/[\-\[\]{}()*+?.,\\\^$|#\s]/g, "\\$&");
            return e.replace(new RegExp("(" + t + ")", "ig"), function (e, t) {
                return "<strong>" + t + "</strong>"
            })
        },
        render: function (t) {
            var n = this;
            return t = e(t).map(function (t, r) {
                return t = e(n.options.item).attr("data-value", r), t.find("a").html(n.highlighter(r)), t[0]
            }), t.first().addClass("active"), this.$menu.html(t), this
        },
        next: function (t) {
            var n = this.$menu.find(".active").removeClass("active"),
                r = n.next();
            r.length || (r = e(this.$menu.find("li")[0])), r.addClass("active")
        },
        prev: function (e) {
            var t = this.$menu.find(".active").removeClass("active"),
                n = t.prev();
            n.length || (n = this.$menu.find("li").last()), n.addClass("active")
        },
        listen: function () {
            this.$element.on("focus", e.proxy(this.focus, this)).on("blur", e.proxy(this.blur, this)).on("keypress", e.proxy(this.keypress, this)).on("keyup", e.proxy(this.keyup, this)), this.eventSupported("keydown") && this.$element.on("keydown", e.proxy(this.keydown, this)), this.$menu.on("click", e.proxy(this.click, this)).on("mouseenter", "li", e.proxy(this.mouseenter, this)).on("mouseleave", "li", e.proxy(this.mouseleave, this))
        },
        eventSupported: function (e) {
            var t = e in this.$element;
            return t || (this.$element.setAttribute(e, "return;"), t = typeof this.$element[e] == "function"), t
        },
        move: function (e) {
            if (!this.shown) return;
            switch (e.keyCode) {
            case 9:
            case 13:
            case 27:
                e.preventDefault();
                break;
            case 38:
                e.preventDefault(), this.prev();
                break;
            case 40:
                e.preventDefault(), this.next()
            }
            e.stopPropagation()
        },
        keydown: function (t) {
            this.suppressKeyPressRepeat = ~e.inArray(t.keyCode, [40, 38, 9, 13, 27]), this.move(t)
        },
        keypress: function (e) {
            if (this.suppressKeyPressRepeat) return;
            this.move(e)
        },
        keyup: function (e) {
            switch (e.keyCode) {
            case 40:
            case 38:
            case 16:
            case 17:
            case 18:
                break;
            case 9:
            case 13:
                if (!this.shown) return;
                this.select();
                break;
            case 27:
                if (!this.shown) return;
                this.hide();
                break;
            default:
                this.lookup()
            }
            e.stopPropagation(), e.preventDefault()
        },
        focus: function (e) {
            this.focused = !0
        },
        blur: function (e) {
            this.focused = !1, !this.mousedover && this.shown && this.hide()
        },
        click: function (e) {
            e.stopPropagation(), e.preventDefault(), this.select(), this.$element.focus()
        },
        mouseenter: function (t) {
            this.mousedover = !0, this.$menu.find(".active").removeClass("active"), e(t.currentTarget).addClass("active")
        },
        mouseleave: function (e) {
            this.mousedover = !1, !this.focused && this.shown && this.hide()
        }
    };
    var n = e.fn.typeahead;
    e.fn.typeahead = function (n) {
        return this.each(function () {
            var r = e(this),
                i = r.data("typeahead"),
                s = typeof n == "object" && n;
            i || r.data("typeahead", i = new t(this, s)), typeof n == "string" && i[n]()
        })
    }, e.fn.typeahead.defaults = {
        source: [],
        items: 8,
        menu: '<ul class="typeahead dropdown-menu"></ul>',
        item: '<li><a href="#"></a></li>',
        minLength: 1
    }, e.fn.typeahead.Constructor = t, e.fn.typeahead.noConflict = function () {
        return e.fn.typeahead = n, this
    }, e(document).on("focus.typeahead.data-api", '[data-provide="typeahead"]', function (t) {
        var n = e(this);
        if (n.data("typeahead")) return;
        n.typeahead(n.data())
    })
}(window.jQuery), ! function (e) {
    "use strict";
    var t = function (t, n) {
        this.options = e.extend({}, e.fn.affix.defaults, n), this.$window = e(window).on("scroll.affix.data-api", e.proxy(this.checkPosition, this)).on("click.affix.data-api", e.proxy(function () {
            setTimeout(e.proxy(this.checkPosition, this), 1)
        }, this)), this.$element = e(t), this.checkPosition()
    };
    t.prototype.checkPosition = function () {
        if (!this.$element.is(":visible")) return;
        var t = e(document).height(),
            n = this.$window.scrollTop(),
            r = this.$element.offset(),
            i = this.options.offset,
            s = i.bottom,
            o = i.top,
            u = "affix affix-top affix-bottom",
            a;
        typeof i != "object" && (s = o = i), typeof o == "function" && (o = i.top()), typeof s == "function" && (s = i.bottom()), a = this.unpin != null && n + this.unpin <= r.top ? !1 : s != null && r.top + this.$element.height() >= t - s ? "bottom" : o != null && n <= o ? "top" : !1;
        if (this.affixed === a) return;
        this.affixed = a, this.unpin = a == "bottom" ? r.top - n : null, this.$element.removeClass(u).addClass("affix" + (a ? "-" + a : ""))
    };
    var n = e.fn.affix;
    e.fn.affix = function (n) {
        return this.each(function () {
            var r = e(this),
                i = r.data("affix"),
                s = typeof n == "object" && n;
            i || r.data("affix", i = new t(this, s)), typeof n == "string" && i[n]()
        })
    }, e.fn.affix.Constructor = t, e.fn.affix.defaults = {
        offset: 0
    }, e.fn.affix.noConflict = function () {
        return e.fn.affix = n, this
    }, e(window).on("load", function () {
        e('[data-spy="affix"]').each(function () {
            var t = e(this),
                n = t.data();
            n.offset = n.offset || {}, n.offsetBottom && (n.offset.bottom = n.offsetBottom), n.offsetTop && (n.offset.top = n.offsetTop), t.affix(n)
        })
    })
}(window.jQuery);

/*
    Scroll Fixed
 */

//(function(a){a.isScrollToFixed=function(b){return a(b).data("ScrollToFixed")!==undefined};a.ScrollToFixed=function(d,h){var k=this;k.$el=a(d);k.el=d;k.$el.data("ScrollToFixed",k);var c=false;var F=k.$el;var G;var D;var p;var C=0;var q=0;var i=-1;var e=-1;var t=null;var y;var f;function u(){F.trigger("preUnfixed.ScrollToFixed");j();F.trigger("unfixed.ScrollToFixed");e=-1;C=F.offset().top;q=F.offset().left;if(k.options.offsets){q+=(F.offset().left-F.position().left)}if(i==-1){i=q}G=F.css("position");c=true;if(k.options.bottom!=-1){F.trigger("preFixed.ScrollToFixed");w();F.trigger("fixed.ScrollToFixed")}}function m(){var H=k.options.limit;if(!H){return 0}if(typeof(H)==="function"){return H.apply(F)}return H}function o(){return G==="fixed"}function x(){return G==="absolute"}function g(){return !(o()||x())}function w(){if(!o()){t.css({display:F.css("display"),width:F.outerWidth(true),height:F.outerHeight(true),"float":F.css("float")});cssOptions={position:"fixed",top:k.options.bottom==-1?s():"",bottom:k.options.bottom==-1?"":k.options.bottom,"margin-left":"0px"};if(!k.options.dontSetWidth){cssOptions.width=F.width()}F.css(cssOptions);F.addClass("scroll-to-fixed-fixed");if(k.options.className){F.addClass(k.options.className)}G="fixed"}}function b(){var I=m();var H=q;if(k.options.removeOffsets){H=0;I=I-C}cssOptions={position:"absolute",top:I,left:H,"margin-left":"0px",bottom:""};if(!k.options.dontSetWidth){cssOptions.width=F.width()}F.css(cssOptions);G="absolute"}function j(){if(!g()){e=-1;t.css("display","none");F.css({width:"",position:D,left:"",top:p.top,"margin-left":""});F.removeClass("scroll-to-fixed-fixed");if(k.options.className){F.removeClass(k.options.className)}G=null}}function v(H){if(H!=e){F.css("left",q-H);e=H}}function s(){var H=k.options.marginTop;if(!H){return 0}if(typeof(H)==="function"){return H.apply(F)}return H}function z(){if(!a.isScrollToFixed(F)){return}var J=c;if(!c){u()}var H=a(window).scrollLeft();var K=a(window).scrollTop();var I=m();if(k.options.minWidth&&a(window).width()<k.options.minWidth){if(!g()||!J){n();F.trigger("preUnfixed.ScrollToFixed");j();F.trigger("unfixed.ScrollToFixed")}}else{if(k.options.bottom==-1){if(I>0&&K>=I-s()){if(!x()||!J){n();F.trigger("preAbsolute.ScrollToFixed");b();F.trigger("unfixed.ScrollToFixed")}}else{if(K>=C-s()){if(!o()||!J){n();F.trigger("preFixed.ScrollToFixed");w();e=-1;F.trigger("fixed.ScrollToFixed")}v(H)}else{if(!g()||!J){n();F.trigger("preUnfixed.ScrollToFixed");j();F.trigger("unfixed.ScrollToFixed")}}}}else{if(I>0){if(K+a(window).height()-F.outerHeight(true)>=I-(s()||-l())){if(o()){n();F.trigger("preUnfixed.ScrollToFixed");if(D==="absolute"){b()}else{j()}F.trigger("unfixed.ScrollToFixed")}}else{if(!o()){n();F.trigger("preFixed.ScrollToFixed");w()}v(H);F.trigger("fixed.ScrollToFixed")}}else{v(H)}}}}function l(){if(!k.options.bottom){return 0}return k.options.bottom}function n(){var H=F.css("position");if(H=="absolute"){F.trigger("postAbsolute.ScrollToFixed")}else{if(H=="fixed"){F.trigger("postFixed.ScrollToFixed")}else{F.trigger("postUnfixed.ScrollToFixed")}}}var B=function(H){if(F.is(":visible")){c=false;z()}};var E=function(H){z()};var A=function(){var I=document.body;if(document.createElement&&I&&I.appendChild&&I.removeChild){var K=document.createElement("div");if(!K.getBoundingClientRect){return null}K.innerHTML="x";K.style.cssText="position:fixed;top:100px;";I.appendChild(K);var L=I.style.height,M=I.scrollTop;I.style.height="3000px";I.scrollTop=500;var H=K.getBoundingClientRect().top;I.style.height=L;var J=(H===100);I.removeChild(K);I.scrollTop=M;return J}return null};var r=function(H){H=H||window.event;if(H.preventDefault){H.preventDefault()}H.returnValue=false};k.init=function(){k.options=a.extend({},a.ScrollToFixed.defaultOptions,h);k.$el.css("z-index",k.options.zIndex);t=a("<div />");G=F.css("position");D=F.css("position");p=a.extend({},F.offset());if(g()){k.$el.after(t)}a(window).bind("resize.ScrollToFixed",B);a(window).bind("scroll.ScrollToFixed",E);if(k.options.preFixed){F.bind("preFixed.ScrollToFixed",k.options.preFixed)}if(k.options.postFixed){F.bind("postFixed.ScrollToFixed",k.options.postFixed)}if(k.options.preUnfixed){F.bind("preUnfixed.ScrollToFixed",k.options.preUnfixed)}if(k.options.postUnfixed){F.bind("postUnfixed.ScrollToFixed",k.options.postUnfixed)}if(k.options.preAbsolute){F.bind("preAbsolute.ScrollToFixed",k.options.preAbsolute)}if(k.options.postAbsolute){F.bind("postAbsolute.ScrollToFixed",k.options.postAbsolute)}if(k.options.fixed){F.bind("fixed.ScrollToFixed",k.options.fixed)}if(k.options.unfixed){F.bind("unfixed.ScrollToFixed",k.options.unfixed)}if(k.options.spacerClass){t.addClass(k.options.spacerClass)}F.bind("resize.ScrollToFixed",function(){t.height(F.height())});F.bind("scroll.ScrollToFixed",function(){F.trigger("preUnfixed.ScrollToFixed");j();F.trigger("unfixed.ScrollToFixed");z()});F.bind("detach.ScrollToFixed",function(H){r(H);F.trigger("preUnfixed.ScrollToFixed");j();F.trigger("unfixed.ScrollToFixed");a(window).unbind("resize.ScrollToFixed",B);a(window).unbind("scroll.ScrollToFixed",E);F.unbind(".ScrollToFixed");k.$el.removeData("ScrollToFixed")});B()};k.init()};a.ScrollToFixed.defaultOptions={marginTop:0,limit:0,bottom:-1,zIndex:1000};a.fn.scrollToFixed=function(b){return this.each(function(){(new a.ScrollToFixed(this,b))})}})(jQuery);

/**
 * StickyScroll
 * written by Rick Harris - @iamrickharris
 *
 * Requires jQuery 1.4+
 *
 * Make elements stick to the top of your page as you scroll
 *
 * See README for details
 *
 */

(function ($) {
    $.fn.stickyScroll = function (options) {

        var methods = {

            init: function (options) {

                var settings;

                if (options.mode !== 'auto' && options.mode !== 'manual') {
                    if (options.container) {
                        options.mode = 'auto';
                    }
                    if (options.bottomBoundary) {
                        options.mode = 'manual';
                    }
                }

                settings = $.extend({
                    mode: 'auto', // 'auto' or 'manual'
                    container: $('body'),
                    topBoundary: null,
                    bottomBoundary: null
                }, options);

                function bottomBoundary() {
                    return $(document).height() - settings.container.offset().top - settings.container.attr('offsetHeight');
                }

                function topBoundary() {
                    return settings.container.offset().top
                }

                function elHeight(el) {
                    return $(el).attr('offsetHeight');
                }

                // make sure user input is a jQuery object
                settings.container = $(settings.container);
                if (!settings.container.length) {
                    if (console) {
                        console.log('StickyScroll: the element ' + options.container +
                            ' does not exist, we\'re throwing in the towel');
                    }
                    return;
                }

                // calculate automatic bottomBoundary
                if (settings.mode === 'auto') {
                    settings.topBoundary = topBoundary();
                    settings.bottomBoundary = bottomBoundary();
                }

                return this.each(function (index) {

                    var el = $(this),
                        win = $(window),
                        id = Date.now() + index,
                        height = elHeight(el);

                    el.data('sticky-id', id);

                    win.bind('scroll.stickyscroll-' + id, function () {
                        var top = $(document).scrollTop(),
                            bottom = $(document).height() - top - height;

                        if (bottom <= settings.bottomBoundary) {
                            el.offset({
                                top: $(document).height() - settings.bottomBoundary - height
                            })
                                .removeClass('sticky-active')
                                .removeClass('sticky-inactive')
                                .addClass('sticky-stopped');
                        } else if (top > settings.topBoundary) {
                            el.offset({
                                top: $(window).scrollTop()
                            })
                                .removeClass('sticky-stopped')
                                .removeClass('sticky-inactive')
                                .addClass('sticky-active');
                        } else if (top < settings.topBoundary) {
                            el.css({
                                position: '',
                                top: '',
                                bottom: ''
                            })
                                .removeClass('sticky-stopped')
                                .removeClass('sticky-active')
                                .addClass('sticky-inactive');
                        }
                    });

                    win.bind('resize.stickyscroll-' + id, function () {
                        if (settings.mode === 'auto') {
                            settings.topBoundary = topBoundary();
                            settings.bottomBoundary = bottomBoundary();
                        }
                        height = elHeight(el);
                        $(this).scroll();
                    })

                    el.addClass('sticky-processed');

                    // start it off
                    win.scroll();

                });

            },

            reset: function () {
                return this.each(function () {
                    var el = $(this),
                        id = el.data('sticky-id');

                    el.css({
                        position: '',
                        top: '',
                        bottom: ''
                    })
                        .removeClass('sticky-stopped')
                        .removeClass('sticky-active')
                        .removeClass('sticky-inactive')
                        .removeClass('sticky-processed');

                    $(window).unbind('.stickyscroll-' + id);
                });
            }

        };

        // if options is a valid method, execute it
        if (methods[options]) {
            return methods[options].apply(this,
                Array.prototype.slice.call(arguments, 1));
        }
        // or, if options is a config object, or no options are passed, init
        else if (typeof options === 'object' || !options) {
            return methods.init.apply(this, arguments);
        } else if (console) {
            console.log('Method' + options +
                ' does not exist on jQuery.stickyScroll');
        }

    };
})(jQuery);

/*! jQuery Validation Plugin - v1.11.1 - 3/22/2013\n* https://github.com/jzaefferer/jquery-validation
 * Copyright (c) 2013 Jörn Zaefferer; Licensed MIT */
(function (t) {
    t.extend(t.fn, {
        validate: function (e) {
            if (!this.length) return e && e.debug && window.console && console.warn("Nothing selected, can't validate, returning nothing."), void 0;
            var i = t.data(this[0], "validator");
            return i ? i : (this.attr("novalidate", "novalidate"), i = new t.validator(e, this[0]), t.data(this[0], "validator", i), i.settings.onsubmit && (this.validateDelegate(":submit", "click", function (e) {
                i.settings.submitHandler && (i.submitButton = e.target), t(e.target).hasClass("cancel") && (i.cancelSubmit = !0), void 0 !== t(e.target).attr("formnovalidate") && (i.cancelSubmit = !0)
            }), this.submit(function (e) {
                function s() {
                    var s;
                    return i.settings.submitHandler ? (i.submitButton && (s = t("<input type='hidden'/>").attr("name", i.submitButton.name).val(t(i.submitButton).val()).appendTo(i.currentForm)), i.settings.submitHandler.call(i, i.currentForm, e), i.submitButton && s.remove(), !1) : !0
                }
                return i.settings.debug && e.preventDefault(), i.cancelSubmit ? (i.cancelSubmit = !1, s()) : i.form() ? i.pendingRequest ? (i.formSubmitted = !0, !1) : s() : (i.focusInvalid(), !1)
            })), i)
        },
        valid: function () {
            if (t(this[0]).is("form")) return this.validate().form();
            var e = !0,
                i = t(this[0].form).validate();
            return this.each(function () {
                e = e && i.element(this)
            }), e
        },
        removeAttrs: function (e) {
            var i = {}, s = this;
            return t.each(e.split(/\s/), function (t, e) {
                i[e] = s.attr(e), s.removeAttr(e)
            }), i
        },
        rules: function (e, i) {
            var s = this[0];
            if (e) {
                var r = t.data(s.form, "validator").settings,
                    n = r.rules,
                    a = t.validator.staticRules(s);
                switch (e) {
                case "add":
                    t.extend(a, t.validator.normalizeRule(i)), delete a.messages, n[s.name] = a, i.messages && (r.messages[s.name] = t.extend(r.messages[s.name], i.messages));
                    break;
                case "remove":
                    if (!i) return delete n[s.name], a;
                    var u = {};
                    return t.each(i.split(/\s/), function (t, e) {
                        u[e] = a[e], delete a[e]
                    }), u
                }
            }
            var o = t.validator.normalizeRules(t.extend({}, t.validator.classRules(s), t.validator.attributeRules(s), t.validator.dataRules(s), t.validator.staticRules(s)), s);
            if (o.required) {
                var l = o.required;
                delete o.required, o = t.extend({
                    required: l
                }, o)
            }
            return o
        }
    }), t.extend(t.expr[":"], {
        blank: function (e) {
            return !t.trim("" + t(e).val())
        },
        filled: function (e) {
            return !!t.trim("" + t(e).val())
        },
        unchecked: function (e) {
            return !t(e).prop("checked")
        }
    }), t.validator = function (e, i) {
        this.settings = t.extend(!0, {}, t.validator.defaults, e), this.currentForm = i, this.init()
    }, t.validator.format = function (e, i) {
        return 1 === arguments.length ? function () {
            var i = t.makeArray(arguments);
            return i.unshift(e), t.validator.format.apply(this, i)
        } : (arguments.length > 2 && i.constructor !== Array && (i = t.makeArray(arguments).slice(1)), i.constructor !== Array && (i = [i]), t.each(i, function (t, i) {
            e = e.replace(RegExp("\\{" + t + "\\}", "g"), function () {
                return i
            })
        }), e)
    }, t.extend(t.validator, {
        defaults: {
            messages: {},
            groups: {},
            rules: {},
            errorClass: "error",
            validClass: "valid",
            errorElement: "label",
            focusInvalid: !0,
            errorContainer: t([]),
            errorLabelContainer: t([]),
            onsubmit: !0,
            ignore: ":hidden",
            ignoreTitle: !1,
            onfocusin: function (t) {
                this.lastActive = t, this.settings.focusCleanup && !this.blockFocusCleanup && (this.settings.unhighlight && this.settings.unhighlight.call(this, t, this.settings.errorClass, this.settings.validClass), this.addWrapper(this.errorsFor(t)).hide())
            },
            onfocusout: function (t) {
                this.checkable(t) || !(t.name in this.submitted) && this.optional(t) || this.element(t)
            },
            onkeyup: function (t, e) {
                (9 !== e.which || "" !== this.elementValue(t)) && (t.name in this.submitted || t === this.lastElement) && this.element(t)
            },
            onclick: function (t) {
                t.name in this.submitted ? this.element(t) : t.parentNode.name in this.submitted && this.element(t.parentNode)
            },
            highlight: function (e, i, s) {
                "radio" === e.type ? this.findByName(e.name).addClass(i).removeClass(s) : t(e).addClass(i).removeClass(s)
            },
            unhighlight: function (e, i, s) {
                "radio" === e.type ? this.findByName(e.name).removeClass(i).addClass(s) : t(e).removeClass(i).addClass(s)
            }
        },
        setDefaults: function (e) {
            t.extend(t.validator.defaults, e)
        },
        messages: {
            required: "This field is required.",
            remote: "Please fix this field.",
            email: "Please enter a valid email address.",
            url: "Please enter a valid URL.",
            date: "Please enter a valid date.",
            dateISO: "Please enter a valid date (ISO).",
            number: "Please enter a valid number.",
            digits: "Please enter only digits.",
            creditcard: "Please enter a valid credit card number.",
            equalTo: "Please enter the same value again.",
            maxlength: t.validator.format("Please enter no more than {0} characters."),
            minlength: t.validator.format("Please enter at least {0} characters."),
            rangelength: t.validator.format("Please enter a value between {0} and {1} characters long."),
            range: t.validator.format("Please enter a value between {0} and {1}."),
            max: t.validator.format("Please enter a value less than or equal to {0}."),
            min: t.validator.format("Please enter a value greater than or equal to {0}.")
        },
        autoCreateRanges: !1,
        prototype: {
            init: function () {
                function e(e) {
                    var i = t.data(this[0].form, "validator"),
                        s = "on" + e.type.replace(/^validate/, "");
                    i.settings[s] && i.settings[s].call(i, this[0], e)
                }
                this.labelContainer = t(this.settings.errorLabelContainer), this.errorContext = this.labelContainer.length && this.labelContainer || t(this.currentForm), this.containers = t(this.settings.errorContainer).add(this.settings.errorLabelContainer), this.submitted = {}, this.valueCache = {}, this.pendingRequest = 0, this.pending = {}, this.invalid = {}, this.reset();
                var i = this.groups = {};
                t.each(this.settings.groups, function (e, s) {
                    "string" == typeof s && (s = s.split(/\s/)), t.each(s, function (t, s) {
                        i[s] = e
                    })
                });
                var s = this.settings.rules;
                t.each(s, function (e, i) {
                    s[e] = t.validator.normalizeRule(i)
                }), t(this.currentForm).validateDelegate(":text, [type='password'], [type='file'], select, textarea, [type='number'], [type='search'] ,[type='tel'], [type='url'], [type='email'], [type='datetime'], [type='date'], [type='month'], [type='week'], [type='time'], [type='datetime-local'], [type='range'], [type='color'] ", "focusin focusout keyup", e).validateDelegate("[type='radio'], [type='checkbox'], select, option", "click", e), this.settings.invalidHandler && t(this.currentForm).bind("invalid-form.validate", this.settings.invalidHandler)
            },
            form: function () {
                return this.checkForm(), t.extend(this.submitted, this.errorMap), this.invalid = t.extend({}, this.errorMap), this.valid() || t(this.currentForm).triggerHandler("invalid-form", [this]), this.showErrors(), this.valid()
            },
            checkForm: function () {
                this.prepareForm();
                for (var t = 0, e = this.currentElements = this.elements(); e[t]; t++) this.check(e[t]);
                return this.valid()
            },
            element: function (e) {
                e = this.validationTargetFor(this.clean(e)), this.lastElement = e, this.prepareElement(e), this.currentElements = t(e);
                var i = this.check(e) !== !1;
                return i ? delete this.invalid[e.name] : this.invalid[e.name] = !0, this.numberOfInvalids() || (this.toHide = this.toHide.add(this.containers)), this.showErrors(), i
            },
            showErrors: function (e) {
                if (e) {
                    t.extend(this.errorMap, e), this.errorList = [];
                    for (var i in e) this.errorList.push({
                        message: e[i],
                        element: this.findByName(i)[0]
                    });
                    this.successList = t.grep(this.successList, function (t) {
                        return !(t.name in e)
                    })
                }
                this.settings.showErrors ? this.settings.showErrors.call(this, this.errorMap, this.errorList) : this.defaultShowErrors()
            },
            resetForm: function () {
                t.fn.resetForm && t(this.currentForm).resetForm(), this.submitted = {}, this.lastElement = null, this.prepareForm(), this.hideErrors(), this.elements().removeClass(this.settings.errorClass).removeData("previousValue")
            },
            numberOfInvalids: function () {
                return this.objectLength(this.invalid)
            },
            objectLength: function (t) {
                var e = 0;
                for (var i in t) e++;
                return e
            },
            hideErrors: function () {
                this.addWrapper(this.toHide).hide()
            },
            valid: function () {
                return 0 === this.size()
            },
            size: function () {
                return this.errorList.length
            },
            focusInvalid: function () {
                if (this.settings.focusInvalid) try {
                    t(this.findLastActive() || this.errorList.length && this.errorList[0].element || []).filter(":visible").focus().trigger("focusin")
                } catch (e) {}
            },
            findLastActive: function () {
                var e = this.lastActive;
                return e && 1 === t.grep(this.errorList, function (t) {
                    return t.element.name === e.name
                }).length && e
            },
            elements: function () {
                var e = this,
                    i = {};
                return t(this.currentForm).find("input, select, textarea").not(":submit, :reset, :image, [disabled]").not(this.settings.ignore).filter(function () {
                    return !this.name && e.settings.debug && window.console && console.error("%o has no name assigned", this), this.name in i || !e.objectLength(t(this).rules()) ? !1 : (i[this.name] = !0, !0)
                })
            },
            clean: function (e) {
                return t(e)[0]
            },
            errors: function () {
                var e = this.settings.errorClass.replace(" ", ".");
                return t(this.settings.errorElement + "." + e, this.errorContext)
            },
            reset: function () {
                this.successList = [], this.errorList = [], this.errorMap = {}, this.toShow = t([]), this.toHide = t([]), this.currentElements = t([])
            },
            prepareForm: function () {
                this.reset(), this.toHide = this.errors().add(this.containers)
            },
            prepareElement: function (t) {
                this.reset(), this.toHide = this.errorsFor(t)
            },
            elementValue: function (e) {
                var i = t(e).attr("type"),
                    s = t(e).val();
                return "radio" === i || "checkbox" === i ? t("input[name='" + t(e).attr("name") + "']:checked").val() : "string" == typeof s ? s.replace(/\r/g, "") : s
            },
            check: function (e) {
                e = this.validationTargetFor(this.clean(e));
                var i, s = t(e).rules(),
                    r = !1,
                    n = this.elementValue(e);
                for (var a in s) {
                    var u = {
                        method: a,
                        parameters: s[a]
                    };
                    try {
                        if (i = t.validator.methods[a].call(this, n, e, u.parameters), "dependency-mismatch" === i) {
                            r = !0;
                            continue
                        }
                        if (r = !1, "pending" === i) return this.toHide = this.toHide.not(this.errorsFor(e)), void 0;
                        if (!i) return this.formatAndAdd(e, u), !1
                    } catch (o) {
                        throw this.settings.debug && window.console && console.log("Exception occurred when checking element " + e.id + ", check the '" + u.method + "' method.", o), o
                    }
                }
                return r ? void 0 : (this.objectLength(s) && this.successList.push(e), !0)
            },
            customDataMessage: function (e, i) {
                return t(e).data("msg-" + i.toLowerCase()) || e.attributes && t(e).attr("data-msg-" + i.toLowerCase())
            },
            customMessage: function (t, e) {
                var i = this.settings.messages[t];
                return i && (i.constructor === String ? i : i[e])
            },
            findDefined: function () {
                for (var t = 0; arguments.length > t; t++)
                    if (void 0 !== arguments[t]) return arguments[t];
                return void 0
            },
            defaultMessage: function (e, i) {
                return this.findDefined(this.customMessage(e.name, i), this.customDataMessage(e, i), !this.settings.ignoreTitle && e.title || void 0, t.validator.messages[i], "<strong>Warning: No message defined for " + e.name + "</strong>")
            },
            formatAndAdd: function (e, i) {
                var s = this.defaultMessage(e, i.method),
                    r = /\$?\{(\d+)\}/g;
                "function" == typeof s ? s = s.call(this, i.parameters, e) : r.test(s) && (s = t.validator.format(s.replace(r, "{$1}"), i.parameters)), this.errorList.push({
                    message: s,
                    element: e
                }), this.errorMap[e.name] = s, this.submitted[e.name] = s
            },
            addWrapper: function (t) {
                return this.settings.wrapper && (t = t.add(t.parent(this.settings.wrapper))), t
            },
            defaultShowErrors: function () {
                var t, e;
                for (t = 0; this.errorList[t]; t++) {
                    var i = this.errorList[t];
                    this.settings.highlight && this.settings.highlight.call(this, i.element, this.settings.errorClass, this.settings.validClass), this.showLabel(i.element, i.message)
                }
                if (this.errorList.length && (this.toShow = this.toShow.add(this.containers)), this.settings.success)
                    for (t = 0; this.successList[t]; t++) this.showLabel(this.successList[t]);
                if (this.settings.unhighlight)
                    for (t = 0, e = this.validElements(); e[t]; t++) this.settings.unhighlight.call(this, e[t], this.settings.errorClass, this.settings.validClass);
                this.toHide = this.toHide.not(this.toShow), this.hideErrors(), this.addWrapper(this.toShow).show()
            },
            validElements: function () {
                return this.currentElements.not(this.invalidElements())
            },
            invalidElements: function () {
                return t(this.errorList).map(function () {
                    return this.element
                })
            },
            showLabel: function (e, i) {
                var s = this.errorsFor(e);
                s.length ? (s.removeClass(this.settings.validClass).addClass(this.settings.errorClass), s.html(i)) : (s = t("<" + this.settings.errorElement + ">").attr("for", this.idOrName(e)).addClass(this.settings.errorClass).html(i || ""), this.settings.wrapper && (s = s.hide().show().wrap("<" + this.settings.wrapper + "/>").parent()), this.labelContainer.append(s).length || (this.settings.errorPlacement ? this.settings.errorPlacement(s, t(e)) : s.insertAfter(e))), !i && this.settings.success && (s.text(""), "string" == typeof this.settings.success ? s.addClass(this.settings.success) : this.settings.success(s, e)), this.toShow = this.toShow.add(s)
            },
            errorsFor: function (e) {
                var i = this.idOrName(e);
                return this.errors().filter(function () {
                    return t(this).attr("for") === i
                })
            },
            idOrName: function (t) {
                return this.groups[t.name] || (this.checkable(t) ? t.name : t.id || t.name)
            },
            validationTargetFor: function (t) {
                return this.checkable(t) && (t = this.findByName(t.name).not(this.settings.ignore)[0]), t
            },
            checkable: function (t) {
                return /radio|checkbox/i.test(t.type)
            },
            findByName: function (e) {
                return t(this.currentForm).find("[name='" + e + "']")
            },
            getLength: function (e, i) {
                switch (i.nodeName.toLowerCase()) {
                case "select":
                    return t("option:selected", i).length;
                case "input":
                    if (this.checkable(i)) return this.findByName(i.name).filter(":checked").length
                }
                return e.length
            },
            depend: function (t, e) {
                return this.dependTypes[typeof t] ? this.dependTypes[typeof t](t, e) : !0
            },
            dependTypes: {
                "boolean": function (t) {
                    return t
                },
                string: function (e, i) {
                    return !!t(e, i.form).length
                },
                "function": function (t, e) {
                    return t(e)
                }
            },
            optional: function (e) {
                var i = this.elementValue(e);
                return !t.validator.methods.required.call(this, i, e) && "dependency-mismatch"
            },
            startRequest: function (t) {
                this.pending[t.name] || (this.pendingRequest++, this.pending[t.name] = !0)
            },
            stopRequest: function (e, i) {
                this.pendingRequest--, 0 > this.pendingRequest && (this.pendingRequest = 0), delete this.pending[e.name], i && 0 === this.pendingRequest && this.formSubmitted && this.form() ? (t(this.currentForm).submit(), this.formSubmitted = !1) : !i && 0 === this.pendingRequest && this.formSubmitted && (t(this.currentForm).triggerHandler("invalid-form", [this]), this.formSubmitted = !1)
            },
            previousValue: function (e) {
                return t.data(e, "previousValue") || t.data(e, "previousValue", {
                    old: null,
                    valid: !0,
                    message: this.defaultMessage(e, "remote")
                })
            }
        },
        classRuleSettings: {
            required: {
                required: !0
            },
            email: {
                email: !0
            },
            url: {
                url: !0
            },
            date: {
                date: !0
            },
            dateISO: {
                dateISO: !0
            },
            number: {
                number: !0
            },
            digits: {
                digits: !0
            },
            creditcard: {
                creditcard: !0
            }
        },
        addClassRules: function (e, i) {
            e.constructor === String ? this.classRuleSettings[e] = i : t.extend(this.classRuleSettings, e)
        },
        classRules: function (e) {
            var i = {}, s = t(e).attr("class");
            return s && t.each(s.split(" "), function () {
                this in t.validator.classRuleSettings && t.extend(i, t.validator.classRuleSettings[this])
            }), i
        },
        attributeRules: function (e) {
            var i = {}, s = t(e),
                r = s[0].getAttribute("type");
            for (var n in t.validator.methods) {
                var a;
                "required" === n ? (a = s.get(0).getAttribute(n), "" === a && (a = !0), a = !! a) : a = s.attr(n), /min|max/.test(n) && (null === r || /number|range|text/.test(r)) && (a = Number(a)), a ? i[n] = a : r === n && "range" !== r && (i[n] = !0)
            }
            return i.maxlength && /-1|2147483647|524288/.test(i.maxlength) && delete i.maxlength, i
        },
        dataRules: function (e) {
            var i, s, r = {}, n = t(e);
            for (i in t.validator.methods) s = n.data("rule-" + i.toLowerCase()), void 0 !== s && (r[i] = s);
            return r
        },
        staticRules: function (e) {
            var i = {}, s = t.data(e.form, "validator");
            return s.settings.rules && (i = t.validator.normalizeRule(s.settings.rules[e.name]) || {}), i
        },
        normalizeRules: function (e, i) {
            return t.each(e, function (s, r) {
                if (r === !1) return delete e[s], void 0;
                if (r.param || r.depends) {
                    var n = !0;
                    switch (typeof r.depends) {
                    case "string":
                        n = !! t(r.depends, i.form).length;
                        break;
                    case "function":
                        n = r.depends.call(i, i)
                    }
                    n ? e[s] = void 0 !== r.param ? r.param : !0 : delete e[s]
                }
            }), t.each(e, function (s, r) {
                e[s] = t.isFunction(r) ? r(i) : r
            }), t.each(["minlength", "maxlength"], function () {
                e[this] && (e[this] = Number(e[this]))
            }), t.each(["rangelength", "range"], function () {
                var i;
                e[this] && (t.isArray(e[this]) ? e[this] = [Number(e[this][0]), Number(e[this][1])] : "string" == typeof e[this] && (i = e[this].split(/[\s,]+/), e[this] = [Number(i[0]), Number(i[1])]))
            }), t.validator.autoCreateRanges && (e.min && e.max && (e.range = [e.min, e.max], delete e.min, delete e.max), e.minlength && e.maxlength && (e.rangelength = [e.minlength, e.maxlength], delete e.minlength, delete e.maxlength)), e
        },
        normalizeRule: function (e) {
            if ("string" == typeof e) {
                var i = {};
                t.each(e.split(/\s/), function () {
                    i[this] = !0
                }), e = i
            }
            return e
        },
        addMethod: function (e, i, s) {
            t.validator.methods[e] = i, t.validator.messages[e] = void 0 !== s ? s : t.validator.messages[e], 3 > i.length && t.validator.addClassRules(e, t.validator.normalizeRule(e))
        },
        methods: {
            required: function (e, i, s) {
                if (!this.depend(s, i)) return "dependency-mismatch";
                if ("select" === i.nodeName.toLowerCase()) {
                    var r = t(i).val();
                    return r && r.length > 0
                }
                return this.checkable(i) ? this.getLength(e, i) > 0 : t.trim(e).length > 0
            },
            email: function (t, e) {
                return this.optional(e) || /^((([a-z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+(\.([a-z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+)*)|((\x22)((((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(([\x01-\x08\x0b\x0c\x0e-\x1f\x7f]|\x21|[\x23-\x5b]|[\x5d-\x7e]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(\\([\x01-\x09\x0b\x0c\x0d-\x7f]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]))))*(((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(\x22)))@((([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.)+(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))$/i.test(t)
            },
            url: function (t, e) {
                return this.optional(e) || /^(https?|s?ftp):\/\/(((([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:)*@)?(((\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5])\.(\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5])\.(\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5])\.(\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5]))|((([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.)+(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.?)(:\d*)?)(\/((([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:|@)+(\/(([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:|@)*)*)?)?(\?((([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:|@)|[\uE000-\uF8FF]|\/|\?)*)?(#((([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:|@)|\/|\?)*)?$/i.test(t)
            },
            date: function (t, e) {
                return this.optional(e) || !/Invalid|NaN/.test("" + new Date(t))
            },
            dateISO: function (t, e) {
                return this.optional(e) || /^\d{4}[\/\-]\d{1,2}[\/\-]\d{1,2}$/.test(t)
            },
            number: function (t, e) {
                return this.optional(e) || /^-?(?:\d+|\d{1,3}(?:,\d{3})+)?(?:\.\d+)?$/.test(t)
            },
            digits: function (t, e) {
                return this.optional(e) || /^\d+$/.test(t)
            },
            creditcard: function (t, e) {
                if (this.optional(e)) return "dependency-mismatch";
                if (/[^0-9 \-]+/.test(t)) return !1;
                var i = 0,
                    s = 0,
                    r = !1;
                t = t.replace(/\D/g, "");
                for (var n = t.length - 1; n >= 0; n--) {
                    var a = t.charAt(n);
                    s = parseInt(a, 10), r && (s *= 2) > 9 && (s -= 9), i += s, r = !r
                }
                return 0 === i % 10
            },
            minlength: function (e, i, s) {
                var r = t.isArray(e) ? e.length : this.getLength(t.trim(e), i);
                return this.optional(i) || r >= s
            },
            maxlength: function (e, i, s) {
                var r = t.isArray(e) ? e.length : this.getLength(t.trim(e), i);
                return this.optional(i) || s >= r
            },
            rangelength: function (e, i, s) {
                var r = t.isArray(e) ? e.length : this.getLength(t.trim(e), i);
                return this.optional(i) || r >= s[0] && s[1] >= r
            },
            min: function (t, e, i) {
                return this.optional(e) || t >= i
            },
            max: function (t, e, i) {
                return this.optional(e) || i >= t
            },
            range: function (t, e, i) {
                return this.optional(e) || t >= i[0] && i[1] >= t
            },
            equalTo: function (e, i, s) {
                var r = t(s);
                return this.settings.onfocusout && r.unbind(".validate-equalTo").bind("blur.validate-equalTo", function () {
                    t(i).valid()
                }), e === r.val()
            },
            remote: function (e, i, s) {
                if (this.optional(i)) return "dependency-mismatch";
                var r = this.previousValue(i);
                if (this.settings.messages[i.name] || (this.settings.messages[i.name] = {}), r.originalMessage = this.settings.messages[i.name].remote, this.settings.messages[i.name].remote = r.message, s = "string" == typeof s && {
                    url: s
                } || s, r.old === e) return r.valid;
                r.old = e;
                var n = this;
                this.startRequest(i);
                var a = {};
                return a[i.name] = e, t.ajax(t.extend(!0, {
                    url: s,
                    mode: "abort",
                    port: "validate" + i.name,
                    dataType: "json",
                    data: a,
                    success: function (s) {
                        n.settings.messages[i.name].remote = r.originalMessage;
                        var a = s === !0 || "true" === s;
                        if (a) {
                            var u = n.formSubmitted;
                            n.prepareElement(i), n.formSubmitted = u, n.successList.push(i), delete n.invalid[i.name], n.showErrors()
                        } else {
                            var o = {}, l = s || n.defaultMessage(i, "remote");
                            o[i.name] = r.message = t.isFunction(l) ? l(e) : l, n.invalid[i.name] = !0, n.showErrors(o)
                        }
                        r.valid = a, n.stopRequest(i, a)
                    }
                }, s)), "pending"
            }
        }
    }), t.format = t.validator.format
})(jQuery),
function (t) {
    var e = {};
    if (t.ajaxPrefilter) t.ajaxPrefilter(function (t, i, s) {
        var r = t.port;
        "abort" === t.mode && (e[r] && e[r].abort(), e[r] = s)
    });
    else {
        var i = t.ajax;
        t.ajax = function (s) {
            var r = ("mode" in s ? s : t.ajaxSettings).mode,
                n = ("port" in s ? s : t.ajaxSettings).port;
            return "abort" === r ? (e[n] && e[n].abort(), e[n] = i.apply(this, arguments), e[n]) : i.apply(this, arguments)
        }
    }
}(jQuery),
function (t) {
    t.extend(t.fn, {
        validateDelegate: function (e, i, s) {
            return this.bind(i, function (i) {
                var r = t(i.target);
                return r.is(e) ? s.apply(r, arguments) : void 0
            })
        }
    })
}(jQuery);