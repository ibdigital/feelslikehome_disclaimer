$(document).ready(function() {

    $('.modal').hide();

    /**
     * -----     MAIN NAVIGATION
     */
    // Initialize Submenu Carousel
    if($('#sub-navigation-planing-carousel-must li').length) {
        $('#sub-navigation-planing-carousel-must').jcarousel();
    }

    if($('#sub-navigation-moving-carousel-must li').length) {
        $('#sub-navigation-moving-carousel-must').jcarousel();
    }

    if($('#sub-navigation-build-up-carousel-must li').length) {
        $('#sub-navigation-build-up-carousel-must').jcarousel();
    }

    if($('#sub-navigation-living-carousel-must li').length) {
        $('#sub-navigation-living-carousel-must').jcarousel();
    }

    if($('#sub-navigation-planing-carousel-can li').length) {
        $('#sub-navigation-planing-carousel-can').jcarousel();
    }

    if($('#sub-navigation-moving-carousel-can li').length) {
        $('#sub-navigation-moving-carousel-can').jcarousel();
    }

    if($('#sub-navigation-build-up-carousel-can li').length) {
        $('#sub-navigation-build-up-carousel-can').jcarousel();
    }

    if($('#sub-navigation-living-carousel-can li').length) {
        $('#sub-navigation-living-carousel-can').jcarousel();
    }

    // Hide
    $('#sub-navigation-planing-carousel-can-wrapper').hide();
    $('#sub-navigation-moving-carousel-can-wrapper').hide();
    $('#sub-navigation-build-up-carousel-can-wrapper').hide();
    $('#sub-navigation-living-carousel-can-wrapper').hide();
    $('#sub-navigation-planing .can span').hide();
    $('#sub-navigation-moving .can span').hide();
    $('#sub-navigation-build-up .can span').hide();
    $('#sub-navigation-living .can span').hide();
    // Hide Submenu


    $('#sub-navigation-wrapper').hide();
    $('.sub-navigation').hide();

    // Initialize Navigation Functionality

    $('#top-navigation li').bind('click mouseover', function () {
        // Set all top navigation items inactive
        $('#top-navigation li').removeClass('active');
        // Hide all sub navigations
        $('.sub-navigation').hide();
        // Set current top navigation item active
        $(this).addClass('active');
        // show current sub navigation
        //Prevent the sub-navigation to appear if the selected section is JOBS...
        if($(this).attr('id') != 'jobs'){
            $('#sub-navigation-'+$(this).attr('id')).show();
            $('#sub-navigation-wrapper').fadeIn(100);
        }else{
            $('#sub-navigation-wrapper').hide();
        }
    });


    $('#content-wrapper').bind('mouseenter click focus', function() {
        $('#top-navigation li').removeClass('active');
        $('#sub-navigation-wrapper').fadeOut(100);
    });

    $('#caressma-wrapper').bind('mouseenter click focus', function() {
        $('#top-navigation li').removeClass('active');
        $('#sub-navigation-wrapper').fadeOut(100);
    });

    $('#caressma-wrapper2').bind('mouseenter click focus', function() {
        $('#top-navigation li').removeClass('active');
        $('#sub-navigation-wrapper').fadeOut(100);
    });

    /*
    $('#header').bind('mouseleave', function() {
        $('#top-navigation li').removeClass('active');
        $('#sub-navigation-wrapper').fadeOut(100);
    });
    */


    $('.must').bind('click', function() {
        $(this).parent().find('.can').removeClass('active').find('span').hide();
        $(this).addClass('active').find('span').show();

        $('#'+$(this).parent().parent().attr('id')+'-carousel-can-wrapper').hide();
        $('#'+$(this).parent().parent().attr('id')+'-carousel-must-wrapper').show();

    });

    $('.can').bind('click', function() {
        $(this).parent().find('.must').removeClass('active').find('span').hide();
        $(this).addClass('active').find('span').show();

        $('#'+$(this).parent().parent().attr('id')+'-carousel-must-wrapper').hide();
        $('#'+$(this).parent().parent().attr('id')+'-carousel-can-wrapper').show();
    });

    /**
     * -----     END MAIN NAVIGATION
     */

    /*
     * ------ Initialize Radio Buttons -------
     */
    $('input[type=checkbox], input[type=radio]').customRadioCheck();



    /*
     * ------- Question Form Submit
     */

    $('.next-question').bind('click', function() {
       $('#surveyQuestion').submit();
    });

    /*
     * ------ Length of Stay submit -------
     */
    $('.target').change(function() {
        $(this).parent().submit();
    });
	
    $('#UserPassword:input', '#Login').keydown(function (event) {
    	var keypressed = event.keyCode || event.which;
    	if (keypressed == 13) {
        	$('#UserLoginForm').submit();
    	}
    });

    $('#loginButton').click(function() {
        $('#UserLoginForm').submit();
    });

    $('#contactButton').click(function() {
        $('#ContactForm').submit();
    });

    // Add This Plugin

    /*
    $('#addthis').scrollToFixed({
        marginTop: $('#header').outerHeight(),
        limit: function() {
            var limit = $('#footer-wrapper').offset().top  - 350;
            return limit;

        }

    });
    */

    if ($('#addthis').length) { // make sure "#sticky" element exists
        var el = $('#addthis');
        var stickyTop = 150; // returns number
        var stickyHeight = 300;

        $(window).scroll(function(){ // scroll event
            var limit = $('#footer-wrapper').offset().top - stickyHeight - 20;

            var windowTop = $(window).scrollTop(); // returns number

            if (stickyTop < windowTop){
                el.css({ position: 'fixed', top: 10 });
            }
            else {
                el.css('position','static');
            }

            if (limit < windowTop) {
                var diff = limit - windowTop;
                el.css({top: diff});
            }
        });
    }

    // Delete FlashMessages
    setTimeout(function(){
        $('.alert').fadeOut();
    },3000);

    $(".careesma-categories").bind( "click", function(){
        $(".careesma-categories").removeClass("careesma-category");
        $(".careesma-categories").addClass("careesma-category2");        
        $(this).addClass("careesma-category");
        $(this).removeClass("careesma-category2");
        $(".cat").hide();
        $("."+$(this).attr('id')).show();
    });

    $("#caressma-search2").click(function(){
        var form = $("#my-custom-form");
        var result = CJSsearch.submit(form, 'search-results');
        //console.log(result.loadResults);
    });

});

var CJSsearch = function() {
    var resutls = '';
    var form = '';
    var searchURL = 'http://www.careesma.at/jobs/jobsuche?jsapi=true&vj4=1';
    var defaultCssLoaded = false;

    var getFormUrl = function(profileIDs) {
      var formURL = 'http://www.careesma.at/js-search/html-form';

      if(typeof(profileIDs) == 'undefined') {
         profileIDs = {};
      }

      params = 'vj4=1&';
      for (var i = 0; i < profileIDs.length; i++) {
         params += 'ProfileIDs[]=' + profileIDs[i];
         if ((i+1) < profileIDs.length) {
            params += '&';
         }
      }

      var url = formURL;
      if(params != '') {
          url += '?' + params;
      }
      return url;
    }

    var paginate = function(link, resultsID) {
        loadResults($(link).attr('real_href'), resultsID);
    }

    var loadResults = function (url, resultsID) {
        var addPaginationLink = function() {
            $('#'+resultsID).html(CJSsearch.results);

            $('.pagination a', $('#'+resultsID)).each(function () {
                $(this).attr('real_href', $(this).attr('href'));
                $(this).attr('href', '#');
                $(this).click(function () {
                    paginate(this, resultsID);
                    return false;
                 });
            });
        }

        $.getScript(url, addPaginationLink);
    }

    //return the public properties and methods
    return {
        submit : function(form, resultsID) {
            loadResults(searchURL + '&' + $(form).serialize(), resultsID);
            return false;
        },

        // Options takes two parameters:
        //    resultsID: loads results in the specidfied dom ID
        //    onload: function called after loading the form
        //
        addForm : function(formID, options) {

            if(typeof(options) == 'undefined') {
               options = {};
            }

            if('resultsID' in options) {
                var ajaxify = function () {
                    $('#'+formID).html(CJSsearch.form)
                    var form = $('form', $('#' + formID));
                    form.submit(function(){
                        return CJSsearch.submit(this, options['resultsID']);
                    });
                    if('onload' in options) {
                        options['onload']();
                    }
                }
            } else {
                var ajaxify = function () {
                    $('#'+formID).html(CJSsearch.form)
                    if('onload' in options) {
                        options['onload']();
                    }
                };
            }

            if('profileIDs' in options) {
               var profileIDs = options['profileIDs'];
            } else {
               var profileIDs = {}
            }

            $.getScript(getFormUrl(profileIDs), ajaxify);
        }
    }
}();