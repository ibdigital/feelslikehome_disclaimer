<?php
    $lengthOfStayForm = '<form method="POST" action="'.PAGEURL.'users/setLengthOfStay">
                            <div class="text-small">Length of Stay </div>
                            <select name="lengthOfStay" class="target">
                                <option value="1">less than 3 months</option>
                                <option value="3">3 to 6 months</option>
                                <option value="6">6 to 18 months</option>
                                <option value="18">1.5 to 2 years</option>
                                <option value="24">more than 2 years</option>
                            </select>
                        </form>';
    if(isset($authUser['trip_length'])){
        $lengthOfStayForm = str_replace('<option value="'.$authUser['trip_length'].'">', '<option value="'.$authUser['trip_length'].'" selected="selected">', $lengthOfStayForm) . "<br />\n";
    }

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
    <head>
        <meta charset="UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width">

        <meta name="google-site-verification" content="iI8IOaVG0VkPTrLKRDabLrIa_lhgeW85DNtwTYed06M" />

		<?php echo $this->Html->charset(); ?>
			<title>
				FLH StepByStep Guide
			</title>
		<?php
		
			echo $this->Html->meta('icon');
			
			// CSS Files to be included
			echo $this->Html->css('normalize.css');
			echo $this->Html->css('main.css');

			// JS Files to be included
			echo $this->Html->script('vendor/modernizr-2.6.2.min.js');
			
			echo $this->fetch('meta');
			echo $this->fetch('css');
			echo $this->fetch('script');
		
		?>

        <script type="text/javascript">var addthis_config = {"data_track_addressbar":true};</script>
        <script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-51640708085e034a"></script>
        <link href='http://fonts.googleapis.com/css?family=Source+Sans+Pro:400' rel='stylesheet' type='text/css'>

        <?php if($print) { ?>
            <style>
                #header {
                    display: none;
                }

                #addthis {
                    display: none;
                }

                .content-right-small {
                    display: none;
                }

                .comment_form {
                    display: none;
                }

                .online {
                    display: none;
                }

                .print {
                    display: block !important;
                }

                #footer-wrapper {
                    display: none;
                }

                .question-navigation {
                    display: none;
                }
                #content-wrapper {
                    margin-left: 10px;
                }
            </style>
        <?php } ?>
    </head>
    <body>
        <script type="text/javascript" src="/js/vendor/jquery-1.7.2.js"></script>     
        <!--<script src="//ajax.googleapis.com/ajax/libs/jquery/1.7.2/jquery.min.js"></script> -->
        <script>window.jQuery || document.write('<script src="js/vendor/jquery-1.4.2.min.js"><\/script>')</script>

        <?php echo $this->Html->script('plugins.js');?>
        <!--[if lt IE 7]>
            <p class="chromeframe">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> or <a href="http://www.google.com/chromeframe/?redirect=true">activate Google Chrome Frame</a> to improve your experience.</p>
        <![endif]-->

        <!-- Add your site or application content here -->
        <!-- FACEBOOK INTEGRATION -->
        <div id="fb-root"></div>
            <script src="//connect.facebook.net/en_US/all.js"></script> 
            <script>
              FB.init({
                    appId  : '169246196609701',
                    status : true, // check login status
                    cookie : true, // enable cookies to allow the server to access the session
                    xfbml  : true,  // parse XFBML
                    oauth : true
                });

            // Load the SDK Asynchronously
            (function(d){
               var js, id = 'facebook-jssdk', ref = d.getElementsByTagName('script')[0];
               if (d.getElementById(id)) {return;}
               js = d.createElement('script'); js.id = id; js.async = true;
               js.src = "//connect.facebook.net/fr_FR/all.js#xfbml=1";
               ref.parentNode.insertBefore(js, ref);
             }(document));
            </script>
        <!-- END FACEBOOK INTEGRATION -->

        <div id="container">
            <div id="header" class="clearfix">
                <div id="navigation-wrapper" class="clearfix">
                    <div id="top-navigation-wrapper">
                            <?php if(empty($authUser)): ?>
                                 <?if($facebool_login == false):?>
                        <div id="user-navigation" style="width:610px;">
                                     <a href="<?=$fb_login_url?>" style="float:left;margin-right:5px; margin-left: 0px;"><img src="/img/fb_login_button.png"/></a>
                                    <a class="button-blue-small" href="#Login2" data-toggle="modal" id="Login_button">Login</a>
                                    <a class="button-blue-small" href="#Registration" data-toggle="modal" id="Reg_button">Register</a>
                                <?endif;?>
                            <?php else: ?>
                        <div id="user-navigation" style="width:430px;">
                                <div class="dropdown" style="float:left;">
                                    <a class="button-blue-small dropdown-toggle" href="#" data-toggle="dropdown">
                                        <?php echo $authUser['firstname'] . ' ' . $authUser['surname'];?>
                                        <img src="/img/down-icon.png" style="vertical-align: middle; margin-left: 15px;width:13px;"/>
                                    </a>
                                    <ul class="dropdown-menu" role="menu" aria-labelledby="dLabel">
                                        <li><a href="/users/profile">My Profile</a></li>
                                        <li><a href="/pages/landing">My SbS Guide</a></li>
                                        <li><a href="/users/logout">Logout</a></li>
                                    </ul>
                                </div>
                            <?php endif; ?>
                            <a class="button-blue-small" href="#Contact" data-toggle="modal">Contact us</a>

<!--BACK UP OF DIV          <div style="vertical-align: middle;display: inline-block; margin-top: 3px;"> -->
                            <div style="margin-top: 3px;">
                                <a title="Facebook" data-toggle="tooltip" href="https://www.facebook.com/feelslikehome.at" target="_blank"><img src="/img/facebook-icon.jpg"></a>
                                <a title="Twitter" data-toggle="tooltip" href="https://twitter.com/FeelsLikeHomeAT" target="_blank"><img src="/img/twitter-icon.jpg"></a>
                               <!--  <a title="Pinterest" data-toggle="tooltip" href="http://pinterest.com/FeelLikeHome/" target="_blank"><img src="/img/pinterest.jpg"></a>
 -->                                <a title="Youtube" data-toggle="tooltip" href="http://www.youtube.com/user/feelslikehomevienna" target="_blank"><img src="/img/youtube-icon.jpg"></a>
                                <a title="Instagram" data-toggle="tooltip" href="http://instagram.com/feelslikehome" target="_blank"><img src="/img/insta-icon.jpg"></a>
                            </div>


                        </div>

                        <?php if(isset($authUser)):?>
                            <a id="logo" href="/pages/landing" alt="Step by Step Guide">
                        <?php else:?>
                            <a id="logo" href="/" alt="Step by Step Guide">
                        <?php endif;?>
                                <img src="/img/logo.png" alt="Step by Step Guide"/>
                            </a>
                        <ul id="top-navigation">
                            <li id="planing">
                                <a href="#">Planning<span>before arrival</span></a>
                            </li>
                            <li id="moving">
                                <a href="#">Moving<span>week 1</span></a>
                            </li>
                            <li id="build-up">
                                <a href="#">Build-Up<span>week 2 - 6</span></a>
                            </li>
                            <li id="living">
                                <a href="#">Living<span>week 7+</span></a>
                            </li>
                            <li id="jobs">
                                <a href="/partners/jobs">Jobs<span>build a career</span></a>
                            </li>                            
                        </ul>
                    </div>
                </div>

                <div id="sub-navigation-wrapper" style="display:none;">

                    <nav id="sub-navigation-planing" class="sub-navigation">

                        <?php echo $lengthOfStayForm; ?>

                        <div id="sub-navigation-planing-carousel-must-wrapper">
                            <ul id="sub-navigation-planing-carousel-must" class="sub-navigation-items jcarousel-skin-flh">
                                <?php
                                      $must = 0;
                                      foreach( $modules[0]['must'] as $module ) : ?>
                                            <li class="<?php if($module['status']) { echo 'done'; $must++; }?>">

                                                <?php if ($module['restricted']): ?>
                                                <div class="restrict">NOT RELEVANT</div>
                                                <?
                                                    $module['slug'] = '#';
                                                endif;
                                                ?>

                                                <a href="/<?php echo $module['slug']; ?>">
                                                    <span></span>
                                                    <?php echo $this->Html->image($module['image'], array('alt' => $module['name'], 'class' => '')) . $module['name']; ?>
                                                </a>
                                            </li>
                                <?php endforeach; ?>
                            </ul>
                        </div>

                        <div id="sub-navigation-planing-carousel-can-wrapper">
                            <ul id="sub-navigation-planing-carousel-can" class="sub-navigation-items jcarousel-skin-flh">
                                <?php
                                      $can = 0;
                                      foreach( $modules[0]['can'] as $module ) : ?>
                                            <li class="<?php if($module['status']) { echo 'done'; $can++; }?>">
                                            <?php if ($module['restricted']): ?>
                                                <div class="restrict">NOT RELEVANT</div>
                                                <?
                                                $module['slug'] = '#';
                                            endif;
                                            ?>
                                            <a href="/<?php echo $module['slug']; ?>"><span></span><?php echo $this->Html->image($module['image'], array('alt' => $module['name'])) . $module['name']; ?></a></li>
                                <?php endforeach; ?>
                            </ul>
                        </div>

                        <div class="must-can-switch ">
                            <div class="clearfix active must"><span><?php echo $must . '/' . count($modules[0]['must']);?></span><a href="#">Must Do's</a></div>
                            <div class="clearfixc can"><span><?php echo $can . '/' . count($modules[0]['can']);?></span><a href="#">Can Do's</a></div>
                        </div>

                    </nav>
                    <nav id="sub-navigation-moving" class="sub-navigation">

                        <?php echo $lengthOfStayForm; ?>

                        <div id="sub-navigation-moving-carousel-must-wrapper">
                            <ul id="sub-navigation-moving-carousel-must" class="sub-navigation-items jcarousel-skin-flh">
                                <?php
                                      $must = 0;
                                      foreach( $modules[1]['must'] as $module ) : ?>
                                            <li class="<?php if($module['status']) { echo 'done'; $must++; }?>">
                                                <?php if ($module['restricted']): ?>
                                                    <div class="restrict">NOT RELEVANT</div>
                                                    <?
                                                    $module['slug'] = '#';
                                                endif;
                                                ?>
                                            <a href="/<?php echo $module['slug']; ?>"><span></span><?php echo $this->Html->image($module['image'], array('alt' => $module['name'])) . $module['name']; ?></a></li>
                                <?php endforeach; ?>
                            </ul>
                        </div>

                        <div id="sub-navigation-moving-carousel-can-wrapper">
                            <ul id="sub-navigation-moving-carousel-can" class="sub-navigation-items jcarousel-skin-flh">
                                <?php
                                      $can = 0;
                                      foreach( $modules[1]['can'] as $module ) : ?>
                                            <li class="<?php if($module['status']) { echo 'done'; $can++; }?>">
                                                <?php if ($module['restricted']): ?>
                                                    <div class="restrict">NOT RELEVANT</div>
                                                    <?
                                                    $module['slug'] = '#';
                                                endif;
                                                ?>
                                                <a href="/<?php echo $module['slug']; ?>"><span></span><?php echo $this->Html->image($module['image'], array('alt' => $module['name'])) . $module['name']; ?></a></li>
                                <?php endforeach; ?>
                            </ul>
                        </div>

                        <div class="must-can-switch">
                            <div class="clearfix active must"><span><?php echo $must . '/' . count($modules[1]['must']);?></span><a href="#">Must Do's</a></div>
                            <div class="clearfix can"><span><?php echo $can . '/' . count($modules[1]['can']);?></span><a href="#">Can Do's</a></div>
                        </div>
                    </nav>
                    <nav id="sub-navigation-build-up" class="sub-navigation">

                        <?php echo $lengthOfStayForm; ?>

                        <div id="sub-navigation-build-up-carousel-must-wrapper">
                            <ul id="sub-navigation-build-up-carousel-must" class="sub-navigation-items jcarousel-skin-flh">
                                <?php
                                  $must = 0;
                                  foreach( $modules[2]['must'] as $module ) : ?>
                                        <li class="<?php if($module['status']) { echo 'done'; $must++;}?>">
                                            <?php if ($module['restricted']): ?>
                                                <div class="restrict">NOT RELEVANT</div>
                                                <?
                                                $module['slug'] = '#';
                                            endif;
                                            ?>
                                            <a href="/<?php echo $module['slug']; ?>"><span></span><?php echo $this->Html->image($module['image'], array('alt' => $module['name'])) . $module['name']; ?></a></li>
                                  <?php endforeach; ?>
                            </ul>
                        </div>

                        <div id="sub-navigation-build-up-carousel-can-wrapper">
                            <ul id="sub-navigation-build-up-carousel-can" class="sub-navigation-items jcarousel-skin-flh">
                                <?php
                                      $can = 0;
                                      foreach( $modules[2]['can'] as $module ) : ?>
                                            <li class="<?php if($module['status']) { echo 'done'; $can++;}?>">
                                                <?php if ($module['restricted']): ?>
                                                    <div class="restrict">NOT RELEVANT</div>
                                                    <?
                                                    $module['slug'] = '#';
                                                endif;
                                                ?>
                                                <a href="/<?php echo $module['slug']; ?>"><span></span><?php echo $this->Html->image($module['image'], array('alt' => $module['name'])) . $module['name']; ?></a></li>
                                <?php endforeach; ?>
                            </ul>
                        </div>

                        <div class="must-can-switch ">
                            <div class="clearfix active must"><span><?php echo $must . '/' .count($modules[2]['must']);?></span><a href="#">Must Do's</a></div>
                            <div class="clearfix can"><span><?php echo $can . '/' . count($modules[2]['can']);?></span><a href="#">Can Do's</a></div>
                        </div>
                    </nav>
                    <nav id="sub-navigation-living" class="sub-navigation">

                        <?php echo $lengthOfStayForm; ?>

                        <div id="sub-navigation-living-carousel-must-wrapper">
                            <ul id="sub-navigation-living-carousel-must" class="sub-navigation-items jcarousel-skin-flh">
                                <?php
                                      $must = 0;
                                      foreach( $modules[3]['must'] as $module ) : ?>
                                          <li class="<?php if($module['status']) { echo 'done'; $must++;}?>">
                                              <?php if ($module['restricted']): ?>
                                                  <div class="restrict">NOT RELEVANT</div>
                                                  <?
                                                  $module['slug'] = '#';
                                              endif;
                                              ?>
                                                <a href="/<?php echo $module['slug']; ?>"><span></span><?php echo $this->Html->image($module['image'], array('alt' => $module['name'])) . $module['name']; ?></a></li>
                                <?php endforeach; ?>
                            </ul>
                        </div>

                        <div id="sub-navigation-living-carousel-can-wrapper">
                            <ul id="sub-navigation-living-carousel-can" class="sub-navigation-items jcarousel-skin-flh">
                                <?php
                                      $can = 0;
                                      foreach( $modules[3]['can'] as $module ) : ?>
                                            <li class="<?php if($module['status']) { echo 'done'; $can++;}?>">
                                                <?php if ($module['restricted']): ?>
                                                    <div class="restrict">NOT RELEVANT</div>
                                                    <?
                                                    $module['slug'] = '#';
                                                endif;
                                                ?>
                                                <a href="/<?php echo $module['slug']; ?>"><span></span><?php echo $this->Html->image($module['image'], array('alt' => $module['name'])) . $module['name']; ?></a></li>
                                <?php endforeach; ?>
                            </ul>
                        </div>

                        <div class="must-can-switch ">
                            <div class="clearfix active must"><span><?php echo $must . '/' . count($modules[3]['must']);?></span><a href="#">Must Do's</a></div>
                            <div class="clearfix can"><span><?php echo $can . '/' . count($modules[3]['can']);?></span><a href="#">Can Do's</a></div>
                        </div>
                    </nav>

                    <nav id="sub-navigation-jobs" class="sub-navigation">

                    </nav>                    
                </div>
            </div>

			<div class="clearfix" style="width:1px;"></div>

            <!-- AddThis Button BEGIN -->
            <?php if ($showAddThis && $current_page_name != 'careesma' && $current_page_name != 'careesma_search'):?>
            <div id="addthis">
                <div class="addthis_toolbox addthis_floating_style addthis_counter_style addthis_button_expanded">
                    <a class="addthis_button_facebook_like" fb:like:layout="box_count"></a>
                    <a class="addthis_button_tweet" tw:count="vertical"></a>
                    <a class="addthis_button_google_plusone" g:plusone:size="tall"></a>
                    <a class="addthis_counter"></a>
                </div>
            </div>
            <?php endif; ?>
            <!-- AddThis Button END -->
            <?if($current_page_name == 'jobs'):
                $wrapper_class = "caressma-wrapper";
                $content_class = "caressma-content"; 
            elseif ($current_page_name == 'jobs_search'):
                $wrapper_class = "caressma-wrapper2";
                $content_class = "caressma-content";                 
            else:
                $wrapper_class = "content-wrapper";
                $content_class = "";
            endif;?>
            <div id="<?=$wrapper_class?>" class="clearfix">
                <div class="<?=$content_class?>">
                    <?php echo $this->Session->flash(); ?>
                    <?php echo $this->fetch('content'); ?>
                </div>
			</div>
			<div id="footer-wrapper">				
				<footer id="footer">
                    <div class="Logos footer-content">
                        <div class="FLH_footer_logo">
                            <p class="logo-title">Operated by:</p>
                            <a href="http://www.feelslikehome.at/" target="_blank"><img style="width:150px;" target="_blank" alt="Feels like Home" src="/img/grass cut out.gif"></a> </div>
                        <div class="AWS_footer_logo">
                            <p class="logo-title">Funded by:</p>
                            <?php echo $this->Html->image('Impulse.jpg', array('alt' => 'impulse', 'style' => 'width:150px;')); ?>
                        </div>
                    </div>
                    <div class="Quick Info footer-content">
                        <p class="footer-title">Quick Info</p>
                        <p><?php echo $this->Html->link('About', '/pages/about'); ?></p>
                        <p><?php echo $this->Html->link('Team', '/pages/team'); ?></p>
                        <p><?php echo $this->Html->link('Jobs', '/pages/jobs'); ?></p>
                    </div>
                    <div class="Information footer-content">
                        <p class="footer-title">Information</p>
                        <p><?php echo $this->Html->link('Terms & Conditions', '/pages/terms'); ?></p>
                        <p><?php echo $this->Html->link('Legal Information', '/pages/legal'); ?></p>
                        <p><?php echo $this->Html->link('Privacy Policy', '/pages/privacy'); ?></p>
                        <p><?php echo $this->Html->link('Sources', '/pages/sources'); ?></p>
                        <p><?php echo $this->Html->link('Impressum', '/pages/impressum'); ?></p>
                    </div>
                    <div class="Business footer-content">
                        <p class="footer-title">Business</p>
                        <p><?php echo $this->Html->link('Supporter', '/pages/supporter'); ?></p>
                        <p><?php echo $this->Html->link('Press', '/pages/press'); ?></p>
                        <p><?php echo $this->Html->link('Become Partner', '/pages/become-partner'); ?></p>
                        <p><?php echo $this->Html->link('Advertise', '/pages/advertise'); ?></p>
                    </div>
				</footer>
			</div>
		</div>
        <?php echo $this->Html->script('main.js');?>

		<?php // echo $this->element('sql_dump'); ?>


        <script type="text/javascript">

            $(document).ready(function() {

                $('.gallery').each(function(index) {

                    maxImages = 30;
                    $(this).attr('id','gallery-'+index);

                    var elementId = '#gallery-'+index;
                    var flickr = "http://api.flickr.com/services/rest/?method=flickr.photosets.getPhotos&api_key=df80068c3747faf779fda00f6c827028&photoset_id="+$(this).attr('data-feed')+"&format=json&nojsoncallback=1";
                    $.support.cors = true;

                      // $.ajax({
                      //   type: 'post',
                      //   url: 'http://api.flickr.com/services/rest/',
                      //   data: {
                      //       method: 'flickr.photosets.getPhotos',
                      //       api_key: 'df80068c3747faf779fda00f6c827028', 
                      //       photoset_id: $(this).attr('data-feed'),
                      //       format: 'json',
                      //       nojsoncallback: '1'
                      //   },
                      //   cache: false,
                      //   dataType: 'json',
                      //   success: function(data){
                      //       alert(data);
                      //   },
                      //   error: function (request, status, error) {
                      //       console.log(request);
                      //       console.log(status);
                      //       console.log(error);                            
                      //   }
                      // });
                    <?if(is_numeric($gallery[0])):
                            $recent = $flickr_api->photosets_getPhotos($gallery, NULL, NULL, NULL, 1);
                            $json_album = json_encode($recent['photoset']['photo']);
                        ?>
                        var json_album = jQuery.parseJSON('<?php echo $json_album;?>');

                        var parentWidth = $(elementId).css("width").replace("px","");
                        $.each(json_album, function(i,item){
                            itemLink = "http://farm"+item.farm+".staticflickr.com/"+item.server+"/"+item.id+"_"+item.secret+".jpg";
                            link = $("<a>").attr({href: itemLink}).appendTo(elementId);
                            $("<img/>").attr({src : itemLink}).height(690).width(440).css({ 'height': '690px', 'width': '440px' }).appendTo(link);
                            if(item.title != "") {
                                $("<span>").text(item.title).appendTo(link);
                            }
                            if ( i == maxImages ) {
                                return false;
                            }
                        });
                        //console.log(parentWidth);
                        $(elementId).coinslider({ hoverPause: false, navigation: true, effect: 'straight', width: 500, height: 350});                     
                        // $(elementId+' img').css({'width':width+'px','height':'440px'});
                        $(elementId+' img').css({'width':'690px','height':'440px'});                                
                        $(elementId).parent().addClass('clearfix');               
                    <?endif;?>
                    // $.getJSON(flickr,
                    //     function(data) {
                    //         if(typeof data.photoset != 'undefined')
                    //         {
                    //             var parentWidth = $(elementId).css("width").replace("px","");
                    //             $.each(data.photoset.photo, function(i,item){

                    //                 itemLink = "http://farm"+item.farm+".staticflickr.com/"+item.server+"/"+item.id+"_"+item.secret+".jpg";
                    //                 link = $("<a>").attr({href: itemLink}).appendTo(elementId);
                    //                 $("<img/>").attr({src : itemLink}).height(690).width(440).css({ 'height': '690px', 'width': '440px' }).appendTo(link);
                    //                 if(item.title != "") {
                    //                     $("<span>").text(item.title).appendTo(link);
                    //                 }
                    //                 if ( i == maxImages ) {
                    //                     return false;
                    //                 }
                    //             });
                    //             //console.log(parentWidth);
                    //             $(elementId).coinslider({ hoverPause: false, navigation: true, effect: 'straight', width: $(elementId).css("width").replace("px",""), height: 440});                     
                    //             // $(elementId+' img').css({'width':width+'px','height':'440px'});
                    //             $(elementId+' img').css({'width':'600px','height':'440px'});                                
                    //             $(elementId).parent().addClass('clearfix');
                    //         }
                    //     });
                        //.error(function(jqXHR, textStatus, errorThrown) { console.log(errorThrown); });
                });
            });
        </script>


        <!-- Modal Login-->
        <?php if(empty($user)): ?>
            <div id="Login2" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                <div class="modal-header" style="text-align: center;">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h2 style="display: inline;"><span style="padding-top: 0;">Login</span></h2>
                </div>
                <div class="modal-body">
                    <?php

                    echo $this->Form->create('User', array('action' => 'login'));
                    echo $this->Form->inputs(array(
                        'legend' => '',
                        'email',
                        'password'
                    ));
                    echo $this->Form->end();

                    ?>
                </div>
                <div class="Login-footer" stlye="font-size: smaller;">
                <?php echo $this->Html->link('Forgot Password?', array('controller' => 'users','action' => 'forgot_password'), array('style' => 'font-size:small')); ?>
                <?php //echo $this->Html->link('Activation Problem?', array('controller' => 'users','action' => 'resendActivationMail'), array('style' => 'font-size:small; float: right;margin-left: 0px;margin-right: 20px;')); ?>
                </div>
                <div class="modal-footer" style="background: none;">
<!--                     <?php echo $this->Html->link('Login with Facebook', array('controller' => 'users','action' => 'add'),array('class' => 'btn', 'style' => 'float:left;margin-right:5px; margin-left: 0px;')); ?> -->
<!-- <fb:login-button show-faces="true" width="200" max-rows="1"></fb:login-button> -->
                        <a href="<?=$fb_login_url?>" style="float:left;margin-right:5px; margin-left: 0px;"><img src="/img/fb_login_button.png"/></a>
<!--                     <button class="btn" data-dismiss="modal" aria-hidden="true">Close</button> -->
                    <button class="button-blue-small" id="loginButton" style="float:right;margin-left:5px;">Login</button>
                </div>
            </div>
        <?php endif; ?>

        <!-- Modal Registration-->
        <?php if(empty($user)): ?>
        <?
            if($facebool_login == true):
                $path = array('action'=>'fb_registration');
            else:
                $path = array('action'=>'registration');
            endif;
        ?>
        <?php echo $this->Form->create('User',$path); ?>
            <div id="Registration" name="Registration" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel2" aria-hidden="true" style="width:600px !important;left:40% !important; top:3% !important;position:absolute;">
<!--             FIRST STEP -->
            <div id="Step1" name="Step1">
                <div class="modal-header" style="text-align: center;">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                     <div class="heading-center"><h1><span>Register</span></h1></div>
                </div>
                <div>
<div class="users form register" style="padding-left:15px;">
    <div>
        You need to register in order to Use the „Step-by-Step“Guide. <br/>Don’t worry – the Guide is completely for free!
    </div>
    <br />
    <fieldset>
        <div class="input required" style="height:40px;">
            <label>Gender</label>
            <fieldset class="input">
                    <label for="UserGenderMale" style="width: 120px !important;">
                        <input id="UserGenderMale" type="radio" name='data[User][gender]' value="m"/>
                        Male
                    </label>
                    <label for="UserGenderFemale">
                        <input id="UserGenderFemale" type="radio" name='data[User][gender]' value="f" />
                        Female
                    </label>
            </fieldset>
        </div>
    <?php
        $placeholder = '[RandomStringWhichDoesNotAppearInTheMarkup]';

        echo $this->Form->input('firstname', array(
            'label' => 'First Name',
            'required' => 'true',
            'value' => @$fb_user_data['first_name']
        ));
        echo $this->Form->input('surname', array(
            'label' => 'Last Name',
            'required' => 'true',
            'value' => @$fb_user_data['last_name']
            ));
        ?><img id="password-image"src="../img/check-green.png" style="visibility: hidden; margin-left: 10px;"/><?
        $out = $this->Form->input('birthdate', array(
            'label' => 'Date of birth',
            'type' => 'date',
            'empty' => $placeholder,
            'dateFormat' => 'DMY',
            'minYear' => date('Y') - 99,
            'maxYear' => date('Y') - 13,
            'required' => 'true',
        ));
        $escapedPlaceholder = preg_quote($placeholder, '/');
        $out = preg_replace("/$escapedPlaceholder/", '(Day)', $out, 1);
        $out = preg_replace("/$escapedPlaceholder/", '(Month)', $out, 1);
        $out = preg_replace("/$escapedPlaceholder/", '(Year)', $out, 1);

        echo $out;


        echo $this->Form->input('start_country', array(
            'label' => 'Citizenship',
            'type' => 'select',
            'options' => $countries,
            'default' => 14,
            'required' => 'true'
        ));
        echo $this->Form->input('language', array(
        'type' => 'select',
        'options' => $languages,
        'default' => 10,
        'required' => 'true'
        )); ?>
        <div class="dropdown-with-hint">
        <?php
            echo $this->Form->input('trip_length', array(
                'options' => $trip_length,
                'empty' => '(Please select)',
                'label' =>  'Length of Stay',
                'required' => 'true',
            ));
        ?>
            <br/><a data-toggle="modal" href="#hint_trip" class="text-small dropdown-hint" style="position:0px;margin-left:-220px;margin-top:15px;">Why do I need to tell you? / What if I don’t know yet?</a><br/>
        </div>

        <br style="clear: both"/>
        <div class="dropdown-with-hint">
        <?php
        //Yep, this is ugly, but it works. See http://stackoverflow.com/questions/11588022 for further info
            $out= $this->Form->input('planned_arrival', array(
                'label' => 'Planned Arrival Date',
                'type' => 'date',
                'empty' => $placeholder,
                'dateFormat' => 'DMY',
                'minYear' => date('Y'),
                'maxYear' => date('Y') + 5,
                'required' => 'true',
            ));
        $escapedPlaceholder = preg_quote($placeholder, '/');
        $out = preg_replace("/$escapedPlaceholder/", '(Day)', $out, 1);
        $out = preg_replace("/$escapedPlaceholder/", '(Month)', $out, 1);
        $out = preg_replace("/$escapedPlaceholder/", '(Year)', $out, 1);

        echo $out;
        ?>
            <br/><a data-toggle="modal" href="#hint_arrival" class="text-small dropdown-hint" style="position:0px;margin-left:-270px;margin-top:18px;">What if I don’t know yet? / What if I am already in Vienna?</a><br/>
        </div>
        <br style="clear: both"/>
        <?php

        if($facebool_login == false):
            echo $this->Form->input('email', array(
                'required' => 'true',
            ));
        else:
            echo $this->Form->input('email', array(
                'required' => 'true',
                'disabled' => 'disabled'
            ));
        endif;
        ?>

        <div class="dropdown-with-hint">
        <?php
        if(!isset($facebool_login) || $facebool_login != true):
            echo $this->Form->input('password', array(
                'id' => 'password',
                'required' => 'true',
                'pattern' =>  '.{4,}',
                'onkeyup' => 'checkPass(); return false;',
            ));?>
                <br/><a data-toggle="modal" href="#hint_password" class="text-small dropdown-hint" style="position:0px;margin-left:-290px;margin-top:15px;">What are the Password Requirements?</a><br/>
            </div>
            <br style="clear: both"/>
            <div class="dropdown-with-hint">
                <?php
                echo $this->Form->input('password_check', array(
                    'id' => 'password_check',
                    'type' => 'password',
                    'required' => 'true',
                    'onkeyup' => 'checkPass(); return false;',
                ));

                //url("../img/check-green.png")
                ?>
                <img id="password-image"src="../img/check-green.png" style="visibility: hidden; margin-left: 10px;"/>
        <?else:?>
            <script>
                $(document).ready(function(){
                    $("#Reg_button").click();
                });
            </script>
        <?endif;?>
        </div>
    </fieldset>
<br />
    <p class="text-small" style="color:red;">* means required</p>
    <!-- <input type="submit" class="button-yellow-big" value="Register"> -->
</div>
<div class="modal modal-large fade" id="hint_trip">
    <div class="modal-header">
        <br/>
        <a class="close" data-dismiss="modal" style="position:0px;margin-left:-270px;margin-top:18px;">&times;</a>
        <h2><span>Why do I need to tell you? / What if I don’t know yet?</span></h2>
    </div>
    <div class="modal-body">
        In order to provide you with personalized and individual information about your Austrian visa, health insurance, accomondation , bank account and much more we need to know your expected length of stay. We can only show you precise informations and solutions, if we know your expected length of stay.
        If you are not yet sure about your length of stay – choose the most probable one. You can change the length of stay directly inside the guide at the header at any time.
    </div>
    <div class="modal-footer">
        <a href="#" class="btn" data-dismiss="modal">Close</a>
    </div>
</div>

<div class="modal modal-large fade" id="hint_arrival">
    <div class="modal-header">
        <a class="close" data-dismiss="modal">&times;</a>
        <h2><span> What if I don’t know yet? / What if I am already in Vienna?</span></h2>
    </div>
    <div class="modal-body">
        Text: Telling us about your arrival date (also approx. date) helps us to give you all interesting information you need for your move to Vienna. The more information we have - the better service and personalized offers we can provide. If you are not yet sure about your arrival date – choose the most probable one. In case you are already in Vienna just put the current day into planned arrival day.
       </div>
    <div class="modal-footer">
        <a href="#" class="btn" data-dismiss="modal">Close</a>
    </div>
</div>

<div class="modal modal-large fade" id="hint_password">
    <div class="modal-header">
        <a class="close" data-dismiss="modal">&times;</a>
        <h2><span>What are the Password Requirements?</span></h2>
    </div>
    <div class="modal-body">
        Your password should at least be 4 characters long.
    </div>
    <div class="modal-footer">
        <a href="#" class="btn" data-dismiss="modal">Close</a>
    </div>
</div>

<script>
    function checkPass()
    {
        var pass1 = document.getElementById('password');
        var pass2 = document.getElementById('password_check');
        var img = document.getElementById('password-image');
        if(pass1.value.length > 3 && pass2.value.length > 3 && (pass1.value == pass2.value)){

            img.style.visibility = 'visible';
        }else{
            img.style.visibility = 'hidden';
        }
    }
</script>

<script>
    $(document).ready(function(){
        $.validator.setDefaults({
            errorPlacement: function(error, element) {
                //$(element).attr({"title": error.append()});
            },
            highlight: function(element){
                $(element).css({"border": "1px solid #FF0000","box-shadow": "box-shadow: -1px -1px 2px 2px rgba(255, 0, 0, 0.075) inset;"});
                //$(element).addClass("errorHighlight");
            },
            unhighlight: function(element){
                $(element).css({"border": "1px solid #CCCCCC", "box-shadow": "0 1px 1px rgba(0, 0, 0, 0.075) inset"});
                //$(element).removeClass("errorHighlight");
            }
        });

        <?
            if($facebool_login == true):
                $form_name = "UserFbRegistrationForm";
            else:
                $form_name = "UserRegistrationForm";
            endif;
        ?>


        $("#<?=$form_name?>").validate();

        $("#NextStep").click(function(){  
            if($("#<?=$form_name?>").valid()){
                $("#Step1").fadeOut(function(){      
                    $("#Step2").fadeIn();
                });
            }
        });

        $("#StepBack").click(function(){               
            $("#Step2").fadeOut(function(){              
                $("#Step1").fadeIn();
            });
        });

        $("#comlete_registration").click(function(){
            $("#<?=$form_name?>").submit();
        });

    });
</script>

                </div>
                <div class="modal-footer" style="background: none;">
                     <?if(!isset($facebool_login)):?>
                     <a href="<?=$fb_login_url?>" style="float:left;margin-right:5px; margin-left: 0px;"><img src="/img/fb_login_button.png"/></a>
                     <?endif;?>
                    <button type="button" class="button-blue-small" id="NextStep" name="NextStep" style="float:right;margin-left:5px;">Next step</button>
                </div>

            </div>
<!--             End of first Step -->
            <div id="Step2" name="Step2" style="display:none;padding-left:15px;padding-right:15px;">
                <div class="modal-header" style="text-align: center;">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                     <div class="heading-center"><h1><span>Complete Registration</span></h1></div>
                </div>
                <div>
                    <div style="width:100%;"><br/>
                        <div>
                            <div style="float:left;margin-left:15px;font-size:12px;">
                                <strong style="float:left;position:absolute;">Now, get your free welcome bag, <br/>which includes:</strong><br/><br/><br/>
                                <div style="margin-top:20px;">
                                    <img src="/img/checkmark.png"/> Free SIM card<br/><br/>
                                    <img src="/img/checkmark.png"/> FLH community card<br/><br/>
                                    <img src="/img/checkmark.png"/> info about Vienna<br/><br/>
                                    <img src="/img/checkmark.png"/> info about our service<br/><br/>
                                    <img src="/img/checkmark.png"/> Free vouchers worth at least € 10<br/>
                                </div>
                            </div>
                            <div style="float:right;margin-top:60px;">
                                <img src="/img/welcomebag.png"/>
                            </div>
                        </div>
                        <br/><br/>
                    </div>
                    <div style="margin-top:310px;text-align:justify;margin-right:5px;">
                        The StepbyStepGuide is provided by FeelsLikeHome.at – the community for internationals in Vienna.
                        Every member receives our Welcome bag for free -  no costs, no tricks, no hooks! With the included FeelsLikeHome community card you get all kinds of discounts, special offers and access to all our events.  Find all descriptions inside the guide as well.
                    </div>
                    <br style="clear: both"/>
                    <div>
                        <fieldset class="input">
                            <label for="receivedbag">
                                <input id="receivedbag" type="radio" name="data[User][bag]" class="bag_option" value="has_bag"/>I received already a Welcome Bag from FeelsLikeHome Partner<br/>
                            </label><br/>
                            <label for="sendbag">
                                <input id="sendbag" type="radio" name="data[User][bag]" class="bag_option" value="send_bag"/>Send Me the Bag
                            </label>
                            <label for="pickup">
                                <input id="pickup" type="radio" name="data[User][bag]" class="bag_option" value="pickup_bag"/>I will Pickup The Bag
                            </label>
                        </fieldset>
                        <script>
                        $(document).ready(function(){
                            $(".bag_option").click(function(){
                                if($(this).attr("id") == "sendbag"){
                                    $(".pickup_data").hide();
                                    $(".address_data").fadeIn();
                                }else if($(this).attr("id") == "pickup"){
                                    $(".address_data").hide();                                    
                                    $(".pickup_data").fadeIn();
                                }else{
                                    $(".pickup_data").fadeOut();
                                    $(".address_data").fadeOut();                                       
                                }
                            });
                        });
                        </script>
                        <div class="address_data" style="display:none;">
                            <legend>Send the Bag to address:</legend>
                            <?php            
                                echo $this->Form->input('country_to', array(
                                    'label' => 'Country',
                                    'type' => 'select',
                                    'options' => $countries,
                                ));?>
                                <div class="input text required" style="float:left;">
                                    <label for="City">City</label>
                                    <input type="text" id="City" maxlength="255" required="true" name="data[bag][city]">
                                </div>
                                <div class="input text required" style="float:right;position:absolute;margin-left:300px;">
                                    <label for="Zip">Zip</label>
                                    <input type="text" id="Zip" maxlength="255" required="true" name="data[bag][zip]" style="width:65px !important;">
                                </div>
                                <br style="clear: both"/>
                                <div class="input text required" style="float:left;">
                                    <label for="Street">Street</label>
                                    <input type="text" id="Street" maxlength="255" required="true" name="data[bag][street]">
                                </div>
                                <div class="input text required" style="float:right;position:absolute;margin-left:300px;">
                                    <label for="Number">Number</label>
                                    <input type="text" id="Number" maxlength="255" required="true" name="data[bag][number]" style="width:65px !important;">
                                </div>
                                <br style="clear: both"/>
                                <?php //echo $this->Form->input('city', array('label' => 'City', 'required' => 'true',));
                                //echo $this->Form->input('zip', array('label' => 'Zip Code', 'required' => 'true',));

                                //echo $this->Form->input('street', array('label' => 'Street', 'required' => 'true',));
                                //echo $this->Form->input('number', array('label' => 'Number', 'required' => 'true',));
                                echo $this->Form->input('additionalInfo', array('label' => 'Additional Info', 'type' => 'textarea'));
                            ?>
                        </div>
                        <div class="pickup_data" style="display:none;font-size:12px;">
                            <legend>Instructions for picking up the bag</legend>
                                <p>Dear StepByStepGuide User, you can pick up your FeelsLikeHome bag in our office.Our office is directly at Webster University Berchtoldgasse 1, 1220 Vienna, 3rd floor, right wing. Our office hours are: Mo – Fr. 10:00h – 16:00h. You will also receive an e-mail with more details about the office pick-up.
                        </div>                        
                    </div> 
                    <br style="clear: both"/>
                    <div class="input" style="height: 30px !important;">
                        <label for="newsletter">
                            <input id="newsletter" type="checkbox" name="newsletter" checked/>Newsletter
                        </label>
                    </div>
                    <div class="input">
                        <label for="TOS" class="TOS">
                            <input id="TOS" type="checkbox" required="true" name="TOS"/>
                            <span style="color: #e32;display:inline;">*</span>
                            I hereby confirm that I have read and accepted the: <a href="/pages/terms" target="_blank">Terms & Conditions</a>.<br/><a href="/pages/legal" target="_blank">Legal Information</a> and the <a href="/pages/privacy" target="_blank">Privacy Policy</a>, including Cookie Use.
                        </label>
                    </div>
                   <br/>
                    <div style="margin-left:120px;">
                    <script type="text/javascript">
                         var RecaptchaOptions = {
                            theme : 'clean'
                         };
                     </script>
                    <?echo $this->ReCaptcha->recaptcha_get_html('6LccBukSAAAAAH9W4lxWoZCT3EY04BRBcFcNFzEN');?>
                   <!--  <a href="#" style="float:right;margin-right:-5px;margin-top:-90px;"><img src="/img/start.jpg" style="width:120px;float:right;margin-top:-7px;" /></a><br/>
                    <button class="button-blue-small" id="StepBack" name="StepBack" style="float:right;margin-right:4px;margin-top:-60px;">Back</button> -->
                    </div>
                    <div class="modal-footer" style="background: none;margin-top:10px;">
                        <button type="button" class="button-blue-small" id="StepBack" name="StepBack" style="margin-left:5px;float:left;margin-right:5px; margin-left: 0px;">Back</button>
                        <a href="#" id="comlete_registration" name="comlete_registration"><img src="/img/start.jpg" style="width:120px;float:right;margin-top:-7px;margin-right:-13px;" /></a>
                    </div>           
                </div>
            </div>

            </div>
        <?php echo $this->Form->end(); ?>
        <?php endif; 
            //END of registration
        ?>

        <?php // Contact Form ?>
        <div id="Contact" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-header" style="text-align: center;">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h2 style="display: inline;"><span style="padding-top: 0;">Contact us</span></h2>
            </div>
            <div class="modal-body">

                <form id="ContactForm" action="/pages/contact" method="POST">

                    <label for="UserName">Name</label>
                    <input type="text" id="UserName" maxlength="255" name="data[name]"><br>
                    <label for="UserEmail">Email</label>
                    <input type="text" id="UserEmail" maxlength="255" name="data[email]"><br>
                    <label for="EmailText">Message</label>
                    <textarea id="EmailText" name="data[text]"></textarea>

                </form>
            </div>
            <div class="modal-footer" style="background: none;">
                <button class="btn" data-dismiss="modal" aria-hidden="true">Close</button>
                <button class="button-blue-small" id="contactButton" style="float:right;margin-left:5px;">Send</button>
            </div>
        </div>

        <script>
            (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
                (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
                m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
            })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

            ga('create', 'UA-41418738-1', 'stepbystepguide.eu');
            ga('send', 'pageview');

        </script>
    </body>
</html>
