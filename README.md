# feelslikehome_disclaimer

__Kunde:__ Dr. Alexander Karakas


__Ansprechpartner:__ Dr. Alexander Karakas office@divania-consultants.com


__Verantwortlicher:__ l.burda@ibdigital.de


__live-url:__ http://feelslikehome.eu/


__Kurzbeschreibung:__


Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ex expedita numquam aliquid, explicabo. Aliquam quod magni tempore, neque vero quam architecto, culpa sint soluta nesciunt enim eum id, commodi qui.

## SETUP

These are the default steps to startup the application
```
git clone git@bitbucket.org:ibdigital/feelslikehome_disclaimer.git
echo "Set me up!" >> README.md
git add README.md
git push -u origin master
...
```

Other steps
```
...
```

***

![logo](http://ibdigital.de/typo3conf/ext/ibtemplate_kickstart/page/images/logo.png)
 © Copyright - ibDigital GmbH